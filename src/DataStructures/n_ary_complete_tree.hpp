/*
 * n_ary_complete_tree.hpp
 *
 *  Created on: Dec 30, 2012
 *      Author: daniel
 */

#ifndef N_ARY_COMPLETE_TREE_HPP_
#define N_ARY_COMPLETE_TREE_HPP_

#include "../Definitions/types.hpp"
#include "../LCP/LCP.hpp"

class n_ary_complete_tree{
public:
	n_ary_complete_tree(std::istream& input);
	n_ary_complete_tree(AbstractLcp* lcp, word n);
	void serialize(std::ostream& output);
	void setLcp(AbstractLcp* lcp);
	static const word tree_factor = 32;

	word get_lcp_leaf(word i);
	bool isRoot(word node);
	bool isLeaf(word node);
	bool isValid(word node);
	word children(word node);
	word parent(word node);
	word getNode(word node);
	word get_number_of_leaves();
	word get_number_of_nodes();
	~n_ary_complete_tree();
private:

	void preprocess_leaves();
	void preprocess_tree();

	AbstractLcp* lcp;
	word height;
	word n_leaves;
	word n_internal;
	word n;
	word* tree;
};

#endif /* N_ARY_COMPLETE_TREE_HPP_ */
