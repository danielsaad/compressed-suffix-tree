/*
 * n_ary_complete_tree.cpp
 *
 *  Created on: Dec 30, 2012
 *      Author: daniel
 */

#include "n_ary_complete_tree.hpp"
#include "../Definitions/utils.hpp"
#include <cmath>

n_ary_complete_tree::n_ary_complete_tree(AbstractLcp* lcp, word n){
	this->n = n;
	tree=0;
	n_leaves = ceil((double)n/tree_factor);
	word n_leaves_complete = pow(tree_factor,ceil(log(n_leaves,tree_factor)));
	height = floor(log(n_leaves_complete,tree_factor));
	n_internal = (pow(tree_factor,height)-1)/(tree_factor-1);
	tree = new word[n_leaves+n_internal];
	this->lcp = lcp;
	preprocess_tree();
}


n_ary_complete_tree::n_ary_complete_tree(std::istream& input){
	tree=0;
	lcp=0;
	input.read(reinterpret_cast<char*>(&n),sizeof(n));
	input.read(reinterpret_cast<char*>(&n_leaves),sizeof(n_leaves));
	input.read(reinterpret_cast<char*>(&n_internal),sizeof(n_internal));
	input.read(reinterpret_cast<char*>(&height),sizeof(height));
	tree = new word[n_internal+n_leaves];
	input.read(reinterpret_cast<char*>(tree),sizeof(word)*(n_leaves+n_internal));
}

void n_ary_complete_tree::serialize(std::ostream& output){
	output.write(reinterpret_cast<const char*>(&n),sizeof(n));
	output.write(reinterpret_cast<const char*>(&n_leaves),sizeof(n_leaves));
	output.write(reinterpret_cast<const char*>(&n_internal),sizeof(n_internal));
	output.write(reinterpret_cast<const char*>(&height),sizeof(height));
	output.write(reinterpret_cast<const char*>(tree),sizeof(word)*(n_leaves+n_internal));
}


void n_ary_complete_tree::setLcp(AbstractLcp* lcp){
	this->lcp = lcp;
}


void n_ary_complete_tree::preprocess_leaves(){
	word i,k;
	word lcp_minimum;
	lcp_minimum = MAX_WORD;
	for(i=0,k=n_internal;i<n;i++){
		if(lcp->access(i)<lcp_minimum){
			tree[k] = i;
			lcp_minimum=lcp->access(i);
		}
		if(i%tree_factor==tree_factor-1){
			k++;
			lcp_minimum = MAX_WORD;
		}
	}
}

bool n_ary_complete_tree::isRoot(word node){
	if(node==0) return true;
	return false;
}
word n_ary_complete_tree::get_lcp_leaf(word i){
	return(n_internal+ (i/tree_factor));
}

void n_ary_complete_tree::preprocess_tree(){
	word i;
	word childrenPtr;
	word minimum,minimum_lcp;
	preprocess_leaves();

	for(i=n_internal-1;i!=MAX_WORD;i--){
		childrenPtr = children(i);
		minimum_lcp = MAX_WORD;
		for(word k=0;k<tree_factor;k++){
			if(isValid(childrenPtr+k)){
				if((lcp->access(tree[childrenPtr+k])<minimum_lcp)){
					minimum = tree[childrenPtr+k];
					minimum_lcp = lcp->access(tree[childrenPtr+k]);
				}
			}
			else if(minimum_lcp==MAX_WORD){
				minimum = tree[n_leaves+n_internal-1];
			}
		}
		tree[i] = minimum;
//		std::cout << "tree[" << i << "] = " << tree[i] << "\n";
	}
}

word n_ary_complete_tree::children(word node){
	if(isLeaf(node)) return node;
	return(node*tree_factor+1);
}

word n_ary_complete_tree::parent(word node){
	if(node==0) return 0;
	return((node-1)/tree_factor);
}


bool n_ary_complete_tree::isValid(word node){
	if(node>=0 && node < n_leaves+n_internal) return true;
	return false;
}

bool n_ary_complete_tree::isLeaf(word node){
	if(node>=n_internal) return true;
	return false;
}

word n_ary_complete_tree::getNode(word node){
	return(tree[node]);
}

word n_ary_complete_tree::get_number_of_leaves(){
	return(n_leaves);
}

word n_ary_complete_tree::get_number_of_nodes(){
	return(n_internal+n_leaves);
}
n_ary_complete_tree::~n_ary_complete_tree(){
	if(tree!=0){
		delete tree;
	}
}
