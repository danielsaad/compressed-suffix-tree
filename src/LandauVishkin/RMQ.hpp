#ifndef RMQ_HPP
#define RMQ_HPP

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <limits.h>
using namespace std;

namespace Algorithms{

typedef int DT;                 // use long for 64bit-version (but take care of fast log!)
typedef unsigned int DTidx;     // for indexing in arrays
typedef unsigned char DTsucc;
typedef unsigned short DTsucc2;


/* Abstract class for RMQ-queries. Proprocessing is done
   in constructor. */
class RMQ {
public:
    // returns index of RMQ[i,j]
    virtual integer operator()(DTidx i,DTidx j){ return query(i,j); }
    virtual DTidx query(DTidx, DTidx) = 0;
    virtual ~RMQ(){}
};


/* Implements the <O(n log n), O(1)>-method for RMQ as described in
 * Bender and Farach's paper, section 3.
 */
class RMQ_nlogn_1 : public RMQ {
public:
    // liefert RMQ[i,j]
    virtual DTidx query(DTidx, DTidx);

    RMQ_nlogn_1(DT* a, DTidx* c, DTidx n);

    virtual ~RMQ_nlogn_1();

    // the following stuff is for fast base 2 logarithms:
    // (currently only implemented for 32 bit numbers)
    static const char LogTable256[256];

    virtual DTidx log2fast(DTidx);

protected:
    // array
    DT* a;

    // index array for a:
    DTidx* c;

    // size of c
    DTidx n;

    // depth of table
    DTidx depth;

    // the precomputed table:
    DTidx** M;

};

class RMQ_n_1_improved : public RMQ {
public:
    // liefert RMQ[i,j]
    virtual DTidx query(DTidx, DTidx);

    RMQ_n_1_improved(DT* a, DTidx n);

    ~RMQ_n_1_improved();

protected:
    // array
    DT *a;

    // index array for the out-of-block queries (contains indices of block-minima)
    DTidx *c;

    // type of blocks
    DTidx *type;

    // precomputed in-block queries
    DTidx** Prec;

    // number of different queries per block = bs*(bs+1)/2
    DTidx qpb;

    // size of array a
    DTidx n;

    // block size
    DTidx bs;

    // number of blocks (always n/bs)
    DTidx nb;

    // return block of entry i:
    inline DTidx block(DTidx i) { return i/bs; }

    // nlogn_1-Algo for out-of-block-block-queries:
    RMQ_nlogn_1* RMQ_ST;

    // precomputed Catalan triangle (17 is enough for 64bit computing):
    static const DTidx Catalan[17][17];

    // minus infinity (change for 64bit version)
    static const DT minus_infinity;
};


template <class T>
class RMQ_succinct : public RMQ {
public:
    // liefert RMQ[i,j]
    virtual DTidx query(DTidx, DTidx);

    RMQ_succinct(T* a, DTidx n);

    ~RMQ_succinct();

protected:
    // array
    T* a;

    // size of array a
    DTidx n;

    // table M for the out-of-block queries (contains indices of block-minima)
    DTsucc** M;

    // because M just stores offsets (rel. to start of block), this method
    // re-calculates the true index:
    inline DTidx m(DTidx k, DTidx block) { return M[k][block]+(block*sprime); }

    // depth of table M:
    DTidx M_depth;

    // table M' for superblock-queries (contains indices of block-minima)
    DTidx** Mprime;

    // depth of table M':
    DTidx Mprime_depth;

    // type of blocks
    DTsucc2 *type;

    // precomputed in-block queries
    DTsucc** Prec;

    // microblock size
    DTidx s;

    // block size
    DTidx sprime;

    // superblock size
    DTidx sprimeprime;

    // number of blocks (always n/sprime)
    DTidx nb;

    // number of superblocks (always n/sprimeprime)
    DTidx nsb;

    // number of microblocks (always n/s)
    DTidx nmb;

    // return microblock-number of entry i:
    inline DTidx microblock(DTidx i) { return i/s; }

    // return block-number of entry i:
    inline DTidx block(DTidx i) { return i/sprime; }

    // return superblock-number of entry i:
    inline DTidx superblock(DTidx i) { return i/sprimeprime; }

    // precomputed Catalan triangle (17 is enough for 64bit computing):
    static const DTidx Catalan[17][17];

    // minus infinity (change for 64bit version)
    static const DT minus_infinity;

    // stuff for clearing the least significant x bits (change for 64-bit computing)
    static const DTsucc HighestBitsSet[8];
    virtual DTsucc clearbits(DTsucc, DTidx);

    // Least Significant Bits for 8-bit-numbers:
    static const char LSBTable256[256];

    // return least signigicant bit in constant time (change for 64bit version)
    virtual DTidx lsb(DTsucc);

    // the following stuff is for fast base 2 logarithms:
    // (currently only implemented for 32 bit numbers)
    static const char LogTable256[256];

    virtual DTidx log2fast(DTidx);
};

#include "RMQ.tpp"

}//End namespace Algorithms

#endif // RMQ_HPP

