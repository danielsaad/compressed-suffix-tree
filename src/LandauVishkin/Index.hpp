#ifndef INDEX_HPP
#define INDEX_HPP
#include <unordered_map>
#include <divsufsort.h>
#include "RMQ.hpp"
#include "../../src/Definitions/types.hpp"
#include "../../src/Text/text.hpp"
namespace Algorithms{



template<class CLCP>
class Index{
public:
    Index(Text* T);
    integer* _sa;
    integer* _isa;
    CLCP* _lcp;
    byte* _textStr;
    integer _size;
    ~Index();
};

template<class CLCP>
class SemiExternalIndex{
public:
    SemiExternalIndex(Text* T);
    integer* _sa;
    integer* _isa;
    CLCP* _lcp;
    byte* _textStr;

    integer size;
    ~SemiExternalIndex();
};

template<class CLCP, class CRMQ>
class IndexRMQ{
public:
    IndexRMQ(Text* T);
    integer* _sa;
    integer* _isa;
    CLCP* _lcp;
    byte* _textStr;
    CRMQ* _rmq;

    integer _size;

    ~IndexRMQ();
};

template<class CLCP, class CRMQ>
class SemiExternalIndexRMQ{
public:
    SemiExternalIndexRMQ(Text* T);
    integer* _sa;
    integer* _isa;
    CLCP* _lcp;
    CRMQ* _rmq;

    integer size;
    ~SemiExternalIndexRMQ();
};


class RegularLCP{
public:
    RegularLCP(integer size){_lcp = new integer[size];}

    integer operator[](const integer i);
    void set(const integer i, const integer l){ _lcp[i]  = l; }
    integer* _lcp;

    ~RegularLCP(){ delete[] _lcp; }
};


class ByteLCP{
 public:
    ByteLCP(integer size){_lcp = new byte[size];}
    integer operator[](const integer i);
    void set(const integer i, const integer l){
        if(l<255){
            _lcp[i] = l;
        }
        else{
            _lcp[i] = 255;
           _hash[i] = l;
        }
    }
    byte* _lcp;
    std::unordered_map<integer,integer> _hash;

    ~ByteLCP(){ delete[] _lcp; }
};

#include "Index.tpp"

}//End namespace algorithms
#endif // INDEX_HPP
