#ifndef LANDAUVISHKIN_HPP
#define LANDAUVISHKIN_HPP

/*
 * LandauVishkin.hpp
 *
 *  Created on: Dec 9, 2013
 *      Author: daniel
 */

#include <iostream>
#include <cstring>
#include <thread>
#include <vector>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include "../SA/SA.hpp"
#include "../Text/text.hpp"
#include "../Definitions/types.hpp"

//#define SHOW


namespace Algorithms{


static const integer LV_DIRECT_MIN_BLOCK = 8;
static const integer LV_STRING_SIZE = 5;
static const integer LV_BLOCK_SIZE = 1<<24;
static const integer LV_NUMBER_OF_THREADS =4;


// Landau-Vishkin with direct comparisons only
void landauVishkin_DC(Text* text_text,Text* pattern_text,integer errors, Text* output);

void LandauVishkin_DC_Navarro(Text* text_text,Text* pattern_text,integer errors, Text* output);

//Landau-Vishkin with direct comparisons only (text based). Semi-external approach.
void landauVishkin_DC_Semi_External(Text* text_text,Text* pattern_text,integer errors, Text* output);



typedef struct LV_DC_parallel_struct_t{
    integer* l_array;
    integer base;
    integer prev;
    integer cur;
    integer next;
    integer begin;
    integer last;
    integer end;
    byte* pattern;
    byte* text;
    integer text_length;
    integer pattern_length;
}LV_DC_parallel_struct_t;


integer match(const byte* str1,const byte* str2);

//Parallel processing of Landau-Vishkin Algorithm using Threads
void landauVishkin_DC_Parallel(Text* text_text,Text* pattern_text,integer errors, Text* output);
void landauVishkin_DC_Parallel_ProcessDiagonals(const LV_DC_parallel_struct_t& LV_struct);

//Requires a SA with support to LCP and Inverse Suffix Array
template<class TSA> void landauVishkin_DMin(Text* text_text,Text* pattern_text,integer errors, Text* output);


//Landau-Vishkin using suffix arrays and direct comparisons
template<class TSA> void landauVishkin_DMin(Text* text_text,Text* pattern_text,integer errors, Text* output);
template<class TSA> integer directComparisons(TSA* sa,word i,word j);
template<class TSA> integer directMin(TSA* sa,word i,word j);

//Classical LV-solution with suffix arrays and RMQ
template<class TSA> void landauVishkin_RMQ(Text* text_text,Text* pattern_text,integer errors, Text* output);

#include "LandauVishkin.tpp"

} //End namespace Algorithms



#endif // LANDAUVISHKIN_HPP
