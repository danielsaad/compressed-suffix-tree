#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <ctime>
#include <cassert>
#include "SA/AbstractSTSA.hpp"
#include "SA/SA.hpp"
#include "SA/STSA.hpp"
#include "SA/CST.hpp"
#include "SA/CST_Navarro.hpp"
#include "debug.hpp"
#include "bit.hpp"
#include "text.hpp"


/**ARGV[1]
    FILE CONTAINING THE TEXT
**/

int main(int argc,char** argv){

    Text* T;
    Text* Index;
    AbstractSTSA_RMQ* c;



    if(argc<3 || argc >5){
    	std::cerr << "Error\n";
    	std::cerr << "Usage = " << argv[0] << "-create <inputText> <indexFile>\n" << std::endl;
    	std::cerr << "Usage = " << argv[0] << " -search <indexFile> <inputText> <patternFile>" << std::endl;
        exit(EXIT_FAILURE);
    }
    /**preprocess step**/
    initAllBitTables();
    initDebug();

    printf("Statistics for %u tree factor,  %u CSA sample rate and %u CSA Block Factor\n",
    		n_ary_complete_tree::tree_factor,CSA::sampleRate,CSA::blockFactor);
    if(strcmp(argv[1],"-c")==0){
        T = new Text(argv[2],std::ios::in | std::ios::binary);
        c = new CST_Navarro(T);
        Index = new Text(argv[3],std::ios::out | std::ios::binary);
        c->serialize(Index->getFile());
        delete Index;
    }

    else if(strcmp(argv[1],"-r")==0){
            T = new Text(argv[2],std::ios::in | std::ios::binary);
            c = new STSA_Navarro(T);
            Index = new Text(argv[3],std::ios::out | std::ios::binary);
            c->serialize(Index->getFile());
            delete Index;
    }

    else if(strcmp(argv[1],"-load")==0){
    	std::cout << "Loading index from " <<  argv[2] << "\n";
    	Index = new Text(argv[2],std::ios::in | std::ios::binary);
    	Index->reset();
    	c =  new CST_Navarro(Index->getFile());
    	delete Index;
    }
//    else if(strcmp(argv[1],"-test")==0){
//    	srand(time(NULL));
//    	ESA* r;
//    	CESA* c;
//    	CESAI* ci;
//    	T = new Text(argv[2],std::ios::in | std::ios::binary);
//       	r = new ESA(T);
//       	T->reset();
//    	c = new CESA(T);
//    	T->reset();
//    	ci = new CESAI(T);
//    	for(word i=0;i<r->getSize();i++){
////    		word k = rand() % c->getSize();
//    		printf("LCP[%u] = RawLCP %u =  CesaLcp = %u Cesailcp = %u\n",i,r->accessLCP(i),c->accessLCP(i),ci->accessLCP(i));
//    		if((ci->accessLCP(i)!=r->accessLCP(i)) || (c->accessLCP(i)!=r->accessLCP(i))){
//    			printf("DEU MERDA CAPITAO\n");
//    			exit(EXIT_FAILURE);
//    		}
//    	}
//    }
    else{
    	std::cerr << "Usage = " << argv[0] << "-create <inputText> <indexFile>\n" << std::endl;
    	std::cerr << "Usage = " << argv[0] << " -search <indexFile> <inputText> <patternFile>" << std::endl;
        exit(EXIT_FAILURE);
    }
    std::cout << "Finished" << std::endl;
    return(0);
}

