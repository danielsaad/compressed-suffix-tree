/*
 * readAlignment.cpp
 *
 *  Created on: Dec 18, 2012
 *      Author: daniel
 */


#include <cstring>
#include <cstdio>
#include <iostream>
#include <queue>
#include <vector>
#include "text.hpp"
#include "SA/CST_Navarro.hpp"
#include "SA/STSA.hpp"
#include "LCP/LCP.hpp"
#include <utility>
#include <algorithm>
#include <utility>
#include "bit.hpp"

using namespace std;


std::vector<node> match(AbstractSTSA_RMQ* A,const std::string& read,word errors=0){
	node tree_node = A->root();
	pair<node,word> vertex;
	vector<node> v;
	std::queue<pair<node,word>> q;
	vector<node> solutions;
	string edge;
	word err=0;
	word errTemp;
	word k;

	vertex = make_pair(tree_node,err);
	q.push(vertex);
	while(q.empty()==false){
		vertex = q.front();
		tree_node = vertex.first;
		err = vertex.second;
		q.pop();
		if(tree_node.valid()==false) continue;
		if(A->isLeaf(tree_node)) continue;

		v = A->children(tree_node);

//		printf("Size = %lu\n",v.size());

		for(word i=0;i<v.size();i++){
			edge = A->edge(tree_node,v[i], read.size()-tree_node.depth);
			errTemp = err;
			for(k=0;k<edge.size() && tree_node.depth+k < read.size();k++){
				if(read[tree_node.depth+k]!=edge[k]){
					errTemp++;
				}
			}
			if(tree_node.depth+k == read.size() && errTemp<=errors){
				solutions.push_back(v[i]);
			}
			else if(k==edge.size() && errTemp<= errors){
				vertex = make_pair(v[i],errTemp);
				q.push(vertex);
			}
		}
	}
	return(solutions);
}


void outputMatch(AbstractSTSA_RMQ* A, Text* readsTxt, std::ofstream& output,word errors=0){
	std::string read;
	std::vector<node> intervals;
	word occ=0;
	while(((readsTxt->getFile()).eof())==false){
		getline(readsTxt->getFile(),read);
		std::cout << "Processing Read = " << read;
		intervals = match(A,read,errors);
		occ=0;
		for(word i=0;i<intervals.size();i++){
			occ+= intervals[i].r - intervals[i].l + 1;
		}
		cout << ".\t" << occ << " Occurrences found" << endl;
		output << occ << " Occurrences found for read " << read << ":\t";
//		for(word i=0;i<intervals.size();i++){
//			for(word k=intervals[i].l;k<=intervals[i].r;k++){
//				output << A->locate(k) << "\t";
//			}
//		}
		output << "\n";
	}
	read.clear();
}


/** Argv[1] Mode**/
/** Argv[2] Index File**/
/** Argv[3] Reads File**/
/** Argv[4] Output File**/
/** Argv[5] number of errors**/
//
int main(int argc, char** argv){
	initAllBitTables();
	int errors = atoi(argv[5]);
	AbstractSTSA_RMQ* A;
	Text* index = new Text(argv[2],std::ios::in | std::ios::binary);
	Text* readsTxt = new Text(argv[3],std::ios::in);
	std::ofstream output;
	output.open(argv[4],std::ios::out | std::ios::trunc);
	if((strcmp(argv[1],"-c")==0)){
		std::cout << "Compressed option detected\n";
		A = new CST_Navarro(index->getFile());
	}
	else{
		std::cout << "Raw option detected\n";
		A = new STSA_Navarro(index->getFile());
//		A->setText(T);
//		print(A);
	}
//
//	print(A);
//	printf("RMQ[0,size]  = %u\n",A->query(0,A->getSize()-1));
//	printf("RMQ[1,size-1]  = %u\n",A->query(1,A->getSize()-1));
//	printf("RMQ[1310792,size-1]  = %u\n",A->query(1310792,A->getSize()-1));
//	printf("RMQ[2620528,size-1]  = %u\n",A->query(2620528,A->getSize()-1));
//	printf("RMQ[3930878,size-1]  = %u\n",A->query(3930878,A->getSize()-1));
	outputMatch(A,readsTxt,output,errors);
//	delete A;
	return(0);
}
