/*
 * Benchmark.cpp
 *
 *  Created on: Jan 8, 2013
 *      Author: daniel
 */

/**
 * argv[1] = mode
 * argv[2] = index
 * argv[3] = benchmark
 */

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include "../Bitvector/bit.hpp"
#include "../SA/AbstractSTSA_RMQ.hpp"
#include "../SA/STSA.hpp"
#include "../SA/CST_Navarro.hpp"

const word operationsSA =  100000; /**0**/
const word operationsLCP = 100000; /**1**/
const word operationsRMQ = 100000; /**2**/
const word operationsNSV = 100000; /**3**/
const word operationsPSV = 100000; /**4**/
const word operationsLCA = 100000; /**5**/

int main(int argc,char* argv[]){
	AbstractSTSA_RMQ* st;
	Text* index = new Text(argv[2],std::ios::in | std::ios::binary);
	word option = atoi(argv[3]);
	time_t start;
	time_t end;
	word i,j;
	word size;
	node u,v;
	srand(time(NULL));
	if(strcmp(argv[1],"-r") == 0){
		st =  new STSA_Navarro(index->getFile());
	}
	else if(strcmp(argv[1],"-c") == 0){
		initAllBitTables();
		st = new CST_Navarro(index->getFile());
	}
	else{
		printf("Mal formed expression\n");
		exit(EXIT_FAILURE);
	}

	printf("Benchmark initialized\n");

	size = st->getSize();
	if(option==0){
		printf("Accessing %u SA entries\n",operationsSA);
		time(&start);
		for(word k=0;k<operationsSA;k++){
			i = rand()%size;
			st->accesssa(i);
		}
		time(&end);
		printf("Took %.2lf seconds",difftime(end,start));
	}
	else if(option==1){
		printf("Accessing %u LCP entries\n",operationsLCP);
		time(&start);
		for(word k=0;k<operationsLCP;k++){
			i = rand()%size;
			st->accesslcp(i);
		}
		time(&end);
		printf("Took %.2lf seconds",difftime(end,start));
	}
	else if(option==2){
		printf("Doing %u RMQ queries\n",operationsRMQ);
		time(&start);
		for(word k=0;k<operationsRMQ;k++){
			i = rand()%size;
			j = rand()%size;
			st->query(i,j);
		}
		time(&end);
		printf("Took %.2lf seconds",difftime(end,start));
	}
	else if(option==3){
		printf("Doing %u NSV queries\n",operationsNSV);
		time(&start);
		for(word k=0;k<operationsPSV;k++){
			i = rand()%size;
			st->nsv(i);
		}
		time(&end);
		printf("Took %.2lf seconds",difftime(end,start));
	}
	else if(option==4){
		printf("Doing %u PSV queries\n",operationsPSV);
		time(&start);
		for(word k=0;k<operationsPSV;k++){
			i = rand()%size;
			st->psv(i);
		}
		time(&end);
		printf("Took %.2lf seconds",difftime(end,start));
	}
	else if(option==5){
			printf("Doing %u LCA queries among leaves\n",operationsLCA);
			time(&start);
			for(word k=0;k<operationsLCA;k++){
				i = rand()%size;
				j = rand()%size;
				u.l=i;
				u.r=i;
				v.l = j;
				v.r = j;
				st->lca(u,v);
			}
			time(&end);
			printf("Took %.2lf seconds",difftime(end,start));
		}
	else{
		printf("Mal formed expression\n");
		exit(EXIT_FAILURE);
	}
}


