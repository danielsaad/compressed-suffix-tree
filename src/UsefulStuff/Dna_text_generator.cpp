/*
 * Dna_text_generator.cpp
 *
 *  Created on: Jan 3, 2013
 *      Author: daniel
 */




#include <cstdio>
#include <ctime>
#include <cstdlib>
#include <cstring>

/**
 * Argv[1] size in MB
 * Argv[2] name of file
 */

int main(int argc,char** argv){
	char* alphabet = "ACTG";
	int alphabet_size = 4;
	int size;
	srand(time(NULL));
	FILE* fp = fopen(argv[2],"w");
	if(fp==NULL) exit(EXIT_FAILURE);
	size = atoi(argv[1])*1024*1024;
	for(int i=0;i<size;i++){
		fputc(alphabet[rand() % alphabet_size],fp);
	}
	fputc('$',fp);
	return(0);
}
