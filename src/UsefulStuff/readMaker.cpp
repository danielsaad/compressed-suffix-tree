/*
 * readMaker.cpp
 *
 *  Created on: Jan 3, 2013
 *      Author: daniel
 */


/**
 * argv[1] read length
 * argv[2] text
 * argv[3] read file
 * argv[4] number of errors
 */
#include <cstdio>
#include <cstdlib>

int main(int argc,char** argv){
	FILE *in,*out;
	int read_length = atoi(argv[1]);
	int n_errors = 0;
	int n;
	char* txt;

	in = fopen(argv[2],"r");
	out = fopen(argv[3],"w");
	if(in==NULL || out == NULL) exit(EXIT_FAILURE);

	fseek(in,0,SEEK_END);
	n = ftell(in)-1;
	fseek(in,0,SEEK_SET);
	txt = new char[n];
	fread(txt,sizeof(char),n,in);

	for(int i=0;i<n-read_length;i++){
		for(int k=0;k<read_length;k++){
			fprintf(out,"%c",txt[i+k]);
		}
		if(i!=n-read_length-1) fprintf(out,"%c",'\n');
	}
	return(0);
}
