#include <cmath>
#include "utils.hpp"

/**
    @file Provides some utility functions
**/


bool SuffixStructure::operator<(const SuffixStructure& other) const{
     if(P < other.P){
        return(true);
    }
    else if(P > other.P){
        return(false);
    }
    else{
        if(Q < other.Q){
            return(true);
        }
        return (false);
    }
}


double log(double n, double base){
	return(log(n)/log(base));
}
