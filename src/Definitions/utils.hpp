#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

#include "types.hpp"

class SuffixStructure{
public:
    word index;
    word P;
    word Q;
    bool operator<(const SuffixStructure& other) const;
};


double log(double n,double base);


#endif // UTILS_H_INCLUDED

