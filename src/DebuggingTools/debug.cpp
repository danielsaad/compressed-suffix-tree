#include <cstdlib>
#include "debug.hpp"

/**
    @file This file provides some debug resources
**/

/**
    @var the file in which the debug messages will be writtedn
**/
FILE* logFile;

/**
    nop
**/
void nop(void){
    do{}while(0);
}


/**
    Initializes the debug environment,
    opens the file
**/
void initDebug(void){
    logFile = fopen("log.txt","w");
    if(logFile==NULL){
        perror("Error at opening the log file");
        exit(EXIT_FAILURE);
    }
}
