/*
 * AbstractESA.cpp
 *
 *  Created on: Dec 13, 2012
 *      Author: daniel
 */


#include "AbstractESA.hpp"



AbstractESA::~AbstractESA(){
	delete lcp;
}

AbstractLcp* AbstractESA::getLcp(){
	return lcp;
}

void AbstractESA::setLcp(AbstractLcp* lcp){
	this->lcp = lcp;
}

void AbstractESA::serialize(std::ostream& output){
	lcp->serialize(output);
}

AbstractESA::AbstractESA(Text* T): AbstractSA(T){}

AbstractESA::AbstractESA(std::istream& input): AbstractSA(input){}

word AbstractESA::accessLCP(const word i){
	return(lcp->access(i));
}
