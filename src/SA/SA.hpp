/*
 * SA.hpp
 *
 *  Created on: Dec 13, 2012
 *      Author: daniel
 */

#ifndef SA_HPP_
#define SA_HPP_

#include <iostream>
#include <divsufsort.h>
#include "AbstractSA.hpp"
#include "../Text/text.hpp"
class SA;


//class iSA: public serializable{
//public:
//    virtual integer operator[](const integer i) = 0;
//    virtual integer getSize() = 0;
//    virtual ~iSA(){}
//};
//class iISA : public serializable{
//public:
//    virtual integer operator[](const integer i) = 0;
//    virtual integer getSize() = 0;
//    virtual ~iISA(){}
//};



//class LibdivsufsortSA: public iSA{
//public:
//    LibdivsufsortSA(Text& T);

//    virtual integer operator[](const integer i) = 0;
//    virtual integer getSize() = 0;
//    virtual void serialize(std::ostream &output);

//    virtual ~LibdivsufsortSA();

//    Text* _text;
//private:
//    integer* _sa;
//    integer _size;
//};

//class ISA: public iISA{
//public:
//    ISA(iSA* sa);

//    virtual integer operator[](integer i);
//    virtual integer getSize();
//    virtual void serialize(std::ostream &output);

//    virtual ~ISA();

//    Text& _text;
//private:
//    integer* _isa;
//    integer _size;
//};

//class ISA_SemiExternal: public iISA{
//public:
//    ISA_SemiExternal(iSA *sa);

//    virtual integer operator [](integer i);
//    virtual integer getSize();
//    virtual void serialize(std::ostream &output);

//    virtual ~ISA_SemiExternal();

//    Text* _text;
//private:
//    integer* _isa;
//    integer _size;
//};


class rawSA:public abstractSAContainer{
public:
	rawSA(SA* A);
	rawSA(std::istream& input);
	virtual word access(const word i);
	virtual void serialize(std::ostream& output);
	virtual ~rawSA();
private:
    saidx_t* array;
};


class rawISA:public abstractSAContainer{
public:
	rawISA(SA* A);
	rawISA(std::istream& input);
	virtual word access(const word i);
	virtual void serialize(std::ostream& output);
	virtual ~rawISA();
private:
    saidx_t* array;
};


/**
    Raw suffix array built with libdivsufsort
**/
class SA: public virtual AbstractSA{
public:
    SA(Text* T);
    SA(std::istream& input);
    virtual ~SA();
    virtual byte accessSymbol(const word i);
    sauchar_t* getText();
    virtual void serialize(std::ostream& output);
private:
    void build();
    sauchar_t *textStr;
};

class SAI: public AbstractSAI,public SA{
public:
	SAI(Text* T);
	SAI(std::istream& input);
	virtual void serialize(std::ostream& output);
	virtual ~SAI();
	virtual byte accessSymbol(const word i){return SA::accessSymbol(i);} //maldito eclipse
};


#endif /* SA_HPP_ */
