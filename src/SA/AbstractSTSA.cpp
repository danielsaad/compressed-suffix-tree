/*
 * AbstractSTSA.cpp
 *
 *  Created on: Dec 13, 2012
 *      Author: daniel
 */



#include "AbstractSTSA.hpp"



AbstractSTSA::AbstractSTSA(Text* T){}


AbstractSTSA::AbstractSTSA(std::istream& input){}

AbstractSTSA::~AbstractSTSA(){
	delete enhancedSuffixArray;
}

void AbstractSTSA::serialize(std::ostream& output){
	enhancedSuffixArray->serialize(output);
}




bool node::operator==(node other){
	if(other.l==l && other.r==r && other.depth==depth){
		return true;
	}
	return false;
}



bool node::valid(){
	if(l<=r) return true;
	return false;
}
void node::makeInvalidNode(){
	l = 1;
	r = 0;
	depth = 0;
}

node::node():l(0),r(0),depth(0){}

node::node(word l,word r,word depth){
	this->l = l;
	this->r = r;
	this->depth = depth;
}
