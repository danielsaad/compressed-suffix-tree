/*
 * AbstractESA.hpp
 *
 *  Created on: Dec 13, 2012
 *      Author: daniel
 */

#ifndef ABSTRACTESA_HPP_
#define ABSTRACTESA_HPP_

#include "AbstractSA.hpp"
#include "../LCP/LCP.hpp"

/**
    An abstract class for Enhanced suffix array Based Classes
**/

class AbstractESA: public virtual AbstractSA{
public:
	AbstractESA(Text* T);
	AbstractESA(std::istream& input);
	virtual ~AbstractESA();
	word accessLCP(const word i);
    AbstractLcp* getLcp();
    void setLcp(AbstractLcp* lcp);
    void serialize(std::ostream& output);
protected:
    AbstractLcp* lcp;
};



#endif /* ABSTRACTESA_HPP_ */
