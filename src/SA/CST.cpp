/*
 * CST.cpp
 *
 *  Created on: Dec 13, 2012
 *      Author: daniel
 */

#include "CST.hpp"
#include "../RMQ/RMQ_succinct.hpp"
#include "../NSV/NSV_Naive.hpp"

CST::CST(std::istream& input):AbstractSTSA_RMQ(input){
	enhancedSuffixArray = new CESA(input);
	RMQInfo =  new RMQ_succinct(input);
	RMQInfo->setLcp(enhancedSuffixArray->getLcp());
	NSVInfo = new NSV_Naive(input);
	NSVInfo->setLcp(enhancedSuffixArray->getLcp());
}

CST::CST(Text* T):AbstractSTSA_RMQ(T){
	enhancedSuffixArray = new CESA(T);
	RMQInfo = new RMQ_succinct(enhancedSuffixArray->getLcp(),enhancedSuffixArray->getSize());
	NSVInfo = new NSV_Naive(enhancedSuffixArray->getLcp(),enhancedSuffixArray->getSize());
}


CST::~CST(){}

void CST::serialize(std::ostream& output){
	AbstractSTSA_RMQ::serialize(output);
}
