/*
 * CESA.cpp
 *
 *  Created on: Dec 13, 2012
 *      Author: daniel
 */

#include "CESA.hpp"
#include "../LCP/RiceCodedLCP.hpp"
CESA::CESA(std::istream& input):AbstractSA(input),AbstractESA(input),CSA(input){
	lcp = new RiceCodedLcp(input);
	lcp->setSA(this);
	time(&end);
}

//CESA::CESA(std::istream& input,CESAI* saPtr):AbstractSA(input),AbstractESA(input),CSA(input){
//	lcp = new RiceCodedLcp(input);
//	lcp->setSA(saPtr);
//	time(&end);
//}

void CESA::serialize(std::ostream& output){
	std::cout << "Serializing Compressed Enhanced Suffix Array\n";
	CSA::serialize(output);
	AbstractESA::serialize(output);
}



CESA::CESA(Text* T):AbstractSA(T),AbstractESA(T),CSA(T){
	std::cout << "Building Compressed Enhanced Suffix Array\n";
    lcp = new RiceCodedLcp(this);
    time(&end);
    std::cout << "Compressed Enhanced Suffix Array Built in " << getBuildTime() << "s\n";
}



//
//CESA::CESA(Text* T,CESAI* saPtr):AbstractSA(T),AbstractESA(T),CSA(T){
//	std::cout << "Building Compressed Enhanced Suffix Array\n";
//    lcp = new RiceCodedLcp(saPtr);
//    time(&end);
//    std::cout << "Compressed Enhanced Suffix Array Built in " << getBuildTime() << "s\n";
//}

CESA::~CESA(){}




CESAI::CESAI(Text* T):AbstractSA(T),AbstractSAI(T),CESA(T){
    inverseSuffixArray = new CompressedISA(this);
    time(&end);
    std::cout << "Compressed Enhanced Suffix Array Built in " << getBuildTime() << "s\n";
}


CESAI::CESAI(std::istream& input):AbstractSA(input),CESA(input),AbstractSAI(input){
	inverseSuffixArray = new CompressedISA(input);
	inverseSuffixArray->setSA(this);
}
void CESAI::serialize(std::ostream& output){
	CESA::serialize(output);
	AbstractSAI::serialize(output);
}

byte CESAI::accessSymbol(const word i){
	static word textSymbol = 0;
	static word psiValue = accessPsi(0);
	word inverseSA;
	byte symbol;
	if(i==textSymbol){
		symbol = getStartSymbol(psiValue);
		psiValue = accessPsi(psiValue);
		textSymbol++;
	}
	else{
		inverseSA = accessSAI(i);
		symbol = getStartSymbol(inverseSA);
		textSymbol = i+1;
		psiValue = accessPsi(inverseSA);
	}
	return(symbol);
}


 CESAI::~CESAI(){}
