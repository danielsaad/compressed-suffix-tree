/*
 * CSA.cpp
 *
 *  Created on: Dec 13, 2012
 *      Author: daniel
 */



#include <algorithm>
#include <cerrno>
#include <cstring>
#include <cstdio>
#include <ctime>
#include "../StaticDictionaries/GMN_StaticDictionary.hpp"
#include "../Definitions/utils.hpp"
#include "../SuffixSorting/suffixSort.hpp"
#include "CSA.hpp"


/**
    @file This file provides an implementation of the compressed enhanced suffix array.
    We have used Lam.et.al incremental algorithm which runs in O(n log n) time in theory and
    O(n) working space. However, our implementation does not run in O(n log n) time provided that
    select and rank queries does not take O(1).
**/



CompressedISA::CompressedISA(AbstractSA* A):abstractSAContainer(A){
    word i, suffixValue,rank;
    CSA* compressedSuffixArray = dynamic_cast<CSA*> (A);
    AbstractPsi* psi = compressedSuffixArray->getPsi();
    n = compressedSuffixArray->getSize();
    words = 1+ floor(n/CSA::sampleRate);
    array = new word[words];
    A = compressedSuffixArray;
    suffixValue = 0;
    rank = psi->access(0);
    array[suffixValue] = psi->access(0);
    for(i=1;i<n;i++){
        rank = psi->access(rank);
        suffixValue = (suffixValue+1) % n;
        if(suffixValue % CSA::sampleRate==0){
            array[suffixValue/CSA::sampleRate]= rank;
        }
    }
}


CompressedISA::CompressedISA(std::istream& input):abstractSAContainer(input){
	input.read(reinterpret_cast<char*> (&words),sizeof(words));
	array = new word[words];
	input.read(reinterpret_cast<char*>(array),sizeof(word)*words);
}


CompressedISA::~CompressedISA(){
	delete[] array;
}

word CompressedISA::access(const word i){
	word t;
	word rank;
	CSA* CSAPtr = dynamic_cast<CSA*> (this->A);
	AbstractPsi* psi = CSAPtr->getPsi();
	if(i%CSA::sampleRate==0){
		return(array[i/CSA::sampleRate]);
	}
	else{
		rank = array[i/CSA::sampleRate];
		for(t=0;t< (i%CSA::sampleRate) ;t++){
			rank = psi->access(rank);
		}
		return(rank);
	}
}


void CompressedISA::serialize(std::ostream& output){
	abstractSAContainer::serialize(output);
	output.write(reinterpret_cast<const char*> (&words),sizeof(words));
	output.write(reinterpret_cast<const char*>(array),sizeof(word)*words);
}




CompressedSA::CompressedSA(AbstractSA* A):abstractSAContainer(A){
    word i, suffixValue,rank;
    CSA* compressedSuffixArray = dynamic_cast<CSA*> (A);
    AbstractPsi* psi = compressedSuffixArray->getPsi();
    n = compressedSuffixArray->getSize();
    words = 1+ floor(n/CSA::sampleRate);
    array = new word[words];
    array[0] = n-1;
    A = compressedSuffixArray;
    suffixValue = n-1;
    rank = 0;
    for(i=1;i<n;i++){
        rank = psi->access(rank);
        suffixValue = (suffixValue+1) % n;
        if(rank % CSA::sampleRate==0){
            array[rank/CSA::sampleRate]= suffixValue;
        }
    }
}

word CompressedSA::access(const word i){
	word t;
	word rank;
	CSA* CSAPtr = dynamic_cast<CSA*> (this->A);
	AbstractPsi* psi = CSAPtr->getPsi();
	if(i%CSA::sampleRate==0){
		return(array[i/CSA::sampleRate]);
	}
	else{
		t=0;
		rank = i;
		while(rank%CSA::sampleRate!=0){
			rank = psi->access(rank);
			t++;
		}
		if(t>array[rank/CSA::sampleRate]){
			return(n - array[rank/CSA::sampleRate] + t );
		}
		return(array[rank/CSA::sampleRate] - t);
	}
}

CompressedSA::CompressedSA(std::istream& input):abstractSAContainer(input){
	input.read(reinterpret_cast<char*> (&words),sizeof(words));
	array = new word[words];
	input.read(reinterpret_cast<char*>(array),sizeof(word)*words);
}

void CompressedSA::serialize(std::ostream& output){
	abstractSAContainer::serialize(output);
	output.write(reinterpret_cast<const char*> (&words),sizeof(words));
	output.write(reinterpret_cast<const char*>(array),sizeof(word)*words);
}


/**
    Returns the maximum r, such that Psi(r)<= rankNextSuffix.
    Returns the rank of the current long suffix among all the short
    suffixes. Used a Dynamic programming algorithm for achieve the goal.
    @param rankNextSuffix the rank of the immediately near long suffix among all other short suffixes.
    @param ch the symbol which the current long suffix start with.
    @return return the rank of the long suffix among all short suffixes.
**/
word CSA::rankBsearch(word rankNextSuffix,word ch){
    word rank=0;
    word m;
    word tempRank;
    word l,r;
    l = c[ch];
    r = c[ch+1]-1;
    rank = c[ch];
    /**Binary search**/
    while(l<r){
        m = (l+r)/2;
        tempRank = psi->access(m);
        if(tempRank<rankNextSuffix){
            rank=m+1;
            l=m+1;
        }
        else if (tempRank==rankNextSuffix){
            rank = m;
            l=m+1;
        }
        else{
            r=m-1;
        }
    }
    if(l==r){
        tempRank = psi->access(l);
         if(tempRank<rankNextSuffix){
            rank = l+1;
        }
        else if(tempRank==rankNextSuffix){
            rank = l;
        }
    }
    return(rank);
}




/**
    Computes the rank of the long suffix among all the short suffixes
    Stores in the L array.
    @param currentBlockSize the current block size.
**/
void CSA::computeRankAmongShortSuffixes(word currentBlockSize){
    word ch;
    word i;
    L=&WM[2*blockSize];
    LSorted = &WM[3*blockSize];
    for(i=0;i<=ALPHABET_SIZE;i++) cTemp[i]=0;
    i = currentBlockSize;
    L[i]=psi->access(0);
    while(i!=0){
        i--;
        /**Compute a binary Search**/
        ch=currentTextBlock[i];
        (cTemp[ch+1])++;
            L[i] = rankBsearch(L[i+1],ch);
        LSorted[i]=L[i]; /**copy elements**/
    }
    std::sort(LSorted,LSorted+currentBlockSize);
}

/**
    Computer the rank of long suffixes among themselves. Store the
    result in the M array. For that we use a suffix sorting stage
    in the current block and finally we sort the tuples generated,
    as described in Lam et al work.
    @param CSA the compressed suffix array.
    @param currentBlockSize the current block size.
**/
void CSA::computeRankAmongLongSuffixes(word currentBlockSize){

    word i;
    integer* SA; /**suffix array of the considered block**/
    integer* SAI; /**suffix array of the considered block**/
    SuffixStructure* tuples;

    SAI = (integer*) WM;
    SA = (integer*) &WM[blockSize*2+1];
    for(i=0;i<currentBlockSize;i++){
        SAI[i] = (word) currentTextBlock[i];
    }

    for(i=currentBlockSize;i<currentBlockSize*2;i++){
        SAI[i] = (word) previousTextBlock[i-currentBlockSize];
    }
    suffixSort((integer*)SAI,(integer*)SA,currentBlockSize*2,ALPHABET_SIZE,1,currentBlockSize);

    /**FILL P and Q **/
    tuples = (SuffixStructure*) &WM[blockSize];
    tuples[0].Q = psi->access(0);
    tuples[0].P = SAI[0];
    tuples[0].index = 0;
    for(i=1;i<currentBlockSize;i++){
        tuples[i].index = i;
        tuples[i].P = SAI[i];
        tuples[i].Q = psi->access(tuples[i-1].Q);
    }
    /**Sort structure**/
    std::sort(tuples,tuples+currentBlockSize);
    /**set M and MI**/
    M=WM;
    MI = &WM[blockSize];
    for(i=0;i<currentBlockSize;i++) M[tuples[i].index] = i;
    for(i=0;i<currentBlockSize;i++) MI[M[i]] = i;
}

/**
    Computer the new psi function now involving the long suffixes.
    @param currentBlockSize the current block size
**/

void CSA::computeNewPsi(word currentBlockSize){
    word i,k,it;
    word currentSymbol; /**Current symbol (the first symbol in the suffixes being considered)**/
    word symbolIndex=0; /**The index of the read/written new/old psi values**/
    word rank0,rank1,rank;
    word lastSetBit;
    word newSymbolsN;
    word suffix;
    word psiValue;
    word zeroValue;
    word oldSymbolsN;
    word* oldPsiValues;
    word basePosition=0;
    V->reset();
    for(i=0;i<blockSize;i++){
        M[i]+=L[i];
    }
    /**configure new psi**/
    for(i=1;i<=ALPHABET_SIZE;i++) cTemp[i] += cTemp[i-1];
    for(i=0;i<=ALPHABET_SIZE;i++) cTemp[i] += c[i];

    V->setBit(LSorted[0]);
    lastSetBit = LSorted[0];
    for(i=1;i<currentBlockSize;i++){
        V->setBit(lastSetBit+LSorted[i]-LSorted[i-1]+1);
        lastSetBit += LSorted[i]-LSorted[i-1]+1;
    }

    oldPsiValues = L;

    V->preprocess();
    /**initializations**/
    zeroValue = psi->access(0);
    rank0 = 1;
    rank1 = 0;

    /**Base case**/
    for(i=0;i<ALPHABET_SIZE+1;i++){
        oldSymbolsN = c[i];
        c[i] = cTemp[i];
        cTemp[i] = oldSymbolsN;
    }
    psi->configure(getStartSymbol(0));
    psi->set(0,M[0]);
    currentSymbol = getStartSymbol(0);

    for(i=1;i<c[ALPHABET_SIZE];i++){
        /**sets up current symbol**/
        while(c[currentSymbol+1]<=i && currentSymbol<ALPHABET_SIZE){
            currentSymbol++;
            newSymbolsN = c[currentSymbol+1] - c[currentSymbol];
            if(newSymbolsN>0){
            	tempPsiFile.clear();
                tempPsiFile.seekg(0,std::ios_base::beg);
                tempPsiFile.seekp(0,std::ios_base::beg);
                oldSymbolsN = cTemp[currentSymbol+1] - cTemp[currentSymbol];
                symbolIndex=0;
                /**Set up psi configuration**/
                basePosition=0;
                while(oldSymbolsN-basePosition>0){
                    it = std::min(2*blockSize,oldSymbolsN-basePosition);
                    for(k=0;k<it;k++){
                        rank = psi->accessSequential(currentSymbol);
                        tempPsiFile.write(reinterpret_cast<const char*>(&rank),sizeof(word));
                    }
                    basePosition+=it;
                }
            	tempPsiFile.clear();
                tempPsiFile.seekg(0,std::ios_base::beg);
                tempPsiFile.seekp(0,std::ios_base::beg);
                basePosition=0;
                psi->configure(currentSymbol);
            }
        }

        /**Set psi value according to the conditions**/
        if(V->readBit(i)==0){ /**Is a short suffix**/
            if((rank0-cTemp[currentSymbol])%(2*blockSize)==0){
                it = std::min(2*blockSize,oldSymbolsN-basePosition);
                tempPsiFile.read(reinterpret_cast<char*> (oldPsiValues),sizeof(word)*it);
                basePosition= rank0-cTemp[currentSymbol];
            }
            rank = oldPsiValues[rank0-cTemp[currentSymbol]-basePosition];
            psiValue = V->select0(rank+1);
            rank0++;
        }
        else{ /**Is a long suffix**/
            suffix = MI[rank1];
            if(suffix<currentBlockSize-1){
                psiValue = M[suffix+1];
            }
            else{
                rank = zeroValue+1;
                psiValue = V->select0(rank);
            }
            rank1++;
        }
        psi->setSequential(currentSymbol,psiValue);
        symbolIndex++;
    }
    for(i=0;i<ALPHABET_SIZE;i++){
        psi->postConfigure(i);
    }
    /**Preprocess the quotient values**/
}

byte CSA::accessSymbol(const word i){
	return(T->readSymbol(i));
}

/**
    Constructor for the CSA data structure. Build the compressed suffix array
    @param T The text which the CSA will be built upon.
**/
CSA::CSA(Text* T):AbstractSA(T){
	time(&start);
	std::cout << "Building Compressed Suffix Array\n";
    const char* tempName = "tempFile.idx";
    c = new word[ALPHABET_SIZE+1];
    cTemp = new word[ALPHABET_SIZE+1];
    psi = new RiceCodedPsi(this);
    this->n = T->getLength();
    suffixArray = 0;

    blockSize = computeBlockSize();
    WM = new word[(blockSize+1)*4]; /**Working memory**/
    V = new GMN_StaticDictionary(T->getLength()); /**V[i] = 1 if the ith suffix is a long suffix, short suffix otherwise**/
    textWorkingMemory = new byte[blockSize*2];
    currentTextBlock = textWorkingMemory;
    previousTextBlock = &textWorkingMemory[blockSize];
    tempPsiFile.open(tempName,std::ios::out | std::ios::in | std::ios::binary | std::ios::trunc);
    if(!tempPsiFile){
    	std::cerr << "Error: " << strerror(errno) << std::endl;
        exit(EXIT_FAILURE);
    }
    build();
    suffixArray = new CompressedSA(this);
    /**Clean the mess**/
    delete[] WM;
    delete V;
    delete[] textWorkingMemory;
    delete[] cTemp;

    tempPsiFile.close();
    remove(tempName);
    time(&end);
    std::cout << "Compressed Suffix Array built in " << getBuildTime() << "s\n";
}

CSA::~CSA(){
    delete [] c;
    delete psi;
}

void CSA::build(){

    word start,end,it,j;
    byte* textBlock;

    if(T->getLength() > blockSize){
    	it = T->getLength() - blockSize;
    	it = ceil((double)it/blockSize);
    }
    else{
    	it=0;
    }

    end = T->getLength()-1;
    start = blockSize > end ? 0 : end - blockSize + 1;
    currentBlockSize = end-start+1;
    T->readBuffer(currentTextBlock,start,currentBlockSize);
    /**BASE CASE**/
    baseCase(currentBlockSize);
    textBlock = previousTextBlock;
    previousTextBlock = currentTextBlock;
    currentTextBlock = textBlock;
//    for(j=0;j<currentBlockSize;j++){
//        printf("Psi[%u] = %u\n",j,getPsi()->access(j));
//    }
    printf("\n");
    for(word i=1;i<=it;i++){
        printf("i = %u it = %u\n",i,it);
        /**seting boundaries**/
        end=start-1;
        start = blockSize > end ? 0 : end - blockSize + 1;
        currentBlockSize = end - start +1;
        T->readBuffer(currentTextBlock,start,blockSize);

        /**Compute the rank of long suffixes among themselves**/
        computeRankAmongLongSuffixes(currentBlockSize);

        /**Compute rank of long suffixes among the short suffixes**/
        computeRankAmongShortSuffixes(currentBlockSize);

        /**COMPUTE PSI**/
        computeNewPsi(currentBlockSize);
   //     for(j=0;j<currentBlockSize*(i+1);j++){
     //       printf("Psi[%u] = %u\n",j,psi->access(j));
       // }
        //printf("\n");

        textBlock = previousTextBlock;
        previousTextBlock = currentTextBlock;
        currentTextBlock = textBlock;
    }
}

CompressedSA::~CompressedSA(){
	delete[] array;
}

/**
    Compute the base case of Lam et al incremental algorithm.
    @param currentBlockSize the current block size in the incremental algorithm
**/
void CSA::baseCase(word currentBlockSize){
    word i;
    integer *SA,*SAI;
    word *psiValues;
    word symbolCount,temp;
    SAI = (integer*) WM;
    SA = (integer*) &WM[blockSize+2];

    for(i=0;i<=ALPHABET_SIZE;i++){
        c[i]=0;
        cTemp[i]=0;
    }
    for(i=0;i<currentBlockSize;i++){
        SAI[i] = (byte) currentTextBlock[i];
        c[SAI[i]] = c[SAI[i]]+1;
    }

    SAI[i] = -1;
    symbolCount = c[0];
    c[0] = 0;

    for(i=1;i<=ALPHABET_SIZE;i++){
        temp = c[i];
        c[i] = c[i-1]+symbolCount;
        symbolCount = temp;
    }
    for(i=0;i<=ALPHABET_SIZE;i++){
        cTemp[i] = c[i];
    }
    suffixSort(SAI, SA,currentBlockSize,ALPHABET_SIZE,1,currentBlockSize);

    for(i=0;i<currentBlockSize;i++){
        SA[SAI[i]] = i;
    }
    for(i=0;i<ALPHABET_SIZE;i++){
        psi->configure(i);
    }
    psiValues = &WM[3*blockSize];
    psiValues[0] = SAI[0];

    for(i=1;i<currentBlockSize;i++){
        psiValues[i] = SAI[SA[i] + 1];
    }

    /**construct the psi bit vector**/
    for(i=0;i<currentBlockSize;i++){
        psi->set(i,psiValues[i]);
    }
    for(i=0;i<ALPHABET_SIZE;i++){
        psi->postConfigure(i);
    }
}



/**
    Get the start symbol of the ith
    suffix in the suffix array. Uses a binary search in the C array.
    @param i the input
    @return the start symbol of the ith suffix in the suffix array
**/
word CSA::getStartSymbol(word i){
    word l,r,ch,m;
    l = 0;
    r = ALPHABET_SIZE;
    ch  = l;
    while(l<r){
        m = (l+r)/2;
        if(c[m]<=i){
            l = m+1;
            ch = m;
        }
        else{
            r = m-1;
        }
    }
    if(l==r){
        if(c[l]<=i){
            ch = l;
        }
    }
    return(ch);
}

AbstractPsi* CSA::getPsi(){
    return(psi);
}


word CSA::getSymbolsQuantity(word currentSymbol){
    return(c[currentSymbol+1]-c[currentSymbol]);
}

word CSA::getLexicograpichSmallerSymbols(word currentSymbol){
    return(c[currentSymbol]);
}



CSA::CSA(std::istream& input):AbstractSA(input){
	std::cout << "Loading CSA\n";
	suffixArray = new CompressedSA(input);
	suffixArray->setSA(this);
	c = new word[ALPHABET_SIZE];
	input.read(reinterpret_cast<char*> (c),sizeof(word)*ALPHABET_SIZE);
	psi = new RiceCodedPsi(input);
	psi->setSA(this);
}

void CSA::serialize(std::ostream& output){
	std::cout << "Serializing Compressed Suffix Array\n";
	AbstractSA::serialize(output);
	output.write(reinterpret_cast<const char*> (c),sizeof(word)*ALPHABET_SIZE);
	psi->serialize(output);
}

word CSA::computeBlockSize(){
	return(std::max((word)1024,(word) floor(n/blockFactor)));
}

word CSA::accessPsi(word i){
	return psi->access(i);
}

CSAI::CSAI(std::istream& input):AbstractSA(input),AbstractSAI(input),CSA(T){
	inverseSuffixArray = new CompressedISA(input);
	inverseSuffixArray->setSA(this);
}

CSAI::CSAI(Text* T):AbstractSA(T),AbstractSAI(T),CSA(T){
	inverseSuffixArray = new CompressedISA(this);
	inverseSuffixArray->setSA(this);
}

void CSAI::serialize(std::ostream& output){
	CSA::serialize(output);
	AbstractSAI::serialize(output);
}

CSAI::~CSAI(){}

