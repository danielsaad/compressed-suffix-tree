/*
 * CST_Navarro.hpp
 *
 *  Created on: Dec 31, 2012
 *      Author: daniel
 */

#ifndef CST_NAVARRO_HPP_
#define CST_NAVARRO_HPP_

#include "AbstractSTSA_RMQ.hpp"
#include "../RMQ/RMQ_NSV_Navarro.hpp"



class CST_Navarro:public AbstractSTSA_RMQ{
public:
	CST_Navarro(std::istream& input);
	CST_Navarro(Text* T);
	virtual void serialize(std::ostream& output);
	virtual ~CST_Navarro();
protected:
	RMQ_NSV_Navarro* RMQ_NSV_info;
};


#endif /* CST_NAVARRO_HPP_ */
