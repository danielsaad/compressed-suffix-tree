/*
 * AbstractSTSA_RMQ.cpp
 *
 *  Created on: Jan 1, 2013
 *      Author: daniel
 */




#include "AbstractSTSA_RMQ.hpp"
#include "../SA/CESA.hpp"

AbstractSTSA_RMQ::~AbstractSTSA_RMQ(){
	delete RMQInfo;
	delete NSVInfo;
}

void AbstractSTSA_RMQ::serialize(std::ostream& output){
	AbstractSTSA::serialize(output);
	RMQInfo->serialize(output);
	NSVInfo->serialize(output);
}


AbstractSTSA_RMQ::AbstractSTSA_RMQ(Text* T):AbstractSTSA(T){}
AbstractSTSA_RMQ::AbstractSTSA_RMQ(std::istream& input):AbstractSTSA(input){}



node AbstractSTSA_RMQ::root(){
	node root_node(0,enhancedSuffixArray->getSize()-1,0);
	return(root_node);
}

bool AbstractSTSA_RMQ::isLeaf(node v){
	if(v.l==v.r) return true;
	return false;
}
node AbstractSTSA_RMQ::child(node u,byte c){
	node v;
	word lcpLength;

	if(isLeaf(u)==true){
		u.makeInvalidNode();
		return u;
	}

	v.l = u.l;
	v.r = RMQInfo->query(v.l,u.r-1);

	lcpLength = enhancedSuffixArray->accessLCP(v.r);
	while((v.l!=u.r) && (enhancedSuffixArray->accessLCP(v.r)==lcpLength)){
		if((enhancedSuffixArray->accessSymbol(enhancedSuffixArray->accessSA(v.l)+lcpLength)) == c){
				v.depth = depth(v);
				return v;
		}
		v.l = v.r+1;
		v.r = RMQInfo->query(v.l,u.r-1);
	}
	if((enhancedSuffixArray->accessSymbol(enhancedSuffixArray->accessSA(v.l)+lcpLength)) == c){
		v.r = u.r;
		v.depth = depth(v);
		return v;
	}
	v.makeInvalidNode();
	return (v);
}

std::vector<node> AbstractSTSA_RMQ::children(node u){
	std::vector<node> children_vector;
	word h_index;
	word accessedLCP;
	node v;
	if(isLeaf(u)==true){
		v.makeInvalidNode();
		children_vector.push_back(v);
		return(children_vector);
	}

	v.l=u.l;
	v.r = RMQInfo->query(v.l,u.r-1);
	v.depth = depth(v);
	children_vector.push_back(v);
	h_index = enhancedSuffixArray->accessLCP(v.r);
	accessedLCP = h_index;
	while(accessedLCP == h_index){
		v.l = v.r+1;
		v.r = RMQInfo->query(v.l,u.r-1);
		v.depth = depth(v);
		accessedLCP = enhancedSuffixArray->accessLCP(v.r);
		if(accessedLCP==h_index){
			children_vector.push_back(v);
		}
		else{
			break;
		}
	}
	if(v.l>v.r) v.l = v.r;
	v.r = u.r;
	v.depth = depth(v);
	children_vector.push_back(v);
	return(children_vector);
}

word AbstractSTSA_RMQ::depth(node u){
	if(isLeaf(u)){
		return(enhancedSuffixArray->getSize() - enhancedSuffixArray->accessSA(u.r));
	}
	else{
		return(enhancedSuffixArray->accessLCP(RMQInfo->query(u.l,u.r-1)));
	}
}

node AbstractSTSA_RMQ::parent(node v){
	node p;
	word k=0;
	if(v == root()){
		v.makeInvalidNode();
		return(v);
	}
	if(v.l>0){
		k = v.l-1;
	}
	else{
		k = v.r;
	}
	if(v.r<enhancedSuffixArray->getSize()-1){
		if(enhancedSuffixArray->accessLCP(v.r)>enhancedSuffixArray->accessLCP(k)){
			k = v.r;
		}
	}
	else{
		k = v.l;
	}
	p.l = NSVInfo->psv(k)+1;
	p.r = NSVInfo->nsv(k);
	p.depth = depth(p);
	return(p);
}

bool AbstractSTSA_RMQ::isAncestor(node u,node v){
	if(u.l<=v.r && u.r>=v.r) return true;
	return false;
}

node AbstractSTSA_RMQ::lca(node u,node v){
	word m;
	node ancestor;
	if(isAncestor(u,v)) return u;
	if(u.r >= v.l) return lca(v,u);


	m = RMQInfo->query(u.r,v.l-1);
	ancestor.depth = enhancedSuffixArray->accessLCP(m);
	ancestor.l = NSVInfo->psv(m)+1;
	ancestor.r = NSVInfo->nsv(m);
	return ancestor;
}




std::string AbstractSTSA_RMQ::edge(node u,node v,word lenght){
	std::string str;
	word saValue;
	lenght = std::min(lenght,v.depth-u.depth);
	saValue = enhancedSuffixArray->accessSA(v.l);
	for(word i=0;i<lenght;i++){
		str.push_back(enhancedSuffixArray->accessSymbol(saValue+u.depth+i));
	}
	return str;
}

word AbstractSTSA_RMQ::locate(word i){
	return(enhancedSuffixArray->accessSA(i));
}

/**
 * TODO: Override this in CST, STSA can't make suffix link queries yet
 */
node AbstractSTSA_RMQ::suffixLink(node u){
	node v;
	CESA* cesa = dynamic_cast<CESA*>(enhancedSuffixArray); /**TODO: Not Good!**/
	word l,r;
	word rmq;
	if(u==root()) return root();
	if(isLeaf(u)){
		v.l = cesa->accessPsi(u.l);
		v.r = v.l;
		v.depth = depth(v);
		return v;
	}
	l = cesa->accessPsi(u.l);
	r = cesa->accessPsi(u.r);
	rmq = RMQInfo->query(l,r-1);
	v.l = NSVInfo->psv(rmq)+1;
	v.r = NSVInfo->nsv(rmq);
	v.depth = depth(v);
	return(v);
}
