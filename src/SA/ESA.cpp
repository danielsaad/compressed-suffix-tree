/*
 * ESA.cpp
 *
 *  Created on: Dec 13, 2012
 *      Author: daniel
 */

#include "ESA.hpp"
#include "../LCP/RawLCP.hpp"

ESA::ESA(Text* T):AbstractSA(T),SA(T),AbstractESA(T){
	std::cout << "Building Raw Enhanced Suffix Array\n";
	lcp = new rawLcp(this);
	time(&end);
	std::cout << "Raw Suffix Array built in " << getBuildTime() << "s\n";
}

ESA::ESA(std::istream& input):AbstractSA(input),SA(input),AbstractESA(input){
	std::cout << "Loading Enhanced Suffix Array\n" << "\n";
	std::cout << "SA = " << suffixArray->access(0) << "\n";
	lcp = new rawLcp(input);
	lcp->setSA(this);
}



void ESA::serialize(std::ostream& output){
	std::cout << "Serializing Raw Enhanced Suffix Array\n";
	SA::serialize(output);
	AbstractESA::serialize(output);
}

ESA::~ESA(){}



ESAI::ESAI(Text* T):AbstractSA(T),AbstractSAI(T),ESA(T){
	inverseSuffixArray = new rawISA(this);
	inverseSuffixArray->setSA(this);
}
ESAI::ESAI(std::istream& input):AbstractSA(input),AbstractSAI(input),ESA(input){
	inverseSuffixArray = new rawISA(input);
	inverseSuffixArray->setSA(this);
}
void ESAI::serialize(std::ostream& output){
	ESA::serialize(output);
	AbstractSAI::serialize(output);
}

ESAI::~ESAI(){}
