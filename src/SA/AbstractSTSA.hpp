/*
 * AbstractSTSA.hpp
 *
 *  Created on: Dec 13, 2012
 *      Author: daniel
 */

#ifndef ABSTRACTSTSA_HPP_
#define ABSTRACTSTSA_HPP_

#include "../RMQ/RMQ.hpp"
#include "AbstractESA.hpp"
#include "../NSV/NSV.hpp"
#include <string>
#include <vector>
/**Suffix tree based on enhanced suffix array**/

class node{
public:
	node();
	node(word l,word r,word depth);
	word l,r; /**The left and right boundaries in respect to the suffix array**/
	word depth; /**The depth of the node (equivalently, the maximum preffix of the considered interval)**/
	bool operator==(node other);
	bool valid();
	void makeInvalidNode();
};

class AbstractSTSA{
public:
	AbstractSTSA(std::istream& input);
	AbstractSTSA(Text* T);
	virtual node root() = 0;
	virtual bool isLeaf(node u) = 0;
	virtual bool isAncestor(node u, node v) = 0;
	virtual word depth(node v) = 0;
	virtual node child(node v,byte c) = 0;
	virtual std::vector<node> children(node v) = 0;
	virtual node parent(node v) = 0;
	virtual node lca(node u,node v) = 0;
	virtual node suffixLink(node u) = 0;
	virtual void serialize(std::ostream& output);
	virtual word locate(word i) = 0;
	virtual std::string edge(node u,node v,word len = MAX_WORD) = 0;
    virtual ~AbstractSTSA();
public:
    AbstractESA* enhancedSuffixArray;
};


#endif /* ABSTRACTSTSA_HPP_ */
