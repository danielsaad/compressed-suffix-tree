/*
 * AbstractSTSA_RMQ.hpp
 *
 *  Created on: Jan 1, 2013
 *      Author: daniel
 */

#ifndef ABSTRACTSTSA_RMQ_HPP_
#define ABSTRACTSTSA_RMQ_HPP_

#include "AbstractSTSA.hpp"



/**RMQ Based suffix tree**/
class AbstractSTSA_RMQ:public AbstractSTSA{
public:
	AbstractSTSA_RMQ(std::istream& input);
	AbstractSTSA_RMQ(Text* T);
	virtual ~AbstractSTSA_RMQ();

	virtual node root();
	virtual bool isAncestor(node u,node v);
	virtual bool isLeaf(node);
	virtual node child(node v,byte c);
	virtual word depth(node u);
	virtual std::vector<node>  children(node v);
	virtual node parent(node v);
	virtual node lca(node u,node v);
	virtual node suffixLink(node u);
	virtual std::string edge(node u,node v,word len = MAX_WORD);

	virtual word accesssa(word i){ return enhancedSuffixArray->accessSA(i);}
	virtual word accesslcp(word i){ return(enhancedSuffixArray->accessLCP(i));}
	virtual word query(word x,word y){return(RMQInfo->query(x,y));} /**TODO: erase**/
	virtual word nsv(word x){return(NSVInfo->nsv(x));}
	virtual word psv(word x){return(NSVInfo->psv(x));}
	virtual word getSize(){
		return(enhancedSuffixArray->getSize());
	}

	virtual word locate(word i);
	virtual void serialize(std::ostream& output);
protected:
	RMQ* RMQInfo;
	NSV* NSVInfo;
};


#endif /* ABSTRACTSTSA_RMQ_HPP_ */
