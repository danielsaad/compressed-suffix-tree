/*
 * AbstractSA.cpp
 *
 *  Created on: Dec 13, 2012
 *      Author: daniel
 */


#include "AbstractSA.hpp"


abstractSAContainer::abstractSAContainer(AbstractSA* A):n(0){
	this->A = A;
}

abstractSAContainer::abstractSAContainer(std::istream& input):A(0){
	input.read(reinterpret_cast<char*> (&n),sizeof(n));
}

void abstractSAContainer::serialize(std::ostream& output){
	output.write(reinterpret_cast<const char*> (&n),sizeof(n));
}


void abstractSAContainer::setSA(AbstractSA* A){
	this->A = A;
}

//AbstractSA::AbstractSA(){}

word AbstractSA::getSize(){
    return(n);
}

AbstractSA::AbstractSA(Text* T){
	setText(T);
}

void AbstractSA::setText(Text* T){
	this->T = T;
}

AbstractSA::AbstractSA(std::istream& input){
	input.read(reinterpret_cast<char*> (&n),sizeof(n));
	T = new Text(input);
}

double AbstractSA::getBuildTime(){
	return(difftime(end,start));
}

void AbstractSA::serialize(std::ostream& output){
	output.write(reinterpret_cast<const char*> (&n),sizeof(n));
	T->serialize(output);
	suffixArray->serialize(output);
}

word AbstractSA::operator[](const word i){
	return(suffixArray->access(i));
}

word AbstractSA::accessSA(const word i){
	return(suffixArray->access(i));
}

AbstractSA::~AbstractSA(){
	delete suffixArray;
}


AbstractSAI::AbstractSAI(std::istream& input):AbstractSA(input){}

AbstractSAI::AbstractSAI(Text* T):AbstractSA(T){}

word AbstractSAI::accessSAI(const word i){
	return (inverseSuffixArray->access(i));
}

AbstractSAI::~AbstractSAI(){
	delete inverseSuffixArray;
}

void AbstractSAI::serialize(std::ostream& output){
	inverseSuffixArray->serialize(output);
}
