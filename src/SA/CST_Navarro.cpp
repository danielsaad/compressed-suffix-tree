/*
 * CST_Navarro.cpp
 *
 *  Created on: Dec 31, 2012
 *      Author: daniel
 */

#include "CST_Navarro.hpp"
#include "CESA.hpp"

void CST_Navarro::serialize(std::ostream& output){
	AbstractSTSA::serialize(output);
	RMQ_NSV_info->serialize(output);
}

CST_Navarro::CST_Navarro(std::istream& input):AbstractSTSA_RMQ(input){
	enhancedSuffixArray = new CESAI(input);
	RMQ_NSV_info = new RMQ_NSV_Navarro(input);
	RMQInfo =  RMQ_NSV_info;
	RMQInfo->setLcp(enhancedSuffixArray->getLcp());
	NSVInfo = RMQ_NSV_info;
	NSVInfo->setLcp(enhancedSuffixArray->getLcp());
}

CST_Navarro::CST_Navarro(Text* T):AbstractSTSA_RMQ(T){
	enhancedSuffixArray = new CESAI(T);
	RMQ_NSV_info = new RMQ_NSV_Navarro(enhancedSuffixArray->getLcp(),enhancedSuffixArray->getSize());
	RMQInfo = RMQ_NSV_info;
	NSVInfo = RMQ_NSV_info;
	NSVInfo->setLcp(enhancedSuffixArray->getLcp());
}


CST_Navarro::~CST_Navarro(){
	delete RMQ_NSV_info;
}

