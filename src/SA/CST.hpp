/*
 * CST.hpp
 *
 *  Created on: Dec 13, 2012
 *      Author: daniel
 */

#ifndef CST_HPP_
#define CST_HPP_

#include "AbstractSTSA_RMQ.hpp"
#include "CESA.hpp"

/**Compressed suffix tree based on compressed enhanced suffix array
 */
class CST: public AbstractSTSA_RMQ{
public:
	CST(std::istream& input);
	CST(Text* T);
	virtual ~CST();
	virtual void serialize(std::ostream & output);
};



#endif /* CST_HPP_ */
