/*
 * AbstractSA.hpp
 *
 *  Created on: Dec 13, 2012
 *      Author: daniel
 */

#ifndef ABSTRACTSA_HPP_
#define ABSTRACTSA_HPP_

#include "../Definitions/types.hpp"
#include "../Text/text.hpp"
#include <iostream>

class AbstractSA;

class abstractSAContainer{
public:
	abstractSAContainer(AbstractSA* A);
	abstractSAContainer(std::istream& input);
	virtual word access(const word i)=0;
	virtual void serialize(std::ostream& output);
	virtual ~abstractSAContainer(){}
	void setSA(AbstractSA* A);
protected:
	AbstractSA* A;
	word n;
};

/**
    An abstract class for Suffix Array Based Classes
**/
class AbstractSA{
public:
	AbstractSA(Text* T);
	AbstractSA(std::istream& input);
	virtual ~AbstractSA();
    virtual void serialize(std::ostream& output);
    word operator[](word i);
    word accessSA(const word i);
    word getSize();
    void setText(Text* T);
    virtual byte accessSymbol(const word i)=0;
    double getBuildTime();
protected:
    Text* T;
    abstractSAContainer* suffixArray;
    word n;
    time_t start;
    time_t end;
};

class AbstractSAI: public virtual AbstractSA{
public:
	AbstractSAI(std::istream& input);
	AbstractSAI(Text* T);
	word accessSAI(const word i);
	virtual ~AbstractSAI();
	virtual void serialize(std::ostream& output);
protected:
	abstractSAContainer* inverseSuffixArray;
};




#endif /* ABSTRACTSA_HPP_ */
