/*
 * ESA.hpp
 *
 *  Created on: Dec 13, 2012
 *      Author: daniel
 */

#ifndef ESA_HPP_
#define ESA_HPP_

#include "AbstractESA.hpp"
#include "SA.hpp"
/**
    Raw Enhanced suffix array
**/

class ESA:public SA,public AbstractESA{
public:
	ESA(Text* T);
	ESA(std::istream& input);
	virtual ~ESA();
	virtual void serialize(std::ostream& output);
	virtual byte accessSymbol(const word i){return SA::accessSymbol(i);} //necessary in Eclipse :@
};

class ESAI:public AbstractSAI,public ESA{
public:
	ESAI(Text* T);
	ESAI(std::istream& input);
	byte accessSymbol(const word i){ return ESA::accessSymbol(i);} //eclipse
	virtual void serialize(std::ostream& output);
	virtual ~ESAI();
};

#endif /* ESA_HPP_ */
