/*
 * CSA.hpp
 *
 *  Created on: Dec 13, 2012
 *      Author: daniel
 */

#ifndef CSA_HPP_
#define CSA_HPP_

#include "AbstractSA.hpp"
#include "../Text/text.hpp"
#include "../PSI/psi.hpp"
class CSA;

class CompressedSA:public abstractSAContainer{
public:
	CompressedSA(AbstractSA* A);
	CompressedSA(std::istream& input);
	word access(const word i);
	virtual ~CompressedSA();
	virtual void serialize(std::ostream& output);
private:
    word* array; /**Explicit stores some CSA entries**/
    word words; /**Number of words of the CSA**/
};

class CompressedISA: public abstractSAContainer{
public:
	CompressedISA(AbstractSA* A);
	CompressedISA(std::istream& input);
	word access(const word i);
	virtual ~CompressedISA();
	virtual void serialize(std::ostream & output);
private:
	word* array;
	word words;
};

/**The Compressed Suffix Array Data Structure**/
class CSA : public virtual AbstractSA{
public:
    CSA(Text* T);
    CSA(std::istream& input);
    virtual ~CSA();
    virtual word getSymbolsQuantity(word currentSymbol);
    virtual word getLexicograpichSmallerSymbols(word currentSymbol);
    virtual word accessPsi(word i);
    word getStartSymbol(word i);
    AbstractPsi* getPsi();
    virtual byte accessSymbol(const word i);
    void serialize(std::ostream& output);

    const static word sampleRate=20; /**The sampleRate of the SA structure**/
    const static word blockFactor=60; /**Block factor for the CSA construction Algorithm**/

private:
    AbstractPsi* psi;
    word* c;/**array that keeps the number of characters lexicographically smaller than c[i]**/

    /** Necessary variables for Suffix Array construction**/
    word* cTemp; /**Temporary Array c which represent the C array considering the new block in the incremental algorithm**/
    word BLOCKSIZE; /**@var Size of the Block to be processed by Lam et al incremental algorithm**/
    word* WM; /**@var working Memory**/
    word* M; /** @var Stores the rank of Long suffixes among themselves**/
    word* MI; /** @var Inverse of M MI[M[i]]=i**/
    word* L; /** @var Rank of a long suffixes among all the short suffixes**/
    word* LSorted; /**@var contains the sorted list of the Rank of Long suffixes among all suffixes**/
    StaticDictionary* V; /**@var Static Dictionary, V[i]=1 if the ith suffix in lexicographical order is a long suffix, otherwise, V[i]=0**/
    byte* textWorkingMemory; /**@var text working memory**/
    byte* previousTextBlock; /**@var stores the previous block of text in the incremental algorithm**/
    byte* currentTextBlock; /**@var stores the current block of text in the incremental algorithm**/
    std::fstream tempPsiFile; /**@var File which stores the read/written psi values**/
    word blockSize;
    word currentBlockSize;

    void build();
    word rankBsearch(word rankNextSuffix,word ch);
    void computeRankAmongLongSuffixes(word currentBlockSize);
    void computeRankAmongShortSuffixes(word currentBlockSize);
    void computeNewPsi(word currentBlockSize);
    void baseCase(word currentBlockSize);
    word computeBlockSize();
};

class CSAI:public AbstractSAI,public CSA{
public:
	CSAI(std::istream& input);
	CSAI(Text* T);
	virtual byte accessSymbol(const word i){return(CSA::accessSymbol(i));} //eclipse
	virtual void serialize(std::ostream& output);
	virtual ~CSAI();
};

#endif /* CSA_HPP_ */
