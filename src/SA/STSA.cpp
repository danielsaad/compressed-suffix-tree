/*
 * STSA.cpp
 *
 *  Created on: Dec 13, 2012
 *      Author: daniel
 */
#include "STSA.hpp"
#include "../RMQ/RMQ_succinct.hpp"
#include "../RMQ/RMQ_NSV_Navarro.hpp"
#include "../NSV/NSV_Naive.hpp"


STSA_Navarro::STSA_Navarro(Text* T):AbstractSTSA_RMQ(T){
    enhancedSuffixArray = new ESA(T);
	RMQ_NSV_info = new RMQ_NSV_Navarro(enhancedSuffixArray->getLcp(),enhancedSuffixArray->getSize());
	RMQInfo = RMQ_NSV_info;
	NSVInfo = RMQ_NSV_info;
	NSVInfo->setLcp(enhancedSuffixArray->getLcp());
}

STSA_Navarro::STSA_Navarro(std::istream& input):AbstractSTSA_RMQ(input){
    enhancedSuffixArray = new ESA(input);
	RMQ_NSV_info = new RMQ_NSV_Navarro(input);
	RMQInfo =  RMQ_NSV_info;
	RMQInfo->setLcp(enhancedSuffixArray->getLcp());
	NSVInfo = RMQ_NSV_info;
	NSVInfo->setLcp(enhancedSuffixArray->getLcp());
}

STSA_Navarro::~STSA_Navarro(){
	delete RMQ_NSV_info;
    delete enhancedSuffixArray;
}

void STSA_Navarro::serialize(std::ostream& output){
	AbstractSTSA::serialize(output);
	RMQ_NSV_info->serialize(output);
}


STSA_RMQ::STSA_RMQ(Text* T):AbstractSTSA_RMQ(T){
    enhancedSuffixArray = new ESA(T);
    _rmqInfo = new RMQ_succinct(enhancedSuffixArray->getLcp(),enhancedSuffixArray->getSize());
    _rmqInfo->setLcp(enhancedSuffixArray->getLcp());
}

STSA_RMQ::STSA_RMQ(std::istream& input):AbstractSTSA_RMQ(input){
    enhancedSuffixArray = new ESA(input);
    _rmqInfo = new RMQ_succinct(input);
    _rmqInfo->setLcp(enhancedSuffixArray->getLcp());
}

STSA_RMQ::~STSA_RMQ(){
    delete _rmqInfo;
}

void STSA_RMQ::serialize(std::ostream& output){
    AbstractSTSA::serialize(output);
    _rmqInfo->serialize(output);
}

