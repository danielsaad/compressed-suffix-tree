/*
 * STSA.hpp
 *
 *  Created on: Dec 13, 2012
 *      Author: daniel
 */

#ifndef STSA_HPP_
#define STSA_HPP_

#include "AbstractSTSA_RMQ.hpp"
#include "../RMQ/RMQ_succinct.hpp"
#include "../RMQ/RMQ_NSV_Navarro.hpp"
#include "ESA.hpp"

class STSA_Navarro : public AbstractSTSA_RMQ{
public:
    STSA_Navarro(std::istream& input);
    STSA_Navarro(Text* T);
	void serialize(std::ostream& output);
    virtual ~STSA_Navarro();
protected:
    RMQ_NSV_Navarro* RMQ_NSV_info;
};


class STSA_RMQ : public AbstractSTSA_RMQ{
public:
    STSA_RMQ(std::istream& input);
    STSA_RMQ(Text* T);
    void serialize(std::ostream& output);
    virtual ~STSA_RMQ();
public:
    RMQ_succinct* _rmqInfo;
};



#endif /* STSA_HPP_ */
