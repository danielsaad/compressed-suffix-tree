/*
 * CESA.hpp
 *
 *  Created on: Dec 13, 2012
 *      Author: daniel
 */

#ifndef CESA_HPP_
#define CESA_HPP_


#include "AbstractESA.hpp"
#include "CSA.hpp"


class CESAI;

/**
    The compressed Enhanced suffix array data structure provides
    access to the compressed lcp information.
    Lcp(i) = Lcp(A[i],A[i+1]);
**/
class CESA:public AbstractESA, public  CSA{
public:
//	CESA(Text* T,CESAI* saPtr);
    CESA(Text* T);
    CESA(std::istream& input);
//    CESA(std::istream& input,CESAI* saPtr);
    virtual byte accessSymbol(const word i){return CSA::accessSymbol(i);} //necessary in Eclipse :@
    virtual void serialize(std::ostream& output);
    virtual ~CESA();
};

class CESAI: public CESA,public AbstractSAI{
public:
	CESAI(Text* T);
	CESAI(std::istream& input);
	void serialize(std::ostream& output);
	virtual byte accessSymbol(const word i);
	virtual ~CESAI();
};

#endif /* CESA_HPP_ */
