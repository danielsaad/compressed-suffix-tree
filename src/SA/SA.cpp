/*
 * SA.cpp
 *
 *  Created on: Dec 13, 2012
 *      Author: daniel
 */




#include "SA.hpp"
#include <divsufsort.h>
#include <iostream>
#include <cstdlib>


//LibdivsufsortSA::LibdivsufsortSA(const Text* text){
//    _size = text.getLength();
//    _sa = new integer[n];
//    if(divsufsort(A->getText(),_sa,_size)){
//       std::cerr<<("suffix sort error");
//       exit(EXIT_FAILURE);
//    }
//}


//inline integer LibdivsufsortSA::operator [](const integer i){
//    return(_sa[i]);
//}

//inline integer LibdivsufsortSA::getSize(){
//    return(_size);
//}

//void LibdivsufsortSA::serialize(std::ostream& output){
//    //TODO: Implement This
//}

//LibdivsufsortSA::~LibdivsufsortSA(){
//    delete[] _sa;
//}

//ISA::ISA(const iSA* sa){
//    _size = sa->getSize();
//    _isa = new integer[_size];
//    for(integer i=0;i<_size;i++)
//        _isa[sa[i]] = i;
//}


//inline integer ISA::operator [](const integer i){
//    return(_isa[i]);
//}

//integer ISA::getSize(){
//    return(_size);
//}

//void ISA::serialize(std::ostream &output){
//    //TODO: Implement This
//}

//ISA::~ISA(){
//    delete[] _isa;
//}

//ISA_SemiExternal::ISA_SemiExternal(iSA *sa){
//    std::fstream tmpFile;
//    char tmpName[L_tmpnam] = "/tmp/fileXXXXXX" ;

//    _isa = sa->getSize();
//    _isa = sa->

//    mkstemp(tmpName);
//    try{
//        tmpFile.open(tmp_name_input,std::ios::out | std::ios::in | std::ios::trunc | std::ios::binary);
//    }
//    catch(std::fstream::failure e){
//        std::cout << e.what();
//    }

//    for(integer i=0;i<_size;i++){
//        integer x = sa[i];
//        tmpFile.write(&x,sizeof(x));
//    }
//    delete sa;
//    tmpFile.seekp(0,std::ios_base::beg);
//    tmpFile.seekg(0,std::ios_base::beg);
//    for(integer i=0;i<_size;i++){
//        integer x;
//        tmpFile.read(&x,sizeof(x));
//        _isa[x] = i;
//    }
//    tmpFile.close();
//    remove(tmpName);
//}

//integer ISA_SemiExternal::operator [](const integer i){
//    return(_isa[i]);
//}

//integer ISA_SemiExternal::getSize(){
//    return(_size);
//}

//void ISA_SemiExternal::serialize(std::ostream &output){
//    //TODO: Implement
//}

//ISA_SemiExternal::~ISA_SemiExternal(){
//    delete[] _isa;
//}

rawISA::rawISA(SA* A):abstractSAContainer(A){
	n = A->getSize();
	array = new saidx_t[n];
	for(word i=0;i<A->getSize();i++){
		array[A->accessSA(i)] = i;
	}
}




word rawISA::access(const word i){
	return(array[i]);
}


rawISA::rawISA(std::istream& input):abstractSAContainer(input){
	array = new saidx_t[n];
	input.read(reinterpret_cast<char*> (array),sizeof(saidx_t)*n);
}


void rawISA::serialize(std::ostream& output){
	abstractSAContainer::serialize(output);
	output.write(reinterpret_cast<const char*> (array),sizeof(saidx_t)*n);
}

rawISA::~rawISA(){
	delete[] array;
}






rawSA::rawSA(SA* A):abstractSAContainer(A){
	n = A->getSize();
	array = new saidx_t[n];
	if(divsufsort(A->getText(),array,n)){
       std::cerr<<("suffix sort error");
       exit(EXIT_FAILURE);
   }
}




word rawSA::access(const word i){
	return(array[i]);
}


rawSA::rawSA(std::istream& input):abstractSAContainer(input){
	array = new saidx_t[n];
	input.read(reinterpret_cast<char*> (array),sizeof(saidx_t)*n);
}


void rawSA::serialize(std::ostream& output){
	abstractSAContainer::serialize(output);
	output.write(reinterpret_cast<const char*> (array),sizeof(saidx_t)*n);
}

rawSA::~rawSA(){
	delete[] array;
}




SA::SA(Text* T):AbstractSA(T){
	std::cout << "Building Raw Suffix Array\n";
	time(&start);
    n = T->getLength();
    textStr = new sauchar_t[T->getLength()];
    T->reset();
    T->readBuffer(textStr,T->getLength());
    suffixArray = new rawSA(this);
    time(&end);
    std::cout << "Raw Suffix Array build in " << getBuildTime() << "s\n";
}


SA::~SA(){
    delete[] textStr;
}

sauchar_t* SA::getText(){
    return textStr;
}

SA::SA(std::istream& input):AbstractSA(input){
	suffixArray = new rawSA(input);
	suffixArray->setSA(this);
	textStr = new sauchar_t[n];
	input.read(reinterpret_cast<char*> (textStr),sizeof(sauchar_t)*n);
}

void SA::serialize(std::ostream& output){
	std::cout << "Serializing Raw Suffix Array\n";
	AbstractSA::serialize(output);
	output.write(reinterpret_cast<const char*> (textStr),sizeof(sauchar_t)*n);
}

byte SA::accessSymbol(const word i){
	return(textStr[i]);
}


SAI::SAI(Text* T):AbstractSA(T),AbstractSAI(T),SA(T){
	inverseSuffixArray = new rawISA(this);
}

SAI::SAI(std::istream& input):AbstractSA(input),AbstractSAI(input),SA(input){
	inverseSuffixArray = new rawISA(input);
	inverseSuffixArray->setSA(this);
}

void SAI::serialize(std::ostream& output){
	SA::serialize(output);
	AbstractSAI::serialize(output);
}

SAI::~SAI(){}
