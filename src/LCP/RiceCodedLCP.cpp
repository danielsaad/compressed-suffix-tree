/*
 * RiceCodedLCP.cpp
 *
 *  Created on: Dec 16, 2012
 *      Author: daniel
 */


#include "RiceCodedLCP.hpp"
#include "../SA/CSA.hpp"
#include "../StaticDictionaries/GMN_StaticDictionary.hpp"

RiceCodedLcp::RiceCodedLcp(AbstractSA* SA):AbstractLcp(SA){
	std::cout << "Building Rice-Coded LCP\n";
	time(&start);
    lcpQuotient = new GMN_StaticDictionary(2* saPtr->getSize()); /**TODO: SHRINK AFTER**/
    lastCodedLcpQuotient = 0;
    lastQuotientBit = MAX_WORD;
    lcpQuotient->reset();
    build();
    time(&end);
    std::cout << "Rice-Coded LCP built in " << getBuildTime() << "s\n";
}


RiceCodedLcp::~RiceCodedLcp(){
    delete lcpQuotient;
    delete[] str1;
    delete[] str2;
}

/**
    Set the LCP[i]+SA[i] value
    @param saValue the Suffix Array value. LCP[i]+SA[i] holds an increasing sequence.
    @param lcpValue the LCP Value. LCP[i]+SA[i] holds an increasing sequence.
**/
void RiceCodedLcp::set(word saValue,word lcpValue){
    word diff;
    word value = saValue+lcpValue;
    diff = value - lastCodedLcpQuotient;
    lastCodedLcpQuotient = value;
    lcpQuotient->setBit(lastQuotientBit+diff+1);
    lastQuotientBit+=diff+1;
}

/**
    Retrieves the LCP[i] value
    @param the input for LCP[i]
    @return Return LCP[i]
**/
word RiceCodedLcp::access(word i){
    word saEntry = (*saPtr)[i];
    word lcpSA = lcpQuotient->select1(saEntry+1);
    return(lcpSA - 2*saEntry);
}



/**
    Returns LCP value between suffix i and k from the text
    @param CESA the compressed enhanced suffix array
    @param i the first suffix.
    @param k the second suffix.
    @param h the symbols in common that suffixes i and k have due to kasai et al algorith,
    @return return LCP(i,k)
**/

/**
    Use kasai et al algorithm to compute LCP values.

     for(i=0;i<saPtr->getSize();i++){
        h=0;
        if(inverseSuffixArray[i] !=  saPtr->getSize() -1){
                k = (*saPtr)[inverseSuffixArray[i]+1];
                while(text[i+h]==text[k+h]){
                	h++;
                }
                set(inverseSuffixArray[i],h);
                if(h>0) h--;
            }
            else{
                set(inverseSuffixArray[i],0);
            }
    }
**/

word RiceCodedLcp::lcpCompare(word i,word k){
	CSA* saPtr = dynamic_cast<CSA*> (getSA());
	word lcp =0;
	while(saPtr->getStartSymbol(i) == saPtr->getStartSymbol(k)){
		lcp++;
		i = saPtr->accessPsi(i);
		k = saPtr->accessPsi(k);
	}
    return(lcp);
}



void RiceCodedLcp::build(){
	str1 = new byte[lcp_buffer];
	str2 = new byte[lcp_buffer];
	CSA* compressedSA = dynamic_cast<CSA*> (getSA());
    AbstractPsi* psi = compressedSA->getPsi();
    word i,sufInv;
    sufInv = 0;
    word k;
    for(i=0;i<saPtr->getSize();i++){
        sufInv = psi->access(sufInv);
        if(sufInv !=  saPtr->getSize()-1){
        	k = (sufInv+1);
            set(saPtr->accessSA(sufInv),lcpCompare(sufInv,k));
        }
        else{
            set(saPtr->accessSA(sufInv),0);
        }
    }
    lcpQuotient->preprocess();
}

void RiceCodedLcp::serialize(std::ostream& output){
	std::cout << "Serializing RiceCoded LCP information\n";
	AbstractLcp::serialize(output);
    lcpQuotient->serialize(output);
    output.write(reinterpret_cast<const char*> (&lastCodedLcpQuotient),sizeof(word));
    output.write(reinterpret_cast<const char*> (&lastQuotientBit),sizeof(word));
}

RiceCodedLcp::RiceCodedLcp(std::istream& input):AbstractLcp(input){
	lcpQuotient =  new GMN_StaticDictionary(input);
	input.read(reinterpret_cast<char*> (&lastCodedLcpQuotient),sizeof(lastCodedLcpQuotient));
	input.read(reinterpret_cast<char*> (&lastQuotientBit),sizeof(lastQuotientBit));
}
