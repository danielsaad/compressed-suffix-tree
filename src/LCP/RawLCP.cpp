/*
 * RawLCP.cpp
 *
 *  Created on: Dec 16, 2012
 *      Author: daniel
 */



#include "RawLCP.hpp"
#include "../SA/SA.hpp"

rawLcp::rawLcp(AbstractSA* SA):AbstractLcp(SA){
	time(&start);
	std::cout << "Building Raw LCP\n";
    array = new word[saPtr->getSize()];
    build();
    time(&end);
	std::cout << "Raw LCP built in " << getBuildTime() << "s\n";

}


rawLcp::~rawLcp(){
    delete[] array;
}

void rawLcp::build(){
	SA* suffixArray = dynamic_cast<SA*> (saPtr);
    word* inverseSuffixArray = new word[saPtr->getSize()];
    sauchar_t* text = suffixArray->getText();
    word h,i,k;
    for(i=0;i<saPtr->getSize();i++){
            inverseSuffixArray[(*saPtr)[i]] = i;
    }
    h = 0;
    for(i=0;i<saPtr->getSize();i++){
        if(inverseSuffixArray[i] !=  saPtr->getSize() -1){
                k = (*saPtr)[inverseSuffixArray[i]+1];
                while(text[i+h]==text[k+h]){
                	h++;
                }
                set(inverseSuffixArray[i],h);
                if(h>0) h--;
            }
            else{
                set(inverseSuffixArray[i],0);
            }
    }

    delete[] inverseSuffixArray;
}

void rawLcp::set(word i,word value){
    array[i]=value;
}

word rawLcp::access(word i){
    return(array[i]);
}


void rawLcp::serialize(std::ostream& output){
	std::cout << "Serializing Raw LCP information\n";
	AbstractLcp::serialize(output);
	output.write(reinterpret_cast<const char*>(array),sizeof(word)*n);
}

rawLcp::rawLcp(std::istream& input):AbstractLcp(input){
	std::cout << "Loading Raw LCP information\n";
	array = new word[n];
	input.read(reinterpret_cast<char*>(array),sizeof(word)*n);
}
