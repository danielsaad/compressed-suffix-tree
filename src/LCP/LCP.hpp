#ifndef LCP_HPP_INCLUDED
#define LCP_HPP_INCLUDED

#include "../SA/AbstractSA.hpp"
#include "../SA/SA.hpp"
#include "../Definitions/types.hpp"

/**the LCP data structure**/


//class iLCP : public serializable{
//public:
//    virtual integer operator[](const integer i) = 0;
//    virtual integer getSize() = 0;

//    virtual ~iLCP();
//};


//class LCP_Kasai:public iLCP{
//public:
//    LCP_Kasai(iSA* sa);

//    virtual integer operator [](const integer i);
//    virtual integer getSize();
//    virtual void serialize(std::ostream &output);

//    ~LCP_Kasai();
//private:
//    integer*_lcp;
//};


//class LCP_Kasai_Byte{
//    LCP_Kasai_Byte(iSA* sa);

//    virtual integer operator [](const integer i);
//    virtual integer getSize();
//    virtual void serialize(std::ostream &output);

//    ~LCP_Kasai_Byte();
//};


class AbstractLcp{
public:

	AbstractLcp(AbstractSA* A);
	AbstractLcp(std::istream& input);
    virtual word access(word i) = 0;
    virtual void set(word value1,word value2) = 0;
    virtual void build() = 0;
    void setSA(AbstractSA* saPtr);
    virtual void serialize(std::ostream& output);
    AbstractSA* getSA();
    double getBuildTime();
	virtual ~AbstractLcp();
	word getSize();
protected:
	word n;
    AbstractSA* saPtr;
    time_t start;
    time_t end;
};







#endif // LCP_HPP_INCLUDED
