/*
 * RawLCP.hpp
 *
 *  Created on: Dec 16, 2012
 *      Author: daniel
 */

#ifndef RAWLCP_HPP_
#define RAWLCP_HPP_

#include "LCP.hpp"

class rawLcp : public AbstractLcp{
public:
    rawLcp(AbstractSA* SA);
    rawLcp(std::istream& input);

    virtual ~rawLcp();
    word access(word i);
    void set(word i, word value);
    void build();
    void serialize(std::ostream& output);

private:
    word* array;
};


#endif /* RAWLCP_HPP_ */
