/*
 * RIceCodedLCP.hpp
 *
 *  Created on: Dec 16, 2012
 *      Author: daniel
 */

#ifndef RICECODEDLCP_HPP_
#define RICECODEDLCP_HPP_

#include "LCP.hpp"
#include "../StaticDictionaries/StaticDictionary.hpp"
class RiceCodedLcp : public AbstractLcp{
public:
    RiceCodedLcp(AbstractSA* SA);
    RiceCodedLcp(std::istream& input);
    virtual ~RiceCodedLcp();
    virtual word access(word i);
    virtual void set(word saValue,word lcpValue);
    void preprocessQuotientRank();
    void serialize(std::ostream& output);
private:
    StaticDictionary* lcpQuotient;
    word lastCodedLcpQuotient;
    word lastQuotientBit;

    static const word lcp_buffer = 16;
    byte *str1,*str2;

    virtual void build();
    word lcpCompare(word i,word k);


};


#endif /* RICECODEDLCP_HPP_ */
