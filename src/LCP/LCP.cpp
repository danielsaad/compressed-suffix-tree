#include "LCP.hpp"

/**
    Allocates the LCP structure
    @param CSA the compressed suffix array
**/



void AbstractLcp::serialize(std::ostream& output){
	output.write(reinterpret_cast<const char*>(&n),sizeof(n));
}

void AbstractLcp::setSA(AbstractSA* saPtr){
	this->saPtr = saPtr;
}

AbstractSA* AbstractLcp::getSA(){
	return(saPtr);
}



double AbstractLcp::getBuildTime(){
	return(difftime(end,start));
}

AbstractLcp::~AbstractLcp(){}


word AbstractLcp::getSize(){
	return n;
}

AbstractLcp::AbstractLcp(std::istream& input){
	input.read(reinterpret_cast<char*>(&n),sizeof(n));
}

AbstractLcp::AbstractLcp(AbstractSA* SA){
	this->saPtr = SA;
	this->n = SA->getSize();
}
