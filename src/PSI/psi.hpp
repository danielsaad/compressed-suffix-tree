#ifndef PSI_HPP_INCLUDED
#define PSI_HPP_INCLUDED

#include "../SA/AbstractSA.hpp"
#include "../StaticDictionaries/StaticDictionary.hpp"
#include "../Text/text.hpp"


class AbstractPsi{
public:
	AbstractPsi(AbstractSA* A);
	AbstractPsi(std::istream& input);
    virtual word access(word i) = 0;
    virtual void set(word i,word value) = 0;
    virtual void setSequential(word currentSymbol, word value) = 0;
    virtual word accessSequential(word currentSymbol) = 0;
    virtual void configure(word currentSymbol) = 0;
    virtual void postConfigure(word currentSymbol) = 0;
    virtual void serialize(std::ostream& output) = 0;
    AbstractSA* getSA();
    void setSA(AbstractSA* saPtr);
    virtual ~AbstractPsi(){}
protected:
    AbstractSA* saPtr; /** The correspondent Suffix Array DS**/

};

class RiceCodedPsi : public AbstractPsi{
public:
    RiceCodedPsi(AbstractSA* A);
    RiceCodedPsi(std::istream& input);
    virtual ~RiceCodedPsi();
    virtual void set(word i,word value);
    virtual void setSequential(word currentSymbol, word value);
    virtual word access(word i);
    virtual word accessSequential(word currentSymbol);
    virtual void configure(word currentSymbol);
    virtual void postConfigure(word currentSymbol);
    virtual void serialize(std::ostream& output);
 private:
    StaticDictionary* psiQuotient[ALPHABET_SIZE]; /**The quotient part**/
    StaticDictionary* psiRemainder[ALPHABET_SIZE]; /**The remainder part**/
    word psiRemainderBits[ALPHABET_SIZE]; /**the number of bits in the quotient**/
    word psiQuotientBits[ALPHABET_SIZE]; /**the number of bits in the quotient**/
    word lastCodedPsiQuotient[ALPHABET_SIZE]; /**contains the last Coded psi value**/
    word lastQuotientBit[ALPHABET_SIZE]; /**contains the position of the last set bit**/
    word currentWordSize; /** The size of word for each psi entry**/
    word lastSetSequentialIndex; /**Last set sequential index of sequential setPsi**/
    word lastAccessSequentialIndex; /**Last accessed index of sequential setPsi**/
};


#endif // PSI_HPP_INCLUDED
