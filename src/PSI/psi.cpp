#include "../SA/CSA.hpp"
#include "../Definitions/utils.hpp"
#include "psi.hpp"
#include "../StaticDictionaries/GMN_StaticDictionary.hpp"
#include "../StaticDictionaries/BitOperations.hpp"
#include <cmath>

/**
    Configure the psi structure. Resizes each bitvector
    to its propper size. We are using the coding scheme proposed by grossi et al.
    @param CSA Compressed suffix array.
**/


AbstractPsi::AbstractPsi(AbstractSA* A){
	this->saPtr = A;
}

AbstractPsi::AbstractPsi(std::istream& input){}

void RiceCodedPsi::configure(word currentSymbol){
    word symbols;
    word logC;
    word remainderBits;
    CSA* compressedSuffixArray = dynamic_cast<CSA*> (saPtr);
    /**TODO: LITTLE BUG WHEN SYMBOLS=1**/
    currentWordSize = BitOperations::retrieveNumBits(compressedSuffixArray->getLexicograpichSmallerSymbols(ALPHABET_SIZE));
    symbols = compressedSuffixArray->getSymbolsQuantity(currentSymbol);
    if(symbols>0){
        logC = std::max((word)floor(log2(symbols)),(word) 1);
        remainderBits = currentWordSize-logC;
        psiQuotientBits[currentSymbol] = logC;
        psiRemainderBits[currentSymbol] = remainderBits;
        lastQuotientBit[currentSymbol] = MAX_WORD;
        lastCodedPsiQuotient[currentSymbol] = 0;
        /**resize bit vector**/
        if(psiQuotient[currentSymbol]->getSize() < 2*symbols){
            psiQuotient[currentSymbol]->resize(2*symbols);
        }
        if(psiRemainder[currentSymbol]->getSize() < (psiRemainderBits[currentSymbol])*symbols){
            psiRemainder[currentSymbol]->resize(psiRemainderBits[currentSymbol]*symbols);
        }
    }
    lastSetSequentialIndex=0;
    lastAccessSequentialIndex=0;
}



/**
    Set the $\Psi(i)$ value.
    @param i the position in the $\Psi$ data structure.
    @param value the value which $\Psi(i)$ will receive.
**/
void RiceCodedPsi::set(word i ,word value){
    word k;
    word start;
    word ch;
    word diff;
    word quotient;
    word remainder;
    CSA* compressedSuffixArray = dynamic_cast<CSA*> (saPtr);
    ch = compressedSuffixArray->getStartSymbol(i);
    start = WORD_SIZE-currentWordSize;
    quotient = BitOperations::selectBitsFromWord(value,start,start+psiQuotientBits[ch]-1) >> (currentWordSize-psiQuotientBits[ch]);
    k = i-compressedSuffixArray->getLexicograpichSmallerSymbols(ch);
    remainder = BitOperations::selectBitsFromWord(value,start+psiQuotientBits[ch],WORD_SIZE-1);
    diff = quotient - lastCodedPsiQuotient[ch];
    lastCodedPsiQuotient[ch]=quotient;
    psiQuotient[ch]->zeroFill(lastQuotientBit[ch]+1,lastQuotientBit[ch]+diff+1);
    psiQuotient[ch]->setBit(lastQuotientBit[ch]+diff+1);
    lastQuotientBit[ch]+=diff+1;
    if(psiRemainderBits[ch]>0){
        psiRemainder[ch]->setBits(remainder,psiRemainderBits[ch],k*psiRemainderBits[ch]);
    }
}

/**
    Access the $\Psi(i)$ function in the compressed suffix array*
    @param i the input for the $\Psi$ function
    @return returns $\Psi(i)$.
**/
word RiceCodedPsi::access(word i){
    word k,ch;
    word nBits;
    word quotient,remainder;
    CSA* compressedSuffixArray = dynamic_cast<CSA*> (saPtr);
    /**Get the start symbol of the ith suffix**/
    ch = compressedSuffixArray->getStartSymbol(i);
    k = i-compressedSuffixArray->getLexicograpichSmallerSymbols(ch);
    nBits = psiRemainderBits[ch];
    quotient = psiQuotient[ch]->select1(k+1)-(k);
    if(nBits>0){
        remainder = psiRemainder[ch]->extractBits(nBits*k,nBits*(k+1)-1);
    }
    else{
        remainder = 0;
    }
    return(BitOperations::concatenateWords(quotient,remainder,psiRemainderBits[ch]));
}


word RiceCodedPsi::accessSequential(word currentSymbol){
    word quotient;
    word remainder;
    word nBits;
    word startBit;
    word lz;
    word leadingZeroesWord;
    word startWordBit;
    word wordSize;
    startBit = lastQuotientBit[currentSymbol]+1;
    quotient=0;
    do{
        startWordBit = startBit & WORD_MOD;
        wordSize = WORD_SIZE - startWordBit;
        leadingZeroesWord = psiQuotient[currentSymbol]->accessWord(startBit >> WORD_SHIFT);
        leadingZeroesWord = BitOperations::selectBitsFromWord(leadingZeroesWord,startWordBit,WORD_SIZE-1);
        lz = BitOperations::retrieveLeadingZeroes(leadingZeroesWord)-startWordBit;
        quotient += lz;
        startBit += wordSize;
    }while(lz == wordSize);

    lastQuotientBit[currentSymbol]+=quotient+1;
    quotient += lastCodedPsiQuotient[currentSymbol];
    lastCodedPsiQuotient[currentSymbol]=quotient;
    nBits = psiRemainderBits[currentSymbol];

    if(nBits>0){
        remainder = psiRemainder[currentSymbol]->extractBits(nBits*lastAccessSequentialIndex,nBits*(lastAccessSequentialIndex+1)-1);
    }
    else{
        remainder = 0;
    }
    lastAccessSequentialIndex++;
    return(BitOperations::concatenateWords(quotient,remainder, psiRemainderBits[currentSymbol]));
}


void RiceCodedPsi::setSequential(word currentSymbol,word psiValue){
    word start;
    word diff;
    word quotient;
    word remainder;
    start = WORD_SIZE- currentWordSize;
    quotient = BitOperations::selectBitsFromWord(psiValue,start,start+psiQuotientBits[currentSymbol]-1) >>
        (currentWordSize-psiQuotientBits[currentSymbol]);
    remainder = BitOperations::selectBitsFromWord(psiValue,start + psiQuotientBits[currentSymbol],WORD_SIZE-1);
    diff = quotient - lastCodedPsiQuotient[currentSymbol];
    psiQuotient[currentSymbol]->zeroFill(lastQuotientBit[currentSymbol]+1,lastQuotientBit[currentSymbol]+diff+1);
    psiQuotient[currentSymbol]->setBit(lastQuotientBit[currentSymbol]+diff+1);
    lastCodedPsiQuotient[currentSymbol]=quotient;
    lastQuotientBit[currentSymbol]+=diff+1;
    if(psiRemainderBits[currentSymbol]>0){
        psiRemainder[currentSymbol]->setBits(remainder,psiRemainderBits[currentSymbol],lastSetSequentialIndex*psiRemainderBits[currentSymbol]);
    }
    lastSetSequentialIndex++;
}


RiceCodedPsi::RiceCodedPsi(AbstractSA* A):AbstractPsi(A){
	word i;
	currentWordSize=0;
    for(i=0;i<ALPHABET_SIZE;i++){
        psiQuotient[i] = new GMN_StaticDictionary(0);
        psiRemainder[i] = new GMN_StaticDictionary(0);
        lastCodedPsiQuotient[i]=0;
        lastQuotientBit[i]=0;
    }
    lastAccessSequentialIndex=0;
    lastSetSequentialIndex=0;
}

/**
    Preprocess all quotient rank
**/

void RiceCodedPsi::postConfigure(word currentSymbol){
    psiQuotient[currentSymbol]->preprocess();
    lastSetSequentialIndex = 0;
    lastAccessSequentialIndex = 0;
    lastQuotientBit[currentSymbol] = MAX_WORD;
    lastCodedPsiQuotient[currentSymbol] = 0;
}

void RiceCodedPsi::serialize(std::ostream& output){
	std::cout << "Serializing RiceCodedPsi Structure\n";
	for(word i=0;i<ALPHABET_SIZE;i++) psiQuotient[i]->serialize(output);
	for(word i=0;i<ALPHABET_SIZE;i++) psiRemainder[i]->serialize(output);
	output.write(reinterpret_cast<const char*> (psiRemainderBits),ALPHABET_SIZE*sizeof(word));
	output.write(reinterpret_cast<const char*> (psiQuotientBits),ALPHABET_SIZE*sizeof(word));
	output.write(reinterpret_cast<const char*> (lastCodedPsiQuotient),ALPHABET_SIZE*sizeof(word));
	output.write(reinterpret_cast<const char*> (lastQuotientBit),ALPHABET_SIZE*sizeof(word));
	output.write(reinterpret_cast<const char*> (&currentWordSize),sizeof(word));
	output.write(reinterpret_cast<const char*> (&lastSetSequentialIndex),sizeof(word));
	output.write(reinterpret_cast<const char*> (&lastAccessSequentialIndex),sizeof(word));
}

RiceCodedPsi::RiceCodedPsi(std::istream& input):AbstractPsi(input){
	std::cout << "Loading Rice Coded Psi Structure\n";
	for(word i=0;i<ALPHABET_SIZE;i++) psiQuotient[i] = new GMN_StaticDictionary(input);
	for(word i=0;i<ALPHABET_SIZE;i++) psiRemainder[i] = new GMN_StaticDictionary(input);
	input.read(reinterpret_cast<char*> (psiRemainderBits),ALPHABET_SIZE*sizeof(word));
	input.read(reinterpret_cast<char*> (psiQuotientBits),ALPHABET_SIZE*sizeof(word));
	input.read(reinterpret_cast<char*> (lastCodedPsiQuotient),ALPHABET_SIZE*sizeof(word));
	input.read(reinterpret_cast<char*> (lastQuotientBit),ALPHABET_SIZE*sizeof(word));
	input.read(reinterpret_cast<char*> (&currentWordSize),sizeof(word));
	input.read(reinterpret_cast<char*> (&lastSetSequentialIndex),sizeof(word));
	input.read(reinterpret_cast<char*> (&lastAccessSequentialIndex),sizeof(word));
}

RiceCodedPsi::~RiceCodedPsi(){
	for(word i=0;i<ALPHABET_SIZE;i++){
		delete psiQuotient[i];
		delete psiRemainder[i];
	}
}

AbstractSA* AbstractPsi::getSA(){
	return(saPtr);
}

void AbstractPsi::setSA(AbstractSA* A ){
	saPtr =A;
}



