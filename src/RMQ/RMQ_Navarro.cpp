/*
 * RMQ_Navarro.cpp
 *
 *  Created on: Dec 30, 2012
 *      Author: daniel
 */



#include "RMQ_Navarro.hpp"


word RMQ_Navarro::query(word i,word j){
	word rmq;
	word rmqi;
	word rmqj;
	word rmqBlocks;
	word minimum_lcp = MAX_WORD;
	word parent_i,parent_j;
	word childrenPtr;
	word it;
	if(i>j) return query(j,i);

	/**If they are in the same block, a sequential scan suffices**/
	if(tree->get_lcp_leaf(i) == tree->get_lcp_leaf(j)){
		for(word k=i;k<=j;k++){
			if(lcp->access(k)<minimum_lcp){
				minimum_lcp = lcp->access(k);
				rmq = k;
			}
		}
		return rmq;
	}
	/**Otherwise, is necessary to do a query in the i's block and j'block **/

	/**RMQ in i's block**/
	rmqi=i;
	it = (floor((double)i/tree->tree_factor)+1)*(tree->tree_factor);
	for(word k=i;k<it;k++){
		if(lcp->access(k)<minimum_lcp){
			minimum_lcp = lcp->access(k);
			rmqi = k;
		}
	}

	/**RMQ in j's block**/
	rmqj=j;
	it = (tree->tree_factor)*(j/tree->tree_factor);
	for(word k=it;k<=j;k++){
		if(lcp->access(k)<minimum_lcp){
			minimum_lcp = lcp->access(k);
			rmqj = k;
		}
	}

	/**RMQ in interblocks**/
	if(lcp->access(rmqi)<=lcp->access(rmqj)){
		rmqBlocks = rmqi;
	}
	else{
		rmqBlocks = rmqj;
	}
	parent_i = tree->get_lcp_leaf(i);
	parent_j = tree->get_lcp_leaf(j);
	do{
		parent_i = tree->parent(parent_i);
		parent_j = tree->parent(parent_j);

		childrenPtr = tree->children(parent_i);
		for(word k=0;k<tree->tree_factor;k++){
			if(tree->isValid(childrenPtr+k)
					&& lcp->access(tree->getNode(childrenPtr+k)) <= minimum_lcp
					&& tree->getNode(childrenPtr+k) >=i
					&& tree->getNode(childrenPtr+k) <=j){
				if(minimum_lcp == lcp->access(tree->getNode(childrenPtr+k))){
					rmqBlocks = std::min(rmqBlocks,tree->getNode(childrenPtr+k));
				}
				else{
					minimum_lcp = lcp->access(tree->getNode(childrenPtr+k));
					rmqBlocks = tree->getNode(childrenPtr+k);
				}
			}
		}

		childrenPtr = tree->children(parent_j);
		for(word k=0;k<tree->tree_factor;k++){
			if(tree->isValid(childrenPtr+k)
					&& lcp->access(tree->getNode(childrenPtr+k)) <= minimum_lcp
					&& tree->getNode(childrenPtr+k) >=i
					&& tree->getNode(childrenPtr+k) <=j){
				if(minimum_lcp == lcp->access(tree->getNode(childrenPtr+k))){
					rmqBlocks = std::min(rmqBlocks,tree->getNode(childrenPtr+k));
				}
				else{
					minimum_lcp = lcp->access(tree->getNode(childrenPtr+k));
					rmqBlocks = tree->getNode(childrenPtr+k);
				}
			}
		}
	}while(parent_i != parent_j);

	/**Final rmq is the minimum of the previous calculated rmqs**/
	rmq = rmqi;
	if(lcp->access(rmqBlocks)<lcp->access(rmq)) rmq = rmqBlocks;
	if(lcp->access(rmqj)<lcp->access(rmq)) rmq = rmqj;
	return(rmq);
}

void RMQ_Navarro::setLcp(AbstractLcp* lcp){
	this->lcp = lcp;
	tree->setLcp(lcp);
}

void RMQ_Navarro::serialize(std::ostream& output){
	RMQ::serialize(output);
	tree->serialize(output);
}

RMQ_Navarro::RMQ_Navarro(std::istream& input):RMQ(input){
	tree = new n_ary_complete_tree(input);
}

RMQ_Navarro::RMQ_Navarro(AbstractLcp* lcp,word n):RMQ(lcp,n){
	tree = new n_ary_complete_tree(lcp,n);
}

RMQ_Navarro::~RMQ_Navarro(){
	if(tree!=0){
		delete tree;
	}
}
