#include "RMQ.hpp"

RMQ::RMQ(AbstractLcp* lcp,word n){
	this->lcp = lcp;
	this->n = n;
}

RMQ::RMQ(std::istream& input){
	input.read(reinterpret_cast<char*> (&n),sizeof(n));
}

void RMQ::serialize(std::ostream& output){
	output.write(reinterpret_cast<const char*> (&n),sizeof(n));
}


void RMQ::setLcp(AbstractLcp* lcp){
	this->lcp = lcp;
}
