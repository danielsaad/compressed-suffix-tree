#ifndef _RMQ_hpp_
#define _RMQ_hpp_

#include <cmath>
#include <ctime>
#include <iostream>
#include "../LCP/LCP.hpp"
typedef int DT;                 // use long for 64bit-version (but take care of fast log!)
typedef word DTidx;     // for indexing in arrays

/* Abstract class for RMQ-queries. Preprocessing is done
   in constructor. */
class RMQ{
public:
	// returns index of RMQ[i,j]
	RMQ(std::istream& input);
	RMQ(AbstractLcp* lcp,word n);
	virtual DTidx query(DTidx, DTidx) = 0;
	double getBuildTime();
	virtual void serialize(std::ostream& output)=0;
	virtual ~RMQ(){}
	virtual void setLcp(AbstractLcp* lcp);
protected:
	AbstractLcp* lcp;
	word n;
	time_t start;
	time_t end;
};

#endif
