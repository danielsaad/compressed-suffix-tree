/*
 * RMQ_NSV_Navarro.cpp
 *
 *  Created on: Jan 7, 2013
 *      Author: daniel
 */


#include "RMQ_NSV_Navarro.hpp"

RMQ_NSV_Navarro::~RMQ_NSV_Navarro(){
	delete RMQ_Navarro::tree;
	RMQ_Navarro::tree = 0;
	NSV_Navarro::tree = 0;
}

void RMQ_NSV_Navarro::serialize(std::ostream& output){
	RMQ_Navarro::serialize(output);
}

RMQ_NSV_Navarro::RMQ_NSV_Navarro(std::istream& input):
RMQ_Navarro(input),NSV_Navarro(){
	NSV_Navarro::tree = RMQ_Navarro::tree;
	NSV_Navarro::n = RMQ_Navarro::n;
}

RMQ_NSV_Navarro::RMQ_NSV_Navarro(AbstractLcp* lcp,word n):
RMQ_Navarro(lcp,n),NSV_Navarro(){
	NSV_Navarro::tree = RMQ_Navarro::tree;
	NSV_Navarro::n = RMQ_Navarro::n;
}
