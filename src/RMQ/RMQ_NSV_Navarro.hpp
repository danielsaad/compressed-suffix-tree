/*
 * RMQ_NSV_Navarro.hpp
 *
 *  Created on: Jan 7, 2013
 *      Author: daniel
 */

#ifndef RMQ_NSV_NAVARRO_HPP_
#define RMQ_NSV_NAVARRO_HPP_
#include "RMQ_Navarro.hpp"
#include "../NSV/NSV_Navarro.hpp"


class RMQ_NSV_Navarro:public RMQ_Navarro,public NSV_Navarro{
public:
	RMQ_NSV_Navarro(AbstractLcp* lcp, word n);
	RMQ_NSV_Navarro(std::istream& input);
	virtual void serialize(std::ostream& output);
	~RMQ_NSV_Navarro();

};



#endif /* RMQ_NSV_NAVARRO_HPP_ */
