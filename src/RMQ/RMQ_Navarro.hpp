/*
 * RMQ_Navarro.hpp
 *
 *  Created on: Dec 30, 2012
 *      Author: daniel
 */

#ifndef RMQ_NAVARRO_HPP_
#define RMQ_NAVARRO_HPP_


#include "RMQ.hpp"
#include "../DataStructures/n_ary_complete_tree.hpp"

class RMQ_Navarro:public RMQ{
public:
	RMQ_Navarro(std::istream& input);
	RMQ_Navarro(AbstractLcp* lcp,word n);
	word query(word i,word j);
	virtual void setLcp(AbstractLcp* lcp);
	virtual void serialize(std::ostream& output);
	virtual ~RMQ_Navarro();
protected:
	n_ary_complete_tree* tree;
};

#endif /* RMQ_NAVARRO_HPP_ */
