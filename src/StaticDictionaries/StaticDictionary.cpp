/*
 * StaticDictionary.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: daniel
 */

#include "StaticDictionary.hpp"


StaticDictionary::StaticDictionary(word size):BitVector(size){
}

StaticDictionary::StaticDictionary(std::istream& input):BitVector(input){}


void StaticDictionary::serialize(std::ostream& output){
	BitVector::serialize(output);
}

StaticDictionary::~StaticDictionary(){}
