/*
 * EN_StaticDictionary.cpp
 *
 *  Created on: Nov 20, 2013
 *      Author: Daniel Saad Nogueira Nunes
 */


#include <cmath>
#include "EN_StaticDictionary.hpp"
#include "BitOperations.hpp"



/**
 * 	Implementation based on work of Gonzalo Navarro and Eliana Providel.
 * 	Fast, Small, Simple Rank/Select on Bitmaps [1].
 *
 *  [1] Gonzalo Navarro and Eliana Providel.
 *       Fast, Small, Simple Rank/Select on Bitmaps. SEA 2012.
 *
 *  @author Daniel Saad Nogueira Nunes
 */



const word EN_StaticDictionary::RANK_FACTOR;
const word EN_StaticDictionary::SELECT0_FACTOR;
const word EN_StaticDictionary::SELECT1_FACTOR;

EN_StaticDictionary::EN_StaticDictionary(word size):StaticDictionary(size){
	number_of_0s = 0;
	number_of_1s =0;
}

EN_StaticDictionary::EN_StaticDictionary(std::istream& input):StaticDictionary(input){}


word EN_StaticDictionary::rank1(word i){
	word rank = 0;
	word rank_super_block;
	word select1_super_block;
	word k;
	word select_word;
	word ranking_bit;

	/**Obtain the index of the super block**/
	rank_super_block = i/(EN_StaticDictionary::RANK_FACTOR);
	rank = rank_lookup[rank_super_block];

	select1_super_block = rank/EN_StaticDictionary::SELECT1_FACTOR;

	/**Speedup rank queries by avoiding popcounts with select lookup**/

	k=0;
	select_word = select1_lookup[select1_super_block+k+1] >> WORD_SHIFT;
	while(select_word*WORD_SIZE < i){
		k++;
		select_word= select1_lookup[select1_super_block+k+1] >> WORD_SHIFT;
	}

	/**Extract start bit**/
	/**select1_lookup points to the *word* which has the
	 * jth * SELECT1_FACTOR  bit
	 */
    select_word	 =  select1_lookup[select1_super_block+k] >> WORD_SHIFT;
    if(select_word>rank_super_block*WORD_SIZE){
    	/**Select speedup**/
    	/**uses the last log(WORD_SIZE) bits to identify where the select value occurs**/
    	ranking_bit = BitOperations::selectBitsFromWord(select1_lookup[select1_super_block+k],WORD_SIZE-WORD_SHIFT,WORD_SIZE-1);
		rank =  (select1_super_block+k)*SELECT1_FACTOR
			+sequentialPopCount(select_word*WORD_SIZE,i) - ranking_bit;
    }
    else{
    	/**No speed up from select**/
    	rank+=sequentialPopCount(rank_super_block*RANK_FACTOR,i);
    }
	return rank;
}

word EN_StaticDictionary::rank0(word i){
	return(i+1-rank1(i));
}



word EN_StaticDictionary::select0(word i){
	word rank=0;
	word k;
	word rank_boundarie;
	word select0_super_block;
	word select_word;
	word ppcw;

	rank+= SELECT0_FACTOR *  (i/SELECT0_FACTOR);
	select0_super_block = i/SELECT0_FACTOR;
	select_word = select0_lookup[select0_super_block] >> WORD_SHIFT;
    rank-=  BitOperations::selectBitsFromWord(select0_lookup[select0_super_block],WORD_SIZE-WORD_SHIFT,WORD_SIZE-1);
	/**
	 * Do popcounts until we hit a rank barrier
	 */
	rank_boundarie =  select_word+ (RANK_FACTOR/WORD_SIZE - select_word%(RANK_FACTOR/WORD_SIZE));
    for(ppcw = select_word; rank<i && ppcw<rank_boundarie;ppcw++){
		rank+= WORD_SIZE - BitOperations::popCountWord(v[ppcw]);
	}
    /** If the select is before the rank boundary we must inspect the word**/
    if(rank>=i){
    	ppcw--;
    	rank-= WORD_SIZE - BitOperations::popCountWord(v[ppcw]);
        for(k =  0; k<WORD_SIZE && rank!=i;k++){
        	if(((v[ppcw] >> (WORD_SIZE-k-1)) & 1) == 0){
        		rank++;
        	}
        }
    	return(ppcw * WORD_SIZE + k  - 1);
    }

	/**We have hit a rank barrier, now we speed-up select queries by looking up rank values**/
    for(k = (WORD_SIZE*rank_boundarie)/RANK_FACTOR;(k+1)*RANK_FACTOR - rank_lookup[k+1]<i;k++){
        rank = (k+1)*RANK_FACTOR - rank_lookup[k+1];
	}
	/**Continue doing popcounts**/
    for(ppcw = (k*RANK_FACTOR)/WORD_SIZE;rank<i;ppcw++){
        rank+= WORD_SIZE - BitOperations::popCountWord(v[ppcw]);
	}

	/**Inspect final word
	 * TODO: OPTIMIZE
	 **/
    ppcw--;
    rank-=  WORD_SIZE - BitOperations::popCountWord(v[ppcw]);
    for(k =  0; k<WORD_SIZE && rank!=i;k++){
    	if(((v[ppcw] >> (WORD_SIZE-k-1)) & 1) == 0){
    		rank++;
    	}
    }
    return(ppcw * WORD_SIZE + k -1);
}

word EN_StaticDictionary::select1(word i){
	word rank=0;
	word k;
	word rank_boundarie;
	word select1_super_block;
	word select_word;
    word ppcw;

    rank+= SELECT1_FACTOR *  (i/SELECT1_FACTOR);
    select1_super_block = i/SELECT1_FACTOR;
    select_word = select1_lookup[select1_super_block] >> WORD_SHIFT;
    rank-= BitOperations::selectBitsFromWord(select1_lookup[select1_super_block],WORD_SIZE-WORD_SHIFT,WORD_SIZE-1);

	/**
	 * Do popcounts until we hit a rank barrier
	 */
	rank_boundarie =  select_word+ (RANK_FACTOR/WORD_SIZE - select_word%(RANK_FACTOR/WORD_SIZE));
    for(ppcw = select_word; rank<i && ppcw<rank_boundarie;ppcw++){
		rank+= BitOperations::popCountWord(v[ppcw]);
	}
    /** If the select is before the rank boundary we must inspect the word**/
    if(rank>=i){
    	ppcw--;
    	rank-= BitOperations::popCountWord(v[ppcw]);
    	for(k =  0; k<WORD_SIZE && rank!=i;k++){
    		if(((v[ppcw] >> (WORD_SIZE-k-1)) & 1) == 1){
    			rank++;
    		}
    	}
    	return(ppcw * WORD_SIZE + k  - 1);
    }

	/**We have hit a rank barrier, now we speed-up select queries by looking up rank values**/
    for(k = (WORD_SIZE*rank_boundarie)/RANK_FACTOR;rank_lookup[k+1]<i;k++){
        rank =  rank_lookup[k+1];
	}
	/**Continue doing popcounts**/
    for(ppcw = (k*RANK_FACTOR)/WORD_SIZE;rank<i;ppcw++){
        rank+= BitOperations::popCountWord(v[ppcw]);
	}

	/**Inspect final word
	 * TODO: OPTIMIZE
	 **/
    ppcw--;
    rank-=  BitOperations::popCountWord(v[ppcw]);
	for(k =  0; k<WORD_SIZE && rank!=i;k++){
		if(((v[ppcw] >> (WORD_SIZE-k-1)) & 1) == 1){
			rank++;
		}
	}
    return(ppcw * WORD_SIZE + k -1);
}

void EN_StaticDictionary::preprocess(){
	 word rank_words,select_words;
	 word rank_i,select1_i,select0_i;
	 word select_bit;
	 word bits_to_go;
	 word i;
	 word bitc;
	 word ppc;
	 if(v.size()>0){
		 /**
		  * Resize vectors
		  */
		 rank_words =  ceil((double)size/RANK_FACTOR) + 1;
	 	 if(rank_lookup.size()!=rank_words){
	 		 rank_lookup.resize(rank_words);
	 	 }

         /**Do popcounts and store values for rank**/
	     for(i=0,rank_i=0,ppc=0;i<v.size();i++){
	    	 if(i%(RANK_FACTOR/WORD_SIZE)==0){
	    		rank_lookup[rank_i++]=ppc;
	    	 }
	    	 ppc+=BitOperations::popCountWord(v[i]);
	     }

	     rank_lookup[rank_i] = MAX_WORD;

	     number_of_0s = size -ppc;
	     number_of_1s = ppc;



         select_words =  ceil((double)number_of_1s/SELECT1_FACTOR) + 2;
         if(select1_lookup.size()!=select_words){
             select1_lookup.resize(select_words);
         }

         select_words =  ceil((double)number_of_0s/SELECT0_FACTOR) + 2;
         if(select0_lookup.size()!=select_words){
                 select0_lookup.resize(select_words);
         }

         /**Do popcounts and store values for select0 and select1**/
         select1_lookup[0] = 0;
         select0_lookup[0] = 0;
         for(i=0,select0_i=1,select1_i = 1,ppc=0;i<v.size();i++){

        	 /**Select1 preprocessing**/
        	 if(ppc >= select1_i*SELECT1_FACTOR){
        		 bits_to_go = select1_i*SELECT1_FACTOR - (ppc - BitOperations::popCountWord(v[i-1]));
        		 for(bitc=0,select_bit = 0; bitc < bits_to_go ; select_bit++){
        			 if(((v[i-1] >> (WORD_SIZE-select_bit-1)) & 0x1) == 1){
        				 bitc++;
        			 }
        		 }
                 select1_lookup[select1_i++] =  ((i-1)<<WORD_SHIFT) | bitc;
        	 }

        	 /**Select0 preprocessing**/

        	 if((i*WORD_SIZE - ppc) >= select0_i*SELECT0_FACTOR){
        		 bits_to_go = select0_i*SELECT0_FACTOR -
        				 (i*WORD_SIZE-ppc - (WORD_SIZE-BitOperations::popCountWord(v[i-1])));
        		 for(bitc=0,select_bit = 0; bitc < bits_to_go ; select_bit++){
        			 if(((v[i-1] >> (WORD_SIZE-select_bit-1)) & 0x1) == 0){
        				 bitc++;
        			 }
        		 }
                 select0_lookup[select0_i++] =  ((i-1)<<WORD_SHIFT) | bitc;
        	 }

        	 ppc+=BitOperations::popCountWord(v[i]);
         }
         select1_lookup[select1_i] = MAX_WORD;
         select0_lookup[select0_i] = MAX_WORD;
	 }
}


void EN_StaticDictionary::serialize(std::ostream& output){
	StaticDictionary::serialize(output);
	//TODO: serialize
}



EN_StaticDictionary::~EN_StaticDictionary(){}
