/*
 * StaticDictionary.hpp
 *
 *  Created on: Oct 28, 2013
 *      Author: daniel
 */

#ifndef STATICDICTIONARY_HPP_
#define STATICDICTIONARY_HPP_

#include "../Definitions/types.hpp"
#include "BitVector.hpp"

#include <iostream>
#include <vector>

/**
    BitVector Data Structure using words to pack bits.
**/
class StaticDictionary: public BitVector{
public:

	StaticDictionary(word size);
    StaticDictionary(std::istream& input);

    /**A Static Dictionary should provide rank and select queries
     * and also a preprocessing technique to answer these queries efficiently**/
    virtual word rank1(word n) = 0;
    virtual word rank0(word n) = 0;
    virtual word select1(word i) = 0;
    virtual word select0(word i) = 0;
    virtual void preprocess() = 0;
    virtual void serialize(std::ostream& output);

    virtual ~StaticDictionary();

};

#endif /* STATICDICTIONARY_HPP_ */
