/*
 * GMN_StaticDictionary.cpp
 *
 *  Created on: Oct 28, 2013
 */

/** Implementation of Rodrigo Gonzalez et al. practical rank/select solution [1].
 *
 *  [1] Rodrigo Gonzalez, Szymon Grabowski, Veli Makinen, and Gonzalo Navarro.
 *      Practical Implementation of Rank and Select Queries. WEA05.
 *
 *  @author Daniel Saad Nogueira Nunes
 */

#include "GMN_StaticDictionary.hpp"
#include <cmath>


GMN_StaticDictionary::GMN_StaticDictionary(std::istream& input):StaticDictionary(input){
	word rankSize;
	input.read(reinterpret_cast<char*> (&rankSize),sizeof(word));
	if(rankSize>0){
		rankLookUp.resize(rankSize);
		input.read(reinterpret_cast<char*> (&(rankLookUp.front())),sizeof(word)*rankSize);
	}
}

GMN_StaticDictionary::GMN_StaticDictionary(word n):StaticDictionary(n){}


/**
    Returns the numbers of 1's of the bitvector's prefix of length
    n. To do this, we do a look-up in the rank table and
    a sequential population count.
    @param n the length of the prefix in the bitvector.
    @return the number of 1's in the prefix
**/


word GMN_StaticDictionary::rank1(word n){
    word superBlock = n/(WORD_SIZE * GMN_StaticDictionary::RANK_FACTOR);
    word start = superBlock*WORD_SIZE* GMN_StaticDictionary::RANK_FACTOR;
    word end = n;
    return(rankLookUp[superBlock] + sequentialPopCount(start,end));
}
/**
    Returns the number of 0's in the bitvector's prefix of length n. We use
    rank1 for this and then subtract from the total length.
    @param n the length of the prefix in the bitvector.
    @return the number of 0's in the prefix
**/
word GMN_StaticDictionary::rank0(word n){
    return(n+1-rank1(n));
}

/**
    Returns the position of the $i^{th}$ 1 bit of the Bit-vector.
    Uses a binary search in the rank lookup table and the a linear
    scan using popcount.
    @param i the $i^{th}$ one.
    @return the position of the $i^{th}$ one.
**/

word GMN_StaticDictionary::select1(word i){
    word mid,l,r,c,temp;
    word offset;
    word wStart;
    word popC;
    word index=0;
    l = 0;
    r = rankLookUp.size()-1;
    /**Binary Search for the superblock**/
    while(l<r){
        mid = (l+r)/2;
        if(rankLookUp[mid] < i){
            l = mid+1;
            index = mid;
        }
        else{
            r=mid-1;
        }
    }
    if(l==r){
        if(rankLookUp[l]<i){
            index = l;
        }
        else if(rankLookUp[l]==i){
            index = l;
            if(index>0) index--;
        }
    }
    offset = index*RANK_FACTOR*WORD_SIZE;
    /**Sequential search using popCount**/
    c = i - rankLookUp[index];
    wStart = index * GMN_StaticDictionary::RANK_FACTOR;
    popC =  BitOperations::popCountWord(v[wStart]);
    while(c > popC){
        c -= popC;
        wStart++;
        popC =  BitOperations::popCountWord(v[wStart]);
        offset+= WORD_SIZE;
    }
    /**Sequential search using bitwise**/
    for(temp = 0;c!=0;temp++){
        if(BitOperations::retrieveBit(v[wStart],temp)==1){
            c--;
        }
    }
    offset+=temp-1;
    return(offset);
}

/**
    Returns the position of the $i^{th}$ 0 bit of the Bit-vector.
    Uses a binary search in the rank lookup table and the a linear
    scan using popcount.
    @param i the $i^{th}$ zero.
    @return the position of the $i^{th}$ zero.
**/

word GMN_StaticDictionary::select0(word i){
    word mid,l,r,c,temp;
    word offset;
    word wStart;
    word popC;
    word index = 0;
    l=0;
    r=rankLookUp.size()-1;
    /**Binary Search for the superblock**/
    while(l<r){
        mid = (l+r)/2;
        if(mid*WORD_SIZE*RANK_FACTOR - rankLookUp[mid] < i){
            l = mid+1;
            index = mid;
        }
        else{
            r=mid-1;
        }
    }

   if(l==r){
        if(l*WORD_SIZE*GMN_StaticDictionary::RANK_FACTOR-rankLookUp[l]<i){
            index = l;
        }
        else if(l*WORD_SIZE*RANK_FACTOR - rankLookUp[l]==i){
            index = l;
            if(index>0) index--;
        }
    }

    offset = index*GMN_StaticDictionary::RANK_FACTOR*WORD_SIZE;
    /**Sequential search using popCount**/
    c = i - (index*WORD_SIZE* RANK_FACTOR  - rankLookUp[index]);
    wStart = index*RANK_FACTOR;
    popC =  WORD_SIZE - BitOperations::popCountWord(v[wStart]);
    while(c > popC){
        c -= popC;
        wStart++;
        popC = WORD_SIZE- BitOperations::popCountWord(v[wStart]);
        offset+= WORD_SIZE;
    }
    /**Sequential search using bitwise**/
    for(temp = 0;c!=0;temp++){
        if(BitOperations::retrieveBit(accessWord(wStart),temp)==0){
            c--;
        }
    }
    offset+=temp-1;
    return(offset);
}

GMN_StaticDictionary::~GMN_StaticDictionary(){
	rankLookUp.clear();
}

void GMN_StaticDictionary::serialize(std::ostream& output){
	StaticDictionary::serialize(output);
	word rankSize=rankLookUp.size();
	output.write(reinterpret_cast<const char*> (&rankSize),sizeof(word));
	if(rankSize>0)
		output.write(reinterpret_cast<const char*> (&(rankLookUp.front())),sizeof(word)*rankSize);
}


/**
    Preprocess the bit-vector to compute the rank loookup table.
    The rank is preprocessed at each RANK_FACTOR word.
**/
void GMN_StaticDictionary::preprocess(){
    word i;
    word totalBits;
    word c;
    word start,end;
    word rankWords;
    if(v.size()>0){
        rankWords =  ceil((double)size/(RANK_FACTOR*WORD_SIZE));
        if(rankLookUp.size()!=rankWords){
            rankLookUp.resize(rankWords);
        }
        totalBits = size;
        start = 0;
        rankLookUp[0] = 0;
        end =  WORD_SIZE*RANK_FACTOR -1 < totalBits-1 ?
            WORD_SIZE*RANK_FACTOR -1 : totalBits-1 ;
        c=0;
        i=1;
        while(i< rankWords){
            c += sequentialPopCount(start,end);
            totalBits -= (end-start+1);
            start = end+1;
            end =  WORD_SIZE*RANK_FACTOR -1 < totalBits-1 ?
                start+WORD_SIZE*RANK_FACTOR -1 : start+totalBits-1 ;
            rankLookUp[i]=c;
            i++;
        }
    }
}
