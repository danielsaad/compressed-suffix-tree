#include <cmath>
#include "BitOperations.hpp"
#include "BitVector.hpp"
/**
    Constructor for the BitVector data structure
    @param n the number of bits in the bit vector
    @return The bitvector structure
**/
BitVector::BitVector(word n){
	BitOperations* singleton = BitOperations::getSingletonInstance();
    word numWords = ceil((double)n/WORD_SIZE);
    if(n!=0){
        v.resize(numWords);
    }
    size = n;
}

BitVector::BitVector(std::istream& input){

	word vSize;
	input.read(reinterpret_cast<char*> (&size),sizeof(word));
	input.read(reinterpret_cast<char*> (&vSize),sizeof(word));
	if(size>0){
		if(vSize>0){
			v.resize(vSize);
			input.read(reinterpret_cast<char*> (&(v.front())),sizeof(word)*vSize);
		}
	}
}

BitVector::~BitVector(){
    v.clear();
}



/**
    Does a population count in the interval [start,end] of the bitVector.
    start must be a word boundarie for performance issues
    @param start the start position of the popcount operation. (WORD BOUNDARIE)
    @param end the end position of the popcount operation.

**/
word BitVector::sequentialPopCount(word start,word end){
    word i,k,j;
    word c=0;
    /**For each word do a standard popCount**/
    for(i= (start >> WORD_SHIFT);i < (end >> WORD_SHIFT);i++){
        c+=BitOperations::popCountWord(v[i]);
    }

    /**Now we do popCounts until the last byte, which we look
    up in bitPopCount**/


    for(j=WORD_SIZE/16-1,k = end & WORD_MOD;k>15;j--,k-=16){
        c+=BitOperations::popCount[(v[i]>>(j<<4)) & 0xffff];
    }
    c+=BitOperations::bitPopCount[k][(v[i]>>(j<<4)) & 0xffff];

    return(c);
}


/**
    Set the word of nBits in the bitvector starting from start.
    @param word the word containing the bits to be set
    @param nBits number of bits of the word to be set.
    @param start the start position in the bitvector from which the bits will be set.
**/
void BitVector::setBits(word w,word nBits, word start){
    word startW,sl,sr;
    startW = start >> WORD_SHIFT;
    start = start & WORD_MOD;
    if(WORD_SIZE-start  >= nBits){
        sl = WORD_SIZE-start-nBits;
        v[startW] &= ~(BitOperations::maskRangeAccess[start][start+nBits-1]);
        v[startW] |=  (w<<sl);
    }
    else{
        sr = nBits - WORD_SIZE + start;
        v[startW] &= (~BitOperations::maskRangeAccess[start][WORD_SIZE-1]);
        v[startW] |=  (w>>sr);
        v[startW+1] &= (~BitOperations::maskRangeAccess[0][(start+nBits-1) & WORD_MOD]);
        v[startW+1] |=  (w << (WORD_SIZE - sr));
    }
}




/**
    Set a single bit in a position of the bitvector.
    @param i the position to be set.
**/
void BitVector::setBit(word i){
    word startW;
    startW =  i >> WORD_SHIFT;
    i = i & WORD_MOD;
    v[startW] |= BitOperations::maskAccess[i];
}


/**
    Read a bit from the bitvector structure.
    @param i the desired bit to be read.
    @return return the bit.
**/
word BitVector::readBit(word i){
    word w = i>>WORD_SHIFT;
    return(BitOperations::retrieveBit(v[w],i & WORD_MOD));
}



/**
    Extract few bits between [start,end] (less than word size) of a bitvector and return the word.
    @param start the start position of the extraction.
    @param end the end position of the extraction.
**/
word BitVector::extractBits(word start, word end){
    word extracted;
    word h,l;
    word wStart;
    word wEnd;
    if(end<start){
        return 0;
    }
    wStart = start >> WORD_SHIFT;
    wEnd = end>> WORD_SHIFT;
    start = start & WORD_MOD;
    end = end & WORD_MOD;
    if(wStart == wEnd){
        extracted = (v[wStart] & BitOperations::maskRangeAccess[start][end]) >> (WORD_SIZE-end-1);
    }
    else{
        h = v[wStart] & BitOperations::maskRangeAccess[start][WORD_SIZE-1];
        h <<= end+1;
        l = (v[wEnd] & BitOperations::maskRangeAccess[0][end]);
        l >>=  (WORD_SIZE-end-1);
        extracted = h | l;
    }
    return(extracted);
}


/**
    Fill the [start,end] interval of the bitVector with zeroes
    @param start The start of the interval
    @param end The end of the interval
**/
void BitVector::zeroFill(word start, word end){
    word i;
    word startW = start >> WORD_SHIFT;
    word endW = end >> WORD_SHIFT;
    start = start & WORD_MOD;
    end = end & WORD_MOD;
    if(startW==endW){
        v[startW] &= ~BitOperations::maskRangeAccess[start][end];
    }
    else{
        v[startW] &= ~BitOperations::maskRangeAccess[start][WORD_SIZE-1];
        for(i=startW+1;i<endW;i++){
            v[i]=0;
        }
        v[endW]&= ~BitOperations::maskRangeAccess[0][end];
    }
}






/***** OPERATIONS IN  THE BIT-VECTOR*****/


word BitVector::accessWord(word i){
    return(v[i]);
}

/**
    Gets the member size
    @return size
**/

word BitVector::getSize(){
    return(size);
}
/**
    Resize the bitvector structure
    @param size the new size in bits.
**/
void BitVector::resize(word size){
    this->size = size;
    v.resize(ceil((double)size/WORD_SIZE));
}

/**
    Clear BitVector contents (fill with 0's).
**/
void BitVector::reset(){
    fill(v.begin(),v.end(),0);
}





void BitVector::serialize(std::ostream& output){
	word vSize = v.size();
	output.write(reinterpret_cast<const char*> (&size),sizeof(word));
	output.write(reinterpret_cast<const char*> (&vSize),sizeof(word));
	if(size>0){
		if(vSize>0)
			output.write(reinterpret_cast<const char*> (&(v.front())),sizeof(word)*vSize);
	}
}
