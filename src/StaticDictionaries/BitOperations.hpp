#ifndef BITOP_H_INCLUDED
#define BITOP_H_INCLUDED


#include "../Definitions/types.hpp"
/**
 * Class providing Bit operations.
 * This class should be a singleton, since the lookup tables are the same
 * in all instances of this class.
 */
class BitOperations{
public:
	/**
	    @var value one
	**/
	const static word one = 1;
	/**
	    @var value zero
	**/
	const static word zero = 0;

    static word retrieveNumBits(word n);
    static word concatenateWords(word a, word b,word nBitsa);
    static word selectBitsFromWord(word a,int i,int j);
    static word retrieveLeadingZeroes(word a);
    static word popCountWord(word w);
    static word retrieveBit(word w,word i);
    static BitOperations* getSingletonInstance();

	static word maskAccess[WORD_SIZE];
	static word maskRangeAccess[WORD_SIZE][WORD_SIZE];
	static word bitPopCount[16][65536];
	static word popCount[65536];
	static word leadingZeroes[65536];

private:
	BitOperations();
	void initMaskAccess(void);
	void initPopCount(void);
	void initLeadingZeroes(void);
	void initRangeAccess(void);
	void initAllBitTables(void);



};

#endif
