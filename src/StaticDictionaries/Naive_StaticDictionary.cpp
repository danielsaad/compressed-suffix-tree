/*
 * Naive_StaticDictionary.cpp
 *
 *  Created on: Dec 1, 2013
 *      Author: Daniel Saad Nogueira Nunes
 */

#include "Naive_StaticDictionary.hpp"
#include "BitOperations.hpp"

Naive_StaticDictionary::Naive_StaticDictionary(word size):StaticDictionary(size){}

word Naive_StaticDictionary::rank0(word i){
	return(i - sequentialPopCount(0,i)+1);
}

word Naive_StaticDictionary::rank1(word i){
	return(sequentialPopCount(0,i));
}

word Naive_StaticDictionary::select0(word i){
	word k;
	word w;
	word rank=0;

	for(w=0;w<v.size();w++){
		for(k=0;k<WORD_SIZE;k++){
			if(BitOperations::retrieveBit(v[w],k)==0){
				rank++;
			}
			if(rank==i){
				return(w*WORD_SIZE+k);
			}
		}
	}

}

word Naive_StaticDictionary::select1(word i){
	word k;
	word w;
	word rank = 0;
	for(w=0;w<v.size();w++){
		for(k=0;k<WORD_SIZE;k++){
			if(BitOperations::retrieveBit(v[w],k)==1){
				rank++;
			}
			if(rank==i){
				return((w*WORD_SIZE)+k);
			}
		}
	}
}

void Naive_StaticDictionary::preprocess(){}


Naive_StaticDictionary::~Naive_StaticDictionary(){}
