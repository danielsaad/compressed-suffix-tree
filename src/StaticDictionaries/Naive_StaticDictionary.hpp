/*
 * Naive_StaticDictionary.hpp
 *
 *  Created on: Dec 1, 2013
 *      Author: Daniel Saad Nogueira Nunes
 */


#include "StaticDictionary.hpp"


/**
 * Naive implementation for test purposes only
 */


class Naive_StaticDictionary:public StaticDictionary{
public:
	Naive_StaticDictionary(word size);

	word rank0(word i);
	word rank1(word i);
	word select0(word i);
	word select1(word i);
	void preprocess();

	virtual ~Naive_StaticDictionary();
};


