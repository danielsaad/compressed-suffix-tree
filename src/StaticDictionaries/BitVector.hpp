#ifndef BITVECTOR_H_INCLUDED
#define BITVECTOR_H_INCLUDED


#include <iostream>
#include <vector>
#include "../Definitions/types.hpp"



class BitVector{
public:
	BitVector(word size);
	BitVector(std::istream& input);

    word sequentialPopCount(word start,word end);

    void setBits(word w,word nBits,word start);

    void setBit(word i);

    word readBit(word i);

    word extractBits(word start, word end);

    void zeroFill(word start, word end);


    void resize(word size);

    void reset();


    word getSize();
    word accessWord(word i);

    /**Save and load bitVector Structure**/
    virtual void serialize(std::ostream& output);
    /**Destructor**/
    virtual ~BitVector();
protected:
    word size; /**number of bits of a BitVector**/
    std::vector<word> v; /**BitVector**/

};


#endif // BITVECTOR_H_INCLUDED
