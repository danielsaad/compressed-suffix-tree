
#include "BitOperations.hpp"
#include <cmath>

/**
    @file Provides Implementation of bit operations
**/

/**
	@var Masks for isolation of the $i^{th}$-bit in a word
**/
word BitOperations::maskAccess[WORD_SIZE];
/**
	@var Masks for isolation of a specific range [i,j] in a word.
**/
word BitOperations::maskRangeAccess[WORD_SIZE][WORD_SIZE];
/**
	@var Gives the number of ones in a byte counting for the $i^{th}$ first bits.
**/
word BitOperations::bitPopCount[16][65536];
/**
	@var Gives the number of ones in a byte
**/
word BitOperations::popCount[65536];
/**
	@var Gives the number of leading Zeroes
**/
word BitOperations::leadingZeroes[65536];

const word BitOperations::zero;

const word BitOperations::one;



BitOperations::BitOperations(){
	initAllBitTables();
}

BitOperations* BitOperations::getSingletonInstance(){
	static BitOperations* instance = 0;
	static bool is_initialized = false;
	if(is_initialized==false){
		is_initialized=true;
		instance = new BitOperations();
	}
	return instance;
}


/**
    Initializes the maskAccess array. Most Significant bit
    occupies position 0.
**/
void BitOperations::initMaskAccess(void){
    word i;
    for(i=0;i<WORD_SIZE;i++){
        maskAccess[i] = one << (word)(WORD_SIZE - i -1);
    }
}

/**
    Initializes the maskRangeAcces array
**/
void BitOperations::initRangeAccess(void){
    word i,j;
    for(i=0;i<WORD_SIZE;i++){
        maskRangeAccess[i][i] =  maskAccess[i];
        for(j=i+1;j<WORD_SIZE;j++){
            maskRangeAccess[i][j] = maskRangeAccess[i][j-1] | (maskAccess[j]);
        }
    }
}

/**
    Initializes the popCount and the bitPopCount arrays
**/
void BitOperations::initPopCount(void){
    word i,j,c;
    for(i=0;i<65536;i++){
        c=0;
        for(j=0;j<16;j++){
            if(retrieveBit(i,WORD_SIZE-16+j)==1){
                bitPopCount[j][i] = ++c;
            }
            else{
                bitPopCount[j][i] = c;
            }
        }
    }
    for(j=0;j<65536;j++){
        popCount[j] = bitPopCount[15][j];
    }
}

void BitOperations::initLeadingZeroes(void){
    word i;
    leadingZeroes[0]=16;
    for(i=1;i<65536;i++){
        leadingZeroes[i] = 16 - retrieveNumBits(i);
    }
}

/**
    Function that calls all other initialization
    functions
**/
void BitOperations::initAllBitTables(void){
    initMaskAccess();
    initPopCount();
    initRangeAccess();
    initLeadingZeroes();
}






/*****  UTILITIES FUNCTIONS *****/

/**
    Retrieve the ith-bit from a word.
    Least significant bit start from position zero.
    @param w word which the bit will be retrieved.
    @param i the position of the desired bit to be retrieved.
    @return returns zero or one, the desired bit.
**/
word BitOperations::retrieveBit(word w,word i){
    return( (w & maskAccess[i]) == BitOperations::zero ? BitOperations::zero : BitOperations::one);
}


/**
    Concatenate two words by shifting the first one
    by the size of the other.
    @param a first word.
    @param b second word.
    @param nBitsB number of bits in B.
**/
word BitOperations::concatenateWords(word a, word b,word nBitsB){
    a <<= nBitsB;
    return(a|b);
}

/**
    Select the bits from a word in the range [i,j]
    @param a the word.
    @param i beginning of the interval.
    @param j end of the interval.
**/
word BitOperations::selectBitsFromWord(word a,int i,int j){
    if(j<i) return 0;
    return(a & maskRangeAccess[i][j]);
}


/**
    Retrieve the number of bits in a word.
    @param n word.
    @return the number of bits of n.
**/
word BitOperations::retrieveNumBits(word n){
    return (n==0 ? 1 : floor(log2((double)n))+1 );
}




/**
    Retrieves the ammount of leading zeroes on a word.
    @param The word
    @return Leading zeroes of a.
**/
word BitOperations::retrieveLeadingZeroes(word a){
    word lz;
    if((lz=leadingZeroes[(a >> 16) & 0xffff])==16){
        lz += leadingZeroes[a & 0xffff];
    }
    return(lz);
}

/**
    Popcounts one word. 2 memory access if is 32-bit word, 4 access otherwise.
    Uses a precomputed table to do the popcount.
    @param word w wich will be popcounted
    @return The number of ones in the word.
**/


word BitOperations::popCountWord(word w){

#ifdef ARCH_32
	return(__builtin_popcount(w));
    //return(popCount[w & 0xffff] + popCount[(w >> 16) & 0xffff]);
#else
    return(popCount[w & 0xffff] +
           popCount[(w >> (word)16) & 0xffff] +
           popCount[(w >> (word)32) & 0xffff] +
           popCount[(w >> (word)48) & 0xffff]);
#endif
}
