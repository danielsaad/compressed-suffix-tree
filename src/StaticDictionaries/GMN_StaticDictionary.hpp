/*
 * GMN_StaticDictionary.hpp
 *
 *  Created on: Oct 28, 2013
 *      Author: daniel
 */

#ifndef GMN_STATICDICTIONARY_HPP_
#define GMN_STATICDICTIONARY_HPP_

#include "StaticDictionary.hpp"
#include "BitOperations.hpp"
#include "BitVector.hpp"

/** Implementation of Rodrigo Gonzalez et al. practical rank/select solution [1].
 *
 *  [1] Rodrigo Gonzalez, Szymon Grabowski, Veli Makinen, and Gonzalo Navarro.
 *      Practical Implementation of Rank and Select Queries. WEA05.
 *
 *  @author Daniel Saad Nogueira Nunes
 */

class GMN_StaticDictionary : public StaticDictionary{
public:
	GMN_StaticDictionary(std::istream& input);
	GMN_StaticDictionary(word size);
    static const word RANK_FACTOR = 16;

    virtual word rank1(word i);
    virtual word rank0(word i);
    virtual word select1(word i);
    virtual word select0(word i);
    virtual void preprocess();

	virtual void serialize(std::ostream& output);
	virtual ~GMN_StaticDictionary();

protected:
    std::vector<word> rankLookUp; /**Table for rank look-up**/
};



#endif /* GMN_STATICDICTIONARY_HPP_ */
