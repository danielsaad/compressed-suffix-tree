/*
 * EN_StaticDictionary.hpp
 *
 *  Created on: Nov 20, 2013
 *      Author: Daniel Saad
 */

#ifndef EN_STATICDICTIONARY_HPP_
#define EN_STATICDICTIONARY_HPP_

#include "StaticDictionary.hpp"

class EN_StaticDictionary:public StaticDictionary{
public:
	EN_StaticDictionary(std::istream& input);
	EN_StaticDictionary(word size);
    static const word RANK_FACTOR = 1024; //divisible by a computer word
    static const word SELECT0_FACTOR = 8192; //divisible by a computer word
    static const word SELECT1_FACTOR = 8192; //divisible by a computer word


    virtual word rank1(word n);
    virtual word rank0(word n);
    virtual word select1(word i);
    virtual word select0(word i);
    virtual void preprocess();

	virtual void serialize(std::ostream& output);
	virtual ~EN_StaticDictionary();

protected:
	std::vector<word> rank_lookup; /**@var Table for rank look-up**/
	std::vector<word> select0_lookup; /**@var Table for select 0 look-up**/
	std::vector<word> select1_lookup; /**@var Table for select 1 look-up**/
	word number_of_0s;
	word number_of_1s;
};


#endif /* EN_STATICDICTIONARY_HPP_ */
