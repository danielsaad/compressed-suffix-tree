/*
 * NSV_Naive.hpp
 *
 *  Created on: Dec 31, 2012
 *      Author: daniel
 */

#ifndef NSV_NAIVE_HPP_
#define NSV_NAIVE_HPP_

#include "NSV.hpp"

class NSV_Naive: public NSV{
public:
	NSV_Naive(std::istream& input);
	NSV_Naive(AbstractLcp* lcp,word n);
	word nsv(const word i);
	word psv(const word i);
	void serialize(std::ostream& input);
	virtual ~NSV_Naive();
};




#endif /* NSV_NAIVE_HPP_ */
