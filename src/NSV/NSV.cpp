/*
 * NSV.cpp
 *
 *  Created on: Dec 28, 2012
 *      Author: daniel
 */



#include "NSV.hpp"

NSV::NSV():lcp(0),n(0){}

NSV::NSV(AbstractLcp* lcp,word n){
	this->lcp = lcp;
	this->n = n;
}

NSV::NSV(std::istream& input){
	input.read(reinterpret_cast<char*> (&n),sizeof(n));
}

void NSV::serialize(std::ostream& output){
	output.write(reinterpret_cast<char*> (&n),sizeof(n));
}

NSV::~NSV(){}

void NSV::setLcp(AbstractLcp* lcp){
	this->lcp = lcp;
}
