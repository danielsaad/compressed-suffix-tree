/*
 * NSV_Navarro.hpp
 *
 *  Created on: Dec 31, 2012
 *      Author: daniel
 */

#ifndef NSV_NAVARRO_HPP_
#define NSV_NAVARRO_HPP_

#include "NSV.hpp"
#include "../DataStructures/n_ary_complete_tree.hpp"

class NSV_Navarro:public NSV{
public:
	NSV_Navarro();
	NSV_Navarro(AbstractLcp*lcp, word n);
	NSV_Navarro(std::istream& input);
	word query(word i, word j);
	void serialize(std::ostream& output);
	word nsv(const word i);
	word psv(const word i);
	virtual ~NSV_Navarro();

protected:
	n_ary_complete_tree* tree;
};



#endif /* NSV_NAVARRO_HPP_ */
