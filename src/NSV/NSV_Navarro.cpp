/*
 * NSV_Navarro.cpp
 *
 *  Created on: Dec 31, 2012
 *      Author: daniel
 */


#include "NSV_Navarro.hpp"

NSV_Navarro::NSV_Navarro():NSV(),tree(0){}

NSV_Navarro::NSV_Navarro(AbstractLcp* lcp,word n):NSV(lcp,n){
	tree = new n_ary_complete_tree(lcp,n);
}


NSV_Navarro::NSV_Navarro(std::istream& input):NSV(input){
	tree = new n_ary_complete_tree(input);
}

NSV_Navarro::~NSV_Navarro(){
	if(tree!=0){
		delete tree;
	}
}



void NSV_Navarro::serialize(std::ostream& output){
	tree->serialize(output);
}




word NSV_Navarro::nsv(word i){
	word childrenPtr;
	word currentLcp = lcp->access(i);
	word notFound = n-1;
	word internalNode = tree->get_lcp_leaf(i);
	bool foundInternalNode= false;

	/**Scan block**/
	for(word k=i+1 ; (k < ((i/tree->tree_factor)+1)*tree->tree_factor) && (k<n); k++){
		if(lcp->access(k)<currentLcp){
			return k;
		}
	}

	/**Bottom up search**/
	while(tree->isRoot(internalNode)==false && foundInternalNode==false){
		internalNode = tree->parent(internalNode);
		if(lcp->access(tree->getNode(internalNode))<currentLcp){
			childrenPtr = tree->children(internalNode);
			for(word k=0;k<tree->tree_factor && foundInternalNode==false;k++){
				if((tree->isValid(childrenPtr+k)) && (lcp->access(tree->getNode(childrenPtr+k))< currentLcp) &&
						(tree->getNode(childrenPtr+k)>i)){
					foundInternalNode=true;
				}
			}
		}
	}
	/**Top Down search**/
	foundInternalNode = false;
	while((tree->isLeaf(internalNode)==false)){
		childrenPtr = tree->children(internalNode);
		foundInternalNode=false;
		for(word k=0;k<tree->tree_factor && foundInternalNode ==false;k++){
			internalNode = childrenPtr+k;
			if((tree->isValid(childrenPtr+k)) && (lcp->access(tree->getNode(childrenPtr+k)) < currentLcp)
					&&  (tree->getNode(childrenPtr+k)>i)){
				foundInternalNode = true;
				break;
			}
		}
		if(foundInternalNode==false){
			return(notFound);
		}
	}
	/**Search the remaining block**/
	word offset = internalNode-(tree->get_number_of_nodes() - tree->get_number_of_leaves());
	for(word k = offset*tree->tree_factor;k<(offset+1)*tree->tree_factor && k<n;k++){
		if((k>i) && lcp->access(k)<currentLcp){
			return(k);
		}
	}
	return(notFound);
}



word NSV_Navarro::psv(word i){
	word childrenPtr;
	word currentLcp = lcp->access(i);
	word notFound = MAX_WORD;
	word internalNode = tree->get_lcp_leaf(i);
	bool foundInternalNode;
	/**Scan block**/
	for(word k=i-1 ; k >= ((i/tree->tree_factor))*tree->tree_factor  && (k!=MAX_WORD); k--){
		if(lcp->access(k)<currentLcp){
			return k;
		}
	}

	/**Bottom up search**/
	foundInternalNode = false;
	while(tree->isRoot(internalNode)==false && foundInternalNode==false){
		internalNode = tree->parent(internalNode);
		if(lcp->access(tree->getNode(internalNode))<currentLcp){
			childrenPtr = tree->children(internalNode);
			for(word 	k = tree->tree_factor-1; k!=MAX_WORD && foundInternalNode==false;k--){
				if((tree->isValid(childrenPtr+k)) && (lcp->access(tree->getNode(childrenPtr+k)) < currentLcp)
						&&  (tree->getNode(childrenPtr+k)<i)){
					foundInternalNode=true;
				}
			}
		}
	}
	/**Top Down search**/
	while((tree->isLeaf(internalNode)==false)){
		childrenPtr = tree->children(internalNode);
		foundInternalNode = false;
		for(word k=tree->tree_factor-1;k!=MAX_WORD  && foundInternalNode == false ;k--){
			internalNode = childrenPtr+k;
			if((tree->isValid(childrenPtr+k)) && (lcp->access(tree->getNode(childrenPtr+k)) < currentLcp)
					&&  (tree->getNode(childrenPtr+k)<i)){
					foundInternalNode=true;
					break;
			}
		}
		if(foundInternalNode==false){
			return(notFound);
		}
	}
	/**Search the remaining block**/
	word offset = internalNode-(tree->get_number_of_nodes() - tree->get_number_of_leaves());
	for(word k = (offset+1)*tree->tree_factor-1;k>=(offset)*tree->tree_factor && k!=MAX_WORD;k--){
		if((k<i) && lcp->access(k)<currentLcp){
			return(k);
		}
	}
	return(notFound);
}

