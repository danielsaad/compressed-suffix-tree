/*
 * NSV_Naive.cpp
 *
 *  Created on: Dec 31, 2012
 *      Author: daniel
 */



#include "NSV_Naive.hpp"





NSV_Naive::NSV_Naive(AbstractLcp* Lcp,word n):NSV(lcp,n){}


NSV_Naive::NSV_Naive(std::istream& input):NSV(input){}

void NSV_Naive::serialize(std::ostream& output){
	NSV::serialize(output);
}


word NSV_Naive::nsv(word i){
	for(word k=i+1;k<n;k++){
		if(lcp->access(k)<lcp->access(i)){
			return k;
		}
	}
	return(n-1);
}

word NSV_Naive::psv(word i){
	for(word k=i-1;k!=MAX_WORD;k--){
		if(lcp->access(k)<lcp->access(i)){
			return k;
		}
	}
	return(0);
}

NSV_Naive::~NSV_Naive(){}
