/*
 * NSV.hpp
 *
 *  Created on: Dec 28, 2012
 *      Author: daniel
 */

#ifndef NSV_HPP_
#define NSV_HPP_

#include "../LCP/LCP.hpp"


class NSV{
public:
	NSV();
	NSV(AbstractLcp* lcp,word n);
	NSV(std::istream& input);

	virtual word nsv(const word i) = 0;
	virtual word psv(const word i) = 0;
	virtual void serialize(std::ostream& output) = 0;

	virtual void setLcp(AbstractLcp* lcp);
	virtual ~NSV();
protected:
	AbstractLcp* lcp;
	word n;
};


#endif /* NSV_HPP_ */
