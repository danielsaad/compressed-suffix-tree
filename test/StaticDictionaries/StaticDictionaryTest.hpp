/*
 * StaticDictionaryFixture.hpp
 *
 *  Created on: Nov 26, 2013
 *      Author: daniel
 */

#ifndef STATICDICTIONARYFIXTURE_HPP_
#define STATICDICTIONARYFIXTURE_HPP_

#include "gtest/gtest.h"
#include "../../src/StaticDictionaries/StaticDictionary.hpp"
#include "../../src/StaticDictionaries/GMN_StaticDictionary.hpp"
#include "../../src/StaticDictionaries/EN_StaticDictionary.hpp"
#include "../../src/StaticDictionaries/Naive_StaticDictionary.hpp"

#define DICTIONARY_SIZE (1<<26)
#define DICTIONARY_WORDS (1<<21)


class StaticDictionaryTest : public ::testing::Test{
protected:
    static const word TEST_ITERATIONS = 100000; //number of iterations for the tests
	virtual void SetUp();
	virtual void TearDown();

	StaticDictionary* gmn_sd_word1;
	StaticDictionary* en_sd_word1;

	StaticDictionary* gmn_sd_word2;
	StaticDictionary* en_sd_word2;

	StaticDictionary* gmn_sd_word3;
	StaticDictionary* en_sd_word3;

	StaticDictionary* gmn_sd_random;
	StaticDictionary* en_sd_random;
	StaticDictionary* naive_sd_random;

	StaticDictionary* gmn_sd_one0;
	StaticDictionary* en_sd_one0;

};



#endif /* STATICDICTIONARYFIXTURE_HPP_ */
