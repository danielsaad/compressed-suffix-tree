/*
 * StaticDictionaryFixture.cpp
 *
 *  Created on: Nov 26, 2013
 *      Author: daniel
 */


#include "StaticDictionaryTest.hpp"
#include <ctime>

void StaticDictionaryTest::SetUp(){


	const word word1 =  0xAAAAAAAA; //101010...
	const word word2 =  0x55555555; //010101...
	const word word3 = 	0XF93CA25B;  //11111001001111001010001001011011 18 1 e 14 0
	const word zero0 =  0X7FFFFFFF; //0111...
	const word zero31 = 0XFFFFFFFE; //1111..0
	const word one0 =   0X80000000; //100....
	const word one31 =  0x00000001; //000...1
	word random_word;
	srand(time(NULL));

	gmn_sd_word1 = new GMN_StaticDictionary(DICTIONARY_SIZE);
	en_sd_word1 = new EN_StaticDictionary(DICTIONARY_SIZE);

	gmn_sd_word2 = new GMN_StaticDictionary(DICTIONARY_SIZE);
	en_sd_word2 = new EN_StaticDictionary(DICTIONARY_SIZE);

	gmn_sd_word3 = new GMN_StaticDictionary(DICTIONARY_SIZE);
	en_sd_word3 = new EN_StaticDictionary(DICTIONARY_SIZE);

	gmn_sd_random = new GMN_StaticDictionary(DICTIONARY_SIZE);
	en_sd_random = new EN_StaticDictionary(DICTIONARY_SIZE);
	naive_sd_random = new Naive_StaticDictionary(DICTIONARY_SIZE);

	gmn_sd_one0 = new GMN_StaticDictionary(DICTIONARY_SIZE);
	en_sd_one0 = new EN_StaticDictionary(DICTIONARY_SIZE);


	for(int i=0;i<DICTIONARY_WORDS;i++){
		gmn_sd_word1->setBits(word1,WORD_SIZE,i*WORD_SIZE);
		en_sd_word1->setBits(word1,WORD_SIZE,i*WORD_SIZE);

		gmn_sd_word2->setBits(word2,WORD_SIZE,i*WORD_SIZE);
		en_sd_word2->setBits(word2,WORD_SIZE,i*WORD_SIZE);

		gmn_sd_word3->setBits(word3,WORD_SIZE,i*WORD_SIZE);
		en_sd_word3->setBits(word3,WORD_SIZE,i*WORD_SIZE);

		gmn_sd_one0->setBits(one0,WORD_SIZE,i*WORD_SIZE);
		en_sd_one0->setBits(one0,WORD_SIZE,i*WORD_SIZE);

		random_word = rand();
		gmn_sd_random->setBits(random_word,WORD_SIZE,i*WORD_SIZE);
		en_sd_random->setBits(random_word,WORD_SIZE,i*WORD_SIZE);
		naive_sd_random->setBits(random_word,WORD_SIZE,i*WORD_SIZE);
	}
	gmn_sd_word1->preprocess();
	en_sd_word1->preprocess();

	gmn_sd_word2->preprocess();
	en_sd_word2->preprocess();

	gmn_sd_word3->preprocess();
	en_sd_word3->preprocess();

	gmn_sd_one0->preprocess();
	en_sd_one0->preprocess();

	gmn_sd_random->preprocess();
	en_sd_random->preprocess();
	naive_sd_random->preprocess();
}

void StaticDictionaryTest::TearDown(){
	delete gmn_sd_word1;
	delete en_sd_word1;

	delete gmn_sd_word2;
	delete en_sd_word2;

	delete gmn_sd_word3;
	delete en_sd_word3;

	delete gmn_sd_one0;
	delete en_sd_one0;

	delete gmn_sd_random;
	delete en_sd_random;
	delete naive_sd_random;


}


TEST_F(StaticDictionaryTest, Rank0_word1){
	/**
	 * Testing Boundaries
	 */
	ASSERT_EQ(gmn_sd_word1->rank0(0),0);
	ASSERT_EQ(gmn_sd_word1->rank0(1),1);
	ASSERT_EQ(gmn_sd_word1->rank0(31),16);
	ASSERT_EQ(gmn_sd_word1->rank0(DICTIONARY_SIZE-1),(DICTIONARY_SIZE/2));

	ASSERT_EQ(en_sd_word1->rank0(0),0);
	ASSERT_EQ(en_sd_word1->rank0(1),1);
	ASSERT_EQ(en_sd_word1->rank0(31),16);
    ASSERT_EQ(en_sd_word1->rank0(DICTIONARY_SIZE-1),(DICTIONARY_SIZE/2));

    /**
     * Testing arbitrary positions
     */
}
TEST_F(StaticDictionaryTest, Rank0_word2){
	/**
	 * Testing Boundaries
	 */
	ASSERT_EQ(gmn_sd_word2->rank0(0),1);
	ASSERT_EQ(gmn_sd_word2->rank0(1),1);
	ASSERT_EQ(gmn_sd_word2->rank0(31),16);
	ASSERT_EQ(gmn_sd_word2->rank0(DICTIONARY_SIZE-1),(DICTIONARY_SIZE/2));

	ASSERT_EQ(en_sd_word2->rank0(0),1);
	ASSERT_EQ(en_sd_word2->rank0(1),1);
	ASSERT_EQ(en_sd_word2->rank0(31),16);
    ASSERT_EQ(en_sd_word2->rank0(DICTIONARY_SIZE-1),(DICTIONARY_SIZE/2));

    /**
     * Testing arbitrary positions
     */
}

TEST_F(StaticDictionaryTest, Rank0_word3){

	/**
	 * Testing Boundaries
	 */
	ASSERT_EQ(gmn_sd_word3->rank0(0),0);
	ASSERT_EQ(gmn_sd_word3->rank0(1),0);
	ASSERT_EQ(gmn_sd_word3->rank0(31),14);
	ASSERT_EQ(gmn_sd_word3->rank0(DICTIONARY_SIZE-1),14*(DICTIONARY_SIZE)/WORD_SIZE);
	ASSERT_EQ(gmn_sd_word3->rank0(108274),47369);
	ASSERT_EQ(gmn_sd_word3->rank0(572619),250520);
	ASSERT_EQ(gmn_sd_word3->rank0(389612),170454);
	ASSERT_EQ(gmn_sd_word3->rank0(123456),54012);

	ASSERT_EQ(en_sd_word3->rank0(0),0);
	ASSERT_EQ(en_sd_word3->rank0(1),0);
	ASSERT_EQ(en_sd_word3->rank0(31),14);
	ASSERT_EQ(en_sd_word3->rank0(108274),47369);
	ASSERT_EQ(en_sd_word3->rank0(572619),250520);
	ASSERT_EQ(en_sd_word3->rank0(389612),170454);
	ASSERT_EQ(en_sd_word3->rank0(123456),54012);

    /**
     * Testing arbitrary positions
     */
}



TEST_F(StaticDictionaryTest, Rank0_one0){

	/**
	 * Testing Boundaries
	 */
	ASSERT_EQ(0,gmn_sd_one0->rank0(0));
	ASSERT_EQ(1,gmn_sd_one0->rank0(1));
	ASSERT_EQ(31,gmn_sd_one0->rank0(31));
	ASSERT_EQ(DICTIONARY_SIZE - (DICTIONARY_SIZE/WORD_SIZE),gmn_sd_one0->rank0(DICTIONARY_SIZE-1));

	ASSERT_EQ(0,en_sd_one0->rank0(0));
	ASSERT_EQ(1,en_sd_one0->rank0(1));
	ASSERT_EQ(31,en_sd_one0->rank0(31));
    ASSERT_EQ(DICTIONARY_SIZE - (DICTIONARY_SIZE/WORD_SIZE),en_sd_one0->rank0(DICTIONARY_SIZE-1));

    /**
     * Testing arbitrary positions
     */
}

TEST_F(StaticDictionaryTest, Rank1_one0){

	/**
	 * Testing Boundaries
	 */
	ASSERT_EQ(1,gmn_sd_one0->rank1(0));
	ASSERT_EQ(1,gmn_sd_one0->rank1(1));
	ASSERT_EQ(1,gmn_sd_one0->rank1(31));
	ASSERT_EQ((DICTIONARY_SIZE/WORD_SIZE),gmn_sd_one0->rank1(DICTIONARY_SIZE-1));

	ASSERT_EQ(1,en_sd_one0->rank1(0));
	ASSERT_EQ(1,en_sd_one0->rank1(1));
	ASSERT_EQ(1,en_sd_one0->rank1(31));
    ASSERT_EQ((DICTIONARY_SIZE/WORD_SIZE),en_sd_one0->rank1(DICTIONARY_SIZE-1));

    /**
     * Testing arbitrary positions
     */
}



TEST_F(StaticDictionaryTest, Rank1_word1){

	/**
	 * Testing Boundaries
	 */
	ASSERT_EQ(gmn_sd_word1->rank1(0),1);
	ASSERT_EQ(gmn_sd_word1->rank1(1),1);
	ASSERT_EQ(gmn_sd_word1->rank1(31),16);
	ASSERT_EQ(gmn_sd_word1->rank1(DICTIONARY_SIZE-1),(DICTIONARY_SIZE/2));

	ASSERT_EQ(en_sd_word1->rank1(0),1);
	ASSERT_EQ(en_sd_word1->rank1(1),1);
	ASSERT_EQ(en_sd_word1->rank1(31),16);
    ASSERT_EQ(en_sd_word1->rank1(DICTIONARY_SIZE-1),(DICTIONARY_SIZE/2));

    /**
     * Testing arbitrary positions
     */
}

TEST_F(StaticDictionaryTest, Rank1_word2){

	/**
	 * Testing Boundaries
	 */
	ASSERT_EQ(gmn_sd_word2->rank1(0),0);
	ASSERT_EQ(gmn_sd_word2->rank1(1),1);
	ASSERT_EQ(gmn_sd_word2->rank1(31),16);
	ASSERT_EQ(gmn_sd_word2->rank1(DICTIONARY_SIZE-1),(DICTIONARY_SIZE/2));

	ASSERT_EQ(en_sd_word2->rank1(0),0);
	ASSERT_EQ(en_sd_word2->rank1(1),1);
	ASSERT_EQ(en_sd_word2->rank1(31),16);
    ASSERT_EQ(en_sd_word2->rank1(DICTIONARY_SIZE-1),(DICTIONARY_SIZE/2));

    /**
     * Testing arbitrary positions
     */
}


TEST_F(StaticDictionaryTest, Rank1_word3){

	/**
	 * Testing Boundaries
	 */
	ASSERT_EQ(gmn_sd_word3->rank1(0),1);
	ASSERT_EQ(gmn_sd_word3->rank1(1),2);
	ASSERT_EQ(gmn_sd_word3->rank1(31),18);
	ASSERT_EQ(gmn_sd_word3->rank1(DICTIONARY_SIZE-1),18*(DICTIONARY_SIZE)/WORD_SIZE);
	ASSERT_EQ(gmn_sd_word3->rank1(108274),60906);
	ASSERT_EQ(gmn_sd_word3->rank1(572619),322100);
	ASSERT_EQ(gmn_sd_word3->rank1(389612),219159);
	ASSERT_EQ(gmn_sd_word3->rank1(123456),69445);

	ASSERT_EQ(en_sd_word3->rank1(0),1);
	ASSERT_EQ(en_sd_word3->rank1(1),2);
	ASSERT_EQ(en_sd_word3->rank1(31),18);
	ASSERT_EQ(en_sd_word3->rank1(DICTIONARY_SIZE-1),18*(DICTIONARY_SIZE)/WORD_SIZE);
	ASSERT_EQ(en_sd_word3->rank1(108274),60906);
	ASSERT_EQ(en_sd_word3->rank1(572619),322100);
	ASSERT_EQ(en_sd_word3->rank1(389612),219159);
	ASSERT_EQ(en_sd_word3->rank1(123456),69445);

    /**
     * Testing arbitrary positions
     */
}



TEST_F(StaticDictionaryTest, Rank0_random){
	word i_random;

	/**
	 * Premise: one of en_sd or gmn_sd gives the correct output
	 */
	for(word k=0;k<TEST_ITERATIONS;k++){
		i_random = rand()%(DICTIONARY_SIZE);
//		ASSERT_EQ(naive_sd_random->rank0(i_random),gmn_sd_random->rank0(i_random));
//		ASSERT_EQ(naive_sd_random->rank0(i_random),en_sd_random->rank0(i_random));
        ASSERT_EQ(gmn_sd_random->rank0(i_random),en_sd_random->rank0(i_random));
	}

}

TEST_F(StaticDictionaryTest, Rank1_random){
	word i_random;

	/**
	 * Premise: one of en_sd or gmn_sd gives the correct output
	 */
	for(word k=0;k<TEST_ITERATIONS;k++){
		i_random = rand()%(DICTIONARY_SIZE);
//		ASSERT_EQ(naive_sd_random->rank1(i_random),gmn_sd_random->rank1(i_random));
//		ASSERT_EQ(naive_sd_random->rank1(i_random),en_sd_random->rank1(i_random));
        ASSERT_EQ(gmn_sd_random->rank1(i_random),en_sd_random->rank1(i_random));
	}

}

TEST_F(StaticDictionaryTest, Select0_word1){

	/**
	 * Testing Boundaries
	 */
    ASSERT_EQ(1,gmn_sd_word1->select0(1));
    ASSERT_EQ(3,gmn_sd_word1->select0(2));
    ASSERT_EQ(31,gmn_sd_word1->select0(16));
    ASSERT_EQ(1023,gmn_sd_word1->select0(512));

    ASSERT_EQ(1,en_sd_word1->select0(1));
    ASSERT_EQ(3,en_sd_word1->select0(2));
    ASSERT_EQ(31,en_sd_word1->select0(16));
    ASSERT_EQ(1023,en_sd_word1->select0(512));

    /**
     * Testing arbitrary positions
     */
}

TEST_F(StaticDictionaryTest, Select0_word2){

	/**
	 * Testing Boundaries
	 */
    ASSERT_EQ(0,gmn_sd_word2->select0(1));
    ASSERT_EQ(2,gmn_sd_word2->select0(2));
    ASSERT_EQ(30,gmn_sd_word2->select0(16));
    ASSERT_EQ(1022,gmn_sd_word2->select0(512));

    ASSERT_EQ(0,en_sd_word2->select0(1));
    ASSERT_EQ(2,en_sd_word2->select0(2));
    ASSERT_EQ(30,en_sd_word2->select0(16));
    ASSERT_EQ(1022,en_sd_word2->select0(512));

    /**
     * Testing arbitrary positions
     */
}


TEST_F(StaticDictionaryTest, Select0_word3){

	/**
	 * Testing Boundaries
	 */

	ASSERT_EQ(gmn_sd_word3->select0(1),5);
	ASSERT_EQ(gmn_sd_word3->select0(31),72);
	ASSERT_EQ(gmn_sd_word3->select0(108274),247480);
	ASSERT_EQ(gmn_sd_word3->select0(572619),1308846);
	ASSERT_EQ(gmn_sd_word3->select0(389612),890543);
	ASSERT_EQ(gmn_sd_word3->select0(123456),282185);
	ASSERT_EQ(en_sd_word3->select0(25800),58968);
	ASSERT_EQ(gmn_sd_word3->select0(199282),455503);
	ASSERT_EQ(gmn_sd_word3->select0(298321),681876);
	ASSERT_EQ(gmn_sd_word3->select0(871321),1991592);
	ASSERT_EQ(gmn_sd_word3->select0(671827),1535604);
	ASSERT_EQ(gmn_sd_word3->select0(614251),1404005);


	ASSERT_EQ(en_sd_word3->select0(1),5);
	ASSERT_EQ(en_sd_word3->select0(31),72);
	ASSERT_EQ(en_sd_word3->select0(108274),247480);
	ASSERT_EQ(en_sd_word3->select0(572619),1308846);
	ASSERT_EQ(en_sd_word3->select0(389612),890543);
	ASSERT_EQ(en_sd_word3->select0(123456),282185);
	ASSERT_EQ(en_sd_word3->select0(25800),58968);
	ASSERT_EQ(gmn_sd_word3->select0(199282),455503);
	ASSERT_EQ(gmn_sd_word3->select0(298321),681876);
	ASSERT_EQ(gmn_sd_word3->select0(871321),1991592);
	ASSERT_EQ(gmn_sd_word3->select0(671827),1535604);
	ASSERT_EQ(gmn_sd_word3->select0(614251),1404005);
    /**
     * Testing arbitrary positions
     */
}

TEST_F(StaticDictionaryTest, Select0_one0){

	/**
	 * Testing Boundaries
	 */
    ASSERT_EQ(1,gmn_sd_one0->select0(1));
    ASSERT_EQ(2,gmn_sd_one0->select0(2));
    ASSERT_EQ(16,gmn_sd_one0->select0(16));
    ASSERT_EQ(528,gmn_sd_one0->select0(512));

    ASSERT_EQ(1,en_sd_one0->select0(1));
    ASSERT_EQ(2,en_sd_one0->select0(2));
    ASSERT_EQ(16,en_sd_one0->select0(16));
    ASSERT_EQ(528,en_sd_one0->select0(512));

    /**
     * Testing arbitrary positions
     */
}




TEST_F(StaticDictionaryTest, Select0_random){
	word i_random;
	word random_number_of_zeroes;

	random_number_of_zeroes = DICTIONARY_SIZE - gmn_sd_random->sequentialPopCount(0,DICTIONARY_SIZE-1);

	/**
	 * Premise: one of en_sd or gmn_sd gives the correct output
	 */
	for(word k=0;k<TEST_ITERATIONS;k++){
		i_random = rand() % random_number_of_zeroes;
//		ASSERT_EQ(naive_sd_random->select0(i_random),gmn_sd_random->select0(i_random));
//		ASSERT_EQ(naive_sd_random->select0(i_random),en_sd_random->select0(i_random));
        ASSERT_EQ(gmn_sd_random->select0(i_random),en_sd_random->select0(i_random));
	}

}

TEST_F(StaticDictionaryTest, Select1_word2){

	/**
	 * Testing Boundaries
	 */
    ASSERT_EQ(1,gmn_sd_word2->select1(1));
    ASSERT_EQ(3,gmn_sd_word2->select1(2));
    ASSERT_EQ(31,gmn_sd_word2->select1(16));
    ASSERT_EQ(1023,gmn_sd_word2->select1(512));

    ASSERT_EQ(1,en_sd_word2->select1(1));
    ASSERT_EQ(3,en_sd_word2->select1(2));
    ASSERT_EQ(31,en_sd_word2->select1(16));
    ASSERT_EQ(1023,en_sd_word2->select1(512));

    /**
     * Testing arbitrary positions
     */
}

TEST_F(StaticDictionaryTest, Select1_word3){

	/**
	 * Testing Boundaries
	 */
	ASSERT_EQ(gmn_sd_word3->select1(1),0);
	ASSERT_EQ(gmn_sd_word3->select1(31),54);
	ASSERT_EQ(gmn_sd_word3->select1(108274),192483);
	ASSERT_EQ(gmn_sd_word3->select1(572619),1017986);
	ASSERT_EQ(gmn_sd_word3->select1(389612),692641);
	ASSERT_EQ(gmn_sd_word3->select1(123456),219474);

	ASSERT_EQ(en_sd_word3->select1(1),0);
	ASSERT_EQ(en_sd_word3->select1(31),54);
	ASSERT_EQ(en_sd_word3->select1(108274),192483);
    ASSERT_EQ(en_sd_word3->select1(572619),1017986);
	ASSERT_EQ(en_sd_word3->select1(389612),692641);
	ASSERT_EQ(en_sd_word3->select1(123456),219474);

    /**
     * Testing arbitrary positions
     */
}

TEST_F(StaticDictionaryTest, Select1_random){
	word i_random;
	word random_number_of_ones;


	random_number_of_ones = gmn_sd_random->sequentialPopCount(0,DICTIONARY_SIZE-1);
	/**
	 * Premise: one of en_sd or gmn_sd gives the correct output
	 */
	for(word k=0;k<TEST_ITERATIONS;k++){
		i_random = rand()%(random_number_of_ones);
        ASSERT_EQ(gmn_sd_random->select1(i_random),en_sd_random->select1(i_random));
//		ASSERT_EQ(naive_sd_random->select1(i_random),en_sd_random->select1(i_random));
//		ASSERT_EQ(naive_sd_random->select1(i_random),gmn_sd_random->select1(i_random));
	}

}
