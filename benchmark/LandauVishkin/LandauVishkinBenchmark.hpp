#ifndef LANDAUVISHKINBENCHMARK_HPP
#define LANDAUVISHKINBENCHMARK_HPP

#include <celero/Celero.h>
#include <vector>
#include "../../src/Text/text.hpp"


class LandauVishkinBenchmark : public celero::TestFixture{
public:
    const static int NUMBER_OF_RUNS = 3;
    const static int NUMBER_OF_ITERATIONS = 1;

    LandauVishkinBenchmark();
    virtual std::vector<int64_t> getExperimentValues() const;
    virtual void setUp(int64_t experimentValue);
    virtual void tearDown();

    Text* createRandomPattern(Text* T,integer size);
    virtual ~LandauVishkinBenchmark();
protected:
    integer _errors;
};

#endif // LANDAUVISHKINBENCHMARK_HPP
