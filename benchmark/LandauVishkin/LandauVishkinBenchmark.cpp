
#include <iostream>
#include <cstdlib>
#include <sys/time.h>
#include "LandauVishkinBenchmark.hpp"
#include "../../src/Text/text.hpp"
#include "../../src/Definitions/types.hpp"
#include "../../src/LandauVishkin/LandauVishkin.hpp"
#include "../../src/LandauVishkin/Index.hpp"

using namespace Algorithms;

Text* LandauVishkinBenchmark::createRandomPattern(Text *t,integer size = 50){
    Text* p;
    struct timeval time;
    integer i;
    byte buffer[size];
    gettimeofday(&time,NULL);

    // microsecond has 1 000 000
    // Assuming you did not need quite that accuracy
    // Also do not assume the system clock has that accuracy.
    srand((time.tv_sec * 1000) + (time.tv_usec / 1000));


    p = new Text("randomPattern.txt",std::ios::out | std::ios::in |
                 std::ios::binary |std::ios::trunc);

    do{
        i = (rand() % t->getLength())-50;
    }while(i<0);
    t->readBuffer(buffer,i,size);
    p->file.write(reinterpret_cast<const char*> (buffer),size);
    p->reset();
    return(p);

}


/**
  Pizza Chilli
**/

/**
 * DNA.50MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_DNA50_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dna.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_DNA50_50,LV_RMQ,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("media/SSD/pizzaChili/dna.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_RMQ<IndexRMQ<RegularLCP,RMQ_succinct<RegularLCP>>>(t,p,_errors,out);
}

BENCHMARK_F(LV_DNA50_50,LV_DMIN,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dna.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DMin<Index<RegularLCP>>(t,p,_errors,out);
}


BENCHMARK_F(LV_DNA50_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dna.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_DNA50_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dna.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_DNA50_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dna.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}


/**
 * DNA.100MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_DNA100_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dna.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_DNA100_50,LV_RMQ,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dna.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_RMQ<IndexRMQ<RegularLCP,RMQ_succinct<RegularLCP>>>(t,p,_errors,out);
}

BENCHMARK_F(LV_DNA100_50,LV_DMIN,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dna.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DMin<Index<RegularLCP>>(t,p,_errors,out);
}


BENCHMARK_F(LV_DNA100_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dna.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_DNA100_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dna.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_DNA100_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dna.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}

/**
 * DNA.200MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_DNA200_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dna.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_DNA200_50,LV_RMQ,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dna.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_RMQ<IndexRMQ<RegularLCP,RMQ_succinct<RegularLCP>>>(t,p,_errors,out);
}

BENCHMARK_F(LV_DNA200_50,LV_DMIN,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dna.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DMin<Index<RegularLCP>>(t,p,_errors,out);
}


BENCHMARK_F(LV_DNA200_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dna.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_DNA200_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dna.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_DNA200_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dna.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}


/**
 * DNA.400MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_DNA400_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dna",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}


BENCHMARK_F(LV_DNA400_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dna",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_DNA400_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dna",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_DNA400_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dna",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}


/**
 * DBLP XML 50MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_DBLP50_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_DBLP50_50,LV_RMQ,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_RMQ<IndexRMQ<RegularLCP,RMQ_succinct<RegularLCP>>>(t,p,_errors,out);
}

BENCHMARK_F(LV_DBLP50_50,LV_DMIN,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DMin<Index<RegularLCP>>(t,p,_errors,out);
}


BENCHMARK_F(LV_DBLP50_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_DBLP50_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_DBLP50_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}



/**
 * DBLP XML 100MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_DBLP100_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_DBLP100_50,LV_RMQ,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_RMQ<IndexRMQ<RegularLCP,RMQ_succinct<RegularLCP>>>(t,p,_errors,out);
}

BENCHMARK_F(LV_DBLP100_50,LV_DMIN,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DMin<Index<RegularLCP>>(t,p,_errors,out);
}


BENCHMARK_F(LV_DBLP100_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_DBLP100_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_DBLP100_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}


/**
 * DBLP XML 200MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_DBLP200_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_DBLP200_50,LV_RMQ,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_RMQ<IndexRMQ<RegularLCP,RMQ_succinct<RegularLCP>>>(t,p,_errors,out);
}

BENCHMARK_F(LV_DBLP200_50,LV_DMIN,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DMin<Index<RegularLCP>>(t,p,_errors,out);
}


BENCHMARK_F(LV_DBLP200_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_DBLP200_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_DBLP200_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}


/**
 * DBLP XML 283MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_DBLP283_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}


BENCHMARK_F(LV_DBLP283_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_DBLP283_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_DBLP283_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/dblp.xml",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}


/**
 * SOURCES 50MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_SOURCES50_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/sources.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_SOURCES50_50,LV_RMQ,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/sources.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_RMQ<IndexRMQ<RegularLCP,RMQ_succinct<RegularLCP>>>(t,p,_errors,out);
}

BENCHMARK_F(LV_SOURCES50_50,LV_DMIN,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/sources.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DMin<Index<RegularLCP>>(t,p,_errors,out);
}


BENCHMARK_F(LV_SOURCES50_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/sources.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_SOURCES50_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/sources.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_SOURCES50_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/sources.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}


/**
 * SOURCES 100MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_SOURCES100_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/sources.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_SOURCES100_50,LV_RMQ,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/sources.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_RMQ<IndexRMQ<RegularLCP,RMQ_succinct<RegularLCP>>>(t,p,_errors,out);
}

BENCHMARK_F(LV_SOURCES100_50,LV_DMIN,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/sources.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DMin<Index<RegularLCP>>(t,p,_errors,out);
}


BENCHMARK_F(LV_SOURCES100_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/sources.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_SOURCES100_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/sources.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_SOURCES100_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/sources.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}

/**
 * SOURCES 202MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_SOURCES202_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/sources",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_SOURCES202_50,LV_RMQ,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/sources",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_RMQ<IndexRMQ<RegularLCP,RMQ_succinct<RegularLCP>>>(t,p,_errors,out);
}

BENCHMARK_F(LV_SOURCES202_50,LV_DMIN,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/sources",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DMin<Index<RegularLCP>>(t,p,_errors,out);
}


BENCHMARK_F(LV_SOURCES202_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/sources",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_SOURCES202_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/sources",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_SOURCES202_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/sources",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}

/**
 * PITCHES 54MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_PITCHES54_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/pitches",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_PITCHES54,LV_RMQ,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/pitches",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_RMQ<IndexRMQ<RegularLCP,RMQ_succinct<RegularLCP>>>(t,p,_errors,out);
}

BENCHMARK_F(LV_PITCHES54,LV_DMIN,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/pitches",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DMin<Index<RegularLCP>>(t,p,_errors,out);
}


BENCHMARK_F(LV_PITCHES54,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/pitches",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_PITCHES54,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/pitches",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_PITCHES54,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/pitches",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}


/**
 * PROTEINS 50MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_PROTEINS50_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/proteins.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_PROTEINS50_50,LV_RMQ,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/proteins.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_RMQ<IndexRMQ<RegularLCP,RMQ_succinct<RegularLCP>>>(t,p,_errors,out);
}

BENCHMARK_F(LV_PROTEINS50_50,LV_DMIN,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/proteins.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DMin<Index<RegularLCP>>(t,p,_errors,out);
}


BENCHMARK_F(LV_PROTEINS50_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/proteins.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_PROTEINS50_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/proteins.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_PROTEINS50_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/proteins.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}



/**
 * PROTEINS 100MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_PROTEINS100_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/proteins.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_PROTEINS100_50,LV_RMQ,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/proteins.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_RMQ<IndexRMQ<RegularLCP,RMQ_succinct<RegularLCP>>>(t,p,_errors,out);
}

BENCHMARK_F(LV_PROTEINS100_50,LV_DMIN,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/proteins.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DMin<Index<RegularLCP>>(t,p,_errors,out);
}


BENCHMARK_F(LV_PROTEINS100_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/proteins.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_PROTEINS100_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/proteins.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_PROTEINS100_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/proteins.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}


/**
 * PROTEINS 200MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_PROTEINS200_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/proteins.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_PROTEINS200_50,LV_RMQ,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/proteins.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_RMQ<IndexRMQ<RegularLCP,RMQ_succinct<RegularLCP>>>(t,p,_errors,out);
}

BENCHMARK_F(LV_PROTEINS200_50,LV_DMIN,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/proteins.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DMin<Index<RegularLCP>>(t,p,_errors,out);
}


BENCHMARK_F(LV_PROTEINS200_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/proteins.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_PROTEINS200_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/proteins.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_PROTEINS200_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/proteins.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}

/**
 * PROTEINS 1.2GB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_PROTEINS12_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/proteins",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_PROTEINS12_50,LV_DC_SE2,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/proteins",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}


/**
 * ENGLISH 50MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_ENGLISH50_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_ENGLISH50_50,LV_RMQ,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_RMQ<IndexRMQ<RegularLCP,RMQ_succinct<RegularLCP>>>(t,p,_errors,out);
}

BENCHMARK_F(LV_ENGLISH50_50,LV_DMIN,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DMin<Index<RegularLCP>>(t,p,_errors,out);
}


BENCHMARK_F(LV_ENGLISH50_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_ENGLISH50_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_ENGLISH50_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english.50MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}

/**
 * ENGLISH 100MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_ENGLISH100_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_ENGLISH100_50,LV_RMQ,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_RMQ<IndexRMQ<RegularLCP,RMQ_succinct<RegularLCP>>>(t,p,_errors,out);
}

BENCHMARK_F(LV_ENGLISH100_50,LV_DMIN,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DMin<Index<RegularLCP>>(t,p,_errors,out);
}


BENCHMARK_F(LV_ENGLISH100_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_ENGLISH100_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_ENGLISH100_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english.100MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}

/**
 * ENGLISH 200MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_ENGLISH200_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_ENGLISH200_50,LV_RMQ,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_RMQ<IndexRMQ<RegularLCP,RMQ_succinct<RegularLCP>>>(t,p,_errors,out);
}

BENCHMARK_F(LV_ENGLISH200_50,LV_DMIN,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DMin<Index<RegularLCP>>(t,p,_errors,out);
}


BENCHMARK_F(LV_ENGLISH200_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_ENGLISH200_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_ENGLISH200_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english.200MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}


/**
 * ENGLISH 1024MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_ENGLISH1024_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english.1024MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_ENGLISH1024_50,LV_DC_SE2,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english.1024MB",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}


/**
 * ENGLISH 2.1GB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_ENGLISH21_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_ENGLISH21_50,LV_DC_SE2,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("/media/SSD/pizzaChili/english",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}


/**
  Manzini Corpus
**/

/**
 * CHR22 33MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_CHR22_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/chr22.dna",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_CHR22_50,LV_RMQ,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/chr22.dna",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_RMQ<IndexRMQ<RegularLCP,RMQ_succinct<RegularLCP>>>(t,p,_errors,out);
}

BENCHMARK_F(LV_CHR22_50,LV_DMIN,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/chr22.dna",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DMin<Index<RegularLCP>>(t,p,_errors,out);
}


BENCHMARK_F(LV_CHR22_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/chr22.dna",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_CHR22_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/chr22.dna",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_CHR22_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/chr22.dna",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}


/**
 * HOWTO 38MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_HOWTO_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/howto",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_HOWTO_50,LV_RMQ,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/howto",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_RMQ<IndexRMQ<RegularLCP,RMQ_succinct<RegularLCP>>>(t,p,_errors,out);
}

BENCHMARK_F(LV_HOWTO_50,LV_DMIN,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/howto",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DMin<Index<RegularLCP>>(t,p,_errors,out);
}


BENCHMARK_F(LV_HOWTO_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/howto",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_HOWTO_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/howto",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_HOWTO_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/howto",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}


/**
 * JDK 67MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_JDK_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/jdk13c",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_JDK_50,LV_RMQ,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/jdk13c",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_RMQ<IndexRMQ<RegularLCP,RMQ_succinct<RegularLCP>>>(t,p,_errors,out);
}

BENCHMARK_F(LV_JDK_50,LV_DMIN,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/howto",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DMin<Index<RegularLCP>>(t,p,_errors,out);
}


BENCHMARK_F(LV_JDK_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/jdk13c",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_JDK_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/jdk13c",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_JDK_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/jdk13c",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}



/**
 * RCTAIL96 110MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_RCTAIL_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/rctail96",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_RCTAIL_50,LV_RMQ,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/rctail96",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_RMQ<IndexRMQ<RegularLCP,RMQ_succinct<RegularLCP>>>(t,p,_errors,out);
}

BENCHMARK_F(LV_RCTAIL_50,LV_DMIN,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/rctail96",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DMin<Index<RegularLCP>>(t,p,_errors,out);
}


BENCHMARK_F(LV_RCTAIL_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/rctail96",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_RCTAIL_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/rctail96",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_RCTAIL_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/rctail96",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}

/**
 * RFC 112MB
 * PATTERN LENGTH = 50
 */

BASELINE_F(LV_RFC_50,LV_DC_SE,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/rfc",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Semi_External(t,p,_errors,out);
}

BENCHMARK_F(LV_RFC_50,LV_RMQ,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/rfc",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_RMQ<IndexRMQ<RegularLCP,RMQ_succinct<RegularLCP>>>(t,p,_errors,out);
}

BENCHMARK_F(LV_RFC_50,LV_DMIN,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/rfc",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DMin<Index<RegularLCP>>(t,p,_errors,out);
}


BENCHMARK_F(LV_RFC_50,LV_DC,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/rfc",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC(t,p,_errors,out);
}


BENCHMARK_F(LV_RFC_50,LV_DC_NAV,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/rfc",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Navarro(t,p,_errors,out);
}

BENCHMARK_F(LV_RFC_50,LV_DC_PAR,LandauVishkinBenchmark,
            LandauVishkinBenchmark::NUMBER_OF_RUNS,
            LandauVishkinBenchmark::NUMBER_OF_ITERATIONS){
    Text* t = new Text("manzini/rfc",std::ios::in | std::ios::binary);
    Text* p  = createRandomPattern(t);
    Text* out = new Text("/media/SSD/output.txt",std::ios::out);
    Algorithms::landauVishkin_DC_Parallel(t,p,_errors,out);
}
