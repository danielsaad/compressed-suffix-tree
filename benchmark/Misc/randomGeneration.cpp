#include "randomGeneration.hpp"
#include <cstdlib>

BASELINE_F(RandomGeneration,Rand,RandomGeneratorBenchmark,
         RandomGeneratorBenchmark::NUMBER_OF_RUNS,
         RandomGeneratorBenchmark::NUMBER_OF_ITERATIONS){
    rand();
}



BENCHMARK_F(RandomGeneration,Rand,RandomGeneratorBenchmark,
         RandomGeneratorBenchmark::NUMBER_OF_RUNS,
         RandomGeneratorBenchmark::NUMBER_OF_ITERATIONS){
    xorshf96();
}
