#ifndef RANDOMGENERATION_HPP
#define RANDOMGENERATION_HPP


#include <celero/Celero.h>
#include "../../src/Definitions/types.hpp"


class RandomGeneratorBenchmark : public celero::TestFixture{
public:

    const static int NUMBER_OF_RUNS = 100;
    const static int NUMBER_OF_ITERATIONS = 1<<20;

    RandomGeneratorBenchmark(){}
    virtual std::vector<int64_t> getExperimentValues() const{
        std::vector<int64_t> problemSpace;
        problemSpace.push_back(0);
        return(problemSpace);
    }

    unsigned long xorshf96(void) {          //period 2^96-1
        static word x=123456789, y=362436069, z=521288629;
        word t;
        x ^= x << 16;
        x ^= x >> 5;
        x ^= x << 1;

       t = x;
       x = y;
       y = z;
       z = t ^ x ^ y;

      return z;
    }
    ~RandomGeneratorBenchmark(){}
};


#endif // RANDOMGENERATION_HPP
