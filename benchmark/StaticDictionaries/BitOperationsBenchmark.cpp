
#include "BitOperationsBenchmark.hpp"


BitOperationsBenchmark::BitOperationsBenchmark(){}
std::vector<int64_t> BitOperationsBenchmark::getExperimentValues() const{
    std::vector<int64_t> problemSpace;
    problemSpace.push_back(1<<20);
    return(problemSpace);
}


void BitOperationsBenchmark::setUp(int64_t experimentValue){
    const word number_of_words = experimentValue;
    operations = BitOperations::getSingletonInstance();
    v.resize(number_of_words);
    srand(time(NULL));
    for(word i=0;i<v.size();i++){
        v[i] = rand();
    }
}


void BitOperationsBenchmark::tearDown(){
    v.clear();
}

BitOperationsBenchmark::~BitOperationsBenchmark(){}




word BitOperationsBenchmark::popCountWord1(word w){
    return(__builtin_popcount(w));
}

word BitOperationsBenchmark::popCountWord2(word x){
   x = x - ((x >> 1) & 0x55555555);
   x = (x & 0x33333333) + ((x >> 2) & 0x33333333);
   x = (x + (x >> 4)) & 0x0F0F0F0F;
   x = x + (x >> 8);
   x = x + (x >> 16);
   return x & 0x0000003F;
}




BASELINE_F(BitOperations,Popcount1,BitOperationsBenchmark,
            BitOperationsBenchmark::NUMBER_OF_RUNS,
            BitOperationsBenchmark::NUMBER_OF_ITERATIONS){

    for(word i=0;i<v.size();i++){
        popCountWord1(v[i]);
    }
}

BENCHMARK_F(BitOperations,Popcount2,BitOperationsBenchmark,
            BitOperationsBenchmark::NUMBER_OF_RUNS,
            BitOperationsBenchmark::NUMBER_OF_ITERATIONS){

    for(word i=0;i<v.size();i++){
        popCountWord2(v[i]);
    }
}

BENCHMARK_F(BitOperations,Popcount,BitOperationsBenchmark,
            BitOperationsBenchmark::NUMBER_OF_RUNS,
            BitOperationsBenchmark::NUMBER_OF_ITERATIONS){
    for(word i=0;i<v.size();i++){
        operations->popCountWord(v[i]);
    }
}
