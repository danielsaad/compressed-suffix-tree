/*
 * BenchmarkStaticDictionary.hpp
 *
 *  Created on: Dec 2, 2013
 *      Author: daniel
 */

#ifndef BENCHMARKSTATICDICTIONARY_HPP_
#define BENCHMARKSTATICDICTIONARY_HPP_

#include <celero/Celero.h>
#include <ctime>
#include <cstdlib>
#include "../../src/StaticDictionaries/EN_StaticDictionary.hpp"
#include "../../src/StaticDictionaries/GMN_StaticDictionary.hpp"



class StaticDictionaryBenchmark: public celero::TestFixture{
public:

    static const word DICTIONARY_SIZE = (1 << 28);
    static const word DICTIONARY_WORDS = (DICTIONARY_SIZE) >> WORD_SHIFT;
    static const word NUMBER_OF_RUNS = 10;
    static const word NUMBER_OF_ITERATIONS = 1<<10;


    StaticDictionaryBenchmark();

    virtual std::vector<int64_t> getExperimentValues() const;
    virtual void setUp(int64_t experimentValue);
    virtual void tearDown();

    virtual ~StaticDictionaryBenchmark();

protected:
	StaticDictionary* gmn_sd_random;
	StaticDictionary* en_sd_random;
	word number_of_zeroes_random;
	word number_of_ones_random;
};






#endif /* BENCHMARKSTATICDICTIONARY_HPP_ */
