#include "StaticDictionaryBenchmark.hpp"


StaticDictionaryBenchmark::StaticDictionaryBenchmark(){}

std::vector<int64_t> StaticDictionaryBenchmark::getExperimentValues() const{
    std::vector<int64_t> problemSpace;
    return(problemSpace);
}

void StaticDictionaryBenchmark::setUp(int64_t experimentValue){
    word random_word;
    srand(time(NULL));
    gmn_sd_random = new GMN_StaticDictionary(DICTIONARY_SIZE);
    en_sd_random = new EN_StaticDictionary(DICTIONARY_SIZE);
    number_of_ones_random = 0;
    number_of_zeroes_random = 0;
    for(word i=0;i<DICTIONARY_WORDS;i++){
        random_word = rand();
        gmn_sd_random->setBits(random_word,WORD_SIZE,i*WORD_SIZE);
        en_sd_random->setBits(random_word,WORD_SIZE,i*WORD_SIZE);

        //valid only if Dictionary Size % WORD SIZE = 0
        number_of_ones_random+= BitOperations::popCountWord(random_word);

        //valid only if Dictionary Size % WORD SIZE = 0
        number_of_zeroes_random+=
                WORD_SIZE - BitOperations::popCountWord(random_word);
    }
    gmn_sd_random->preprocess();
    en_sd_random->preprocess();
}

void StaticDictionaryBenchmark::tearDown(){
    delete gmn_sd_random;
    delete en_sd_random;
}

StaticDictionaryBenchmark::~StaticDictionaryBenchmark(){}


BASELINE_F(Rank0,GMN_Rank0_random,StaticDictionaryBenchmark,
        StaticDictionaryBenchmark::NUMBER_OF_RUNS,
            StaticDictionaryBenchmark::NUMBER_OF_ITERATIONS){
    word i_random;
    i_random = rand() % DICTIONARY_SIZE;
    gmn_sd_random->rank0(i_random);

}

BASELINE_F(Rank1,GMN_Rank1_random,StaticDictionaryBenchmark,
        StaticDictionaryBenchmark::NUMBER_OF_RUNS,
            StaticDictionaryBenchmark::NUMBER_OF_ITERATIONS){
    word i_random;
    i_random = rand() % DICTIONARY_SIZE;
    gmn_sd_random->rank1(i_random);
}

BASELINE_F(Select0,GMN_Select0_random,StaticDictionaryBenchmark,
        StaticDictionaryBenchmark::NUMBER_OF_RUNS,
            StaticDictionaryBenchmark::NUMBER_OF_ITERATIONS){
    word i_random;
    i_random = rand()% number_of_zeroes_random;
    gmn_sd_random->select0(i_random);
}

BASELINE_F(Select1,GMN_Select1_random,StaticDictionaryBenchmark,
        StaticDictionaryBenchmark::NUMBER_OF_RUNS,
            StaticDictionaryBenchmark::NUMBER_OF_ITERATIONS){
    word i_random = rand();
    i_random = rand()% number_of_ones_random;
    gmn_sd_random->select1(i_random);
}


BENCHMARK_F(Rank0,EN_Rank0_random,StaticDictionaryBenchmark,
        StaticDictionaryBenchmark::NUMBER_OF_RUNS,
            StaticDictionaryBenchmark::NUMBER_OF_ITERATIONS){
    word i_random;
    i_random = rand() % DICTIONARY_SIZE;
    en_sd_random->rank0(i_random);

}

BENCHMARK_F(Rank1,EN_Rank1_random,StaticDictionaryBenchmark,
        StaticDictionaryBenchmark::NUMBER_OF_RUNS,
            StaticDictionaryBenchmark::NUMBER_OF_ITERATIONS){
    word i_random;
    i_random = rand() % DICTIONARY_SIZE;
    en_sd_random->rank1(i_random);
}

BENCHMARK_F(Select0,EN_Select0_random,StaticDictionaryBenchmark,
        StaticDictionaryBenchmark::NUMBER_OF_RUNS,
            StaticDictionaryBenchmark::NUMBER_OF_ITERATIONS){
    word i_random;
    i_random = rand()% number_of_zeroes_random;
    en_sd_random->select0(i_random);
}

BENCHMARK_F(Select1,EN_Select1_random,StaticDictionaryBenchmark,
        StaticDictionaryBenchmark::NUMBER_OF_RUNS,
            StaticDictionaryBenchmark::NUMBER_OF_ITERATIONS){
    word i_random = rand();
    i_random = rand()% number_of_ones_random;
    en_sd_random->select1(i_random);
}

