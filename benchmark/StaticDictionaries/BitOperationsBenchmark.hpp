/*
 * BenchmarkBitOperations.hpp
 *
 *  Created on: Dec 2, 2013
 *      Author: daniel
 */

#ifndef BENCHMARKBITOPERATIONS_HPP_
#define BENCHMARKBITOPERATIONS_HPP_

#include <ctime>
#include <cstdlib>
#include <vector>
#include <celero/Celero.h>
#include "../../src/StaticDictionaries/BitOperations.hpp"


class BitOperationsBenchmark : public celero::TestFixture{
public:

    const static word NUMBER_OF_RUNS = 10;
    const static word NUMBER_OF_ITERATIONS = 100;

    BitOperationsBenchmark();

    virtual std::vector<int64_t> getExperimentValues() const;
    virtual void setUp(int64_t experimentValue);
    virtual void tearDown();

	virtual word popCountWord1(word w); //benchmark against our popcount
	virtual word popCountWord2(word w); //benchmark against our popcount
    virtual ~BitOperationsBenchmark();

protected:
	BitOperations* operations;
	std::vector<word> v;
};


#endif /* BENCHMARKBITOPERATIONS_HPP_ */
