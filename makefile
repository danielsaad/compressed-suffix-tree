#Makefile
#Author: Daniel Saad Nogueira Nunes

#Compiler
CPP=g++
#Linker
LD=g++

#compilation flags
CFLAGS= -std=c++0x  -march=native -O2 -Wall -mpopcnt -finline-functions -msse4.2
CFLAGS_DEBUG= -std=c++0x -g -Wall 
CFLAGS_TEST= -std=c++0x -g -Wall 
CFLAGS_BENCHMARK= -std=c++0x  -march=native -mpopcnt -O2 -Wall -finline-functions -msse4.2

#linker flags
LFLAGS= -ldivsufsort -lm -pthread
LFLAGS_DEBUG= -ldivsufsort -lm -pthread
LFLAGS_TEST= -ldivsufsort -lm -pthread  -lgtest
LFLAGS_BENCHMARK= -ldivsufsort -lm -pthread -lcelero

#Modules
MODULES:= StaticDictionaries DataStructures LCP NSV PSI RMQ SA SuffixSorting Text Definitions LandauVishkin
MODULES_BENCHMARK:= StaticDictionaries DataStructures LCP NSV PSI RMQ SA SuffixSorting Text Definitions LandauVishkin Misc
#generate all source folders
SRC_DIR:= $(addprefix src/,$(MODULES))
#Build Directory
BUILD_DIR:= $(addprefix build/,$(MODULES))
#Debug Directory
DEBUG_DIR:= $(addprefix debug/,$(MODULES))
#Test Directory
TEST_DIR:= $(addprefix test/,$(MODULES))
#Benchmark Directory
BENCHMARK_DIR:= $(addprefix benchmark/,$(MODULES_BENCHMARK))
#Bin Directory
BIN_DIR:= bin



#Source Files
SRC:= $(foreach sdir,$(SRC_DIR),$(wildcard $(sdir)/*.cpp))

#Test Source Files
TEST_SRC:= $(foreach sdir,$(TEST_DIR),$(wildcard $(sdir)/*.cpp))

#Benchmark Source Files
BENCHMARK_SRC:= $(foreach sdir,$(BENCHMARK_DIR),$(wildcard $(sdir)/*.cpp))

#objects for src files
OBJ_BUILD:= $(patsubst src/%.cpp,build/%.o,$(SRC))
OBJ_DEBUG:= $(patsubst src/%.cpp,debug/%.o,$(SRC))
OBJ_BENCHMARK:= $(patsubst src/%.cpp,benchmark/%.o,$(SRC))
OBJ_TEST:= $(patsubst src/%.cpp,test/%.o,$(SRC))

#objects for test src files
OBJ_TEST_SRC = $(patsubst test/%.cpp,test/%.o,$(TEST_SRC))
#objects for benchmark src files
OBJ_BENCHMARK_SRC = $(patsubst benchmark/%.cpp,benchmark/%.o,$(BENCHMARK_SRC))

#includesddd
INCLUDES  := $(addprefix -I,$(SRC_DIR))


vpath %.cpp $(SRC_DIR)
vpath %.cpp $(TEST_DIR)
vpath %.cpp $(BENCHMARK_DIR)

define make-goal
$1/%.o: %.cpp
	$(CPP) $(INCLUDES) -c  $(CFLAGS) $$< -o $$@
endef

define make-goal-debug
$1/%.o: %.cpp
	$(CPP) $(INCLUDES) -c  $(CFLAGS_DEBUG) $$< -o $$@
endef

define make-goal-test
$1/%.o: %.cpp
	$(CPP) $(INCLUDES) -I external/ -c  $(CFLAGS_TEST) $$< -o $$@
endef

define make-goal-benchmark
$1/%.o: %.cpp
	$(CPP) $(INCLUDES) -I external/ -c  $(CFLAGS_BENCHMARK) $$< -o $$@
endef

all: release benchmark test debug install landauVishkin

release: checkdirs build

build: $(OBJ_BUILD)
	
benchmark: checkdirs buildBenchmark

landauVishkin: checkdirs buildLandauVishkin

buildLandauVishkin: $(OBJ_BUILD)
	$(CPP) $(CFLAGS) $^  src/LandauVishkinMain.cpp -o src/LandauVishkinMain.exe $(LFLAGS)

buildBenchmark: $(OBJ_BENCHMARK) $(OBJ_BENCHMARK_SRC) benchmark/celeroMain.o
	$(CPP) $^ -o benchmark/benchmark.exe $(LFLAGS_BENCHMARK)

benchmark/celeroMain.o: benchmark/celeroMain.cpp
	$(CPP) -I include/ $(CFLAGS_BENCHMARK) -c  $^ -o $@
	
	
test: $(OBJ_TEST) $(OBJ_TEST_SRC) test/test.o
	$(CPP) $^  -o test/test.exe  $(LFLAGS_TEST)

test/test.o: test/test.cpp
	$(CPP) $(CFLAGS_TEST) -I external/ -c  $^ -o $@

debug: $(OBJ_DEBUG)

checkdirs: $(BUILD_DIR) $(DEBUG_DIR) $(TEST_DIR) $(BENCHMARK_DIR) $(BIN_DIR) $(APPLICATIONS_DIR)
	@echo Checking Directories

applications: $(OBJ_APPLICATIONS)

$(BIN_DIR):
	@mkdir -p $@

$(BUILD_DIR):
	@mkdir -p $@
	
$(TEST_DIR):
	@mkdir -p $@
	
$(DEBUG_DIR):
	@mkdir -p $@

$(BENCHMARK_DIR):
	@mkdir -p $@
	
$(APPLICATIONS_DIR):
	@mkdir -p $@
   
install: 
	@cp benchmark/benchmark.exe $(BIN_DIR)
	
.PHONY: clean
clean:
	@rm $(OBJ_BUILD)
	@rm $(OBJ_DEBUG)
	@rm $(OBJ_BENCHMARK)
	@rm $(OBJ_BENCHMARK_SRC)
	@rm $(OBJ_TEST)
	@rm $(OBJ_TEST_SRC)
	@rm bin/*
	@rm benchmark/*.o
	@rm test/*.o
	@rm debug/*.o

$(foreach bdir,$(BUILD_DIR),$(eval $(call make-goal,$(bdir))))
$(foreach bdir,$(DEBUG_DIR),$(eval $(call make-goal-debug,$(bdir))))
$(foreach bdir,$(TEST_DIR),$(eval $(call make-goal-test,$(bdir))))
$(foreach bdir,$(BENCHMARK_DIR),$(eval $(call make-goal-benchmark,$(bdir))))
